//
//  LVHomeNativeAds.m
//  Lichviet2016
//
//  Created by Quyet Nguyen on 8/30/16.
//  Copyright © 2016 company. All rights reserved.
//

#import "LVHomeNativeAds.h"
#import "FBMediaView.h"
#import "MPNativeAd.h"
#import "MPNativeAdDelegate.h"
#import "VDConfigNotification.h"
#import "MPMediationAdController.h"
#import "FBAdChoicesView.h"
#import "MPNativeAdAdapter.h"
#import "MPNativeAdConstants.h"
#import "MPMediationAdController.h"
#import "FBAdChoicesView.h"
#import "MPNativeAdAdapter.h"
#import "UIImageView+WebCache.h"
#import "LVGlobal.h"
#import "Global.h"
#import "MPMediationAdController.h"

//extern UIImage *TintImageByConstantColor( UIImage *image, UIColor *color );

@interface LVHomeNativeAds ()<MPNativeAdDelegate, FBNativeAdDelegate>
{
    FBAdChoicesView *fbAdChoiceView;
}

@property (strong, nonatomic) NSObject      *curNativeAd;
@property (strong, nonatomic) NSString      *curAdNetworkName;

@property (weak, nonatomic) IBOutlet UIImageView *coverAdsImageView;
@property (weak, nonatomic) IBOutlet FBMediaView *adCoverMediaView;

@property (weak, nonatomic) IBOutlet UILabel *descriptionAdsLabel;
@property (weak, nonatomic) IBOutlet UIButton *imageCoverButton;

@property (weak, nonatomic) IBOutlet UIImageView *appImageView;

@property (weak, nonatomic) IBOutlet UILabel *titleAdsLabel;
@property (weak, nonatomic) IBOutlet UILabel *sponsoredLabel;
@property (weak, nonatomic) IBOutlet UIImageView *sponsoredIcon;
@property (weak, nonatomic) IBOutlet UIView *adChoicesView;
@property (weak, nonatomic) IBOutlet UIButton *installAppButton;
@property (weak, nonatomic) IBOutlet UIButton *sponsoredButton;

@property (weak, nonatomic) IBOutlet UIView *buttonView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sponsoredIconSize;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sponsoredIconToRight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sponsoredIconToLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *adChoicesViewWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *appIconToLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *coverAdsImageToLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *installAppButtonWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconAppSize;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleAdsLabelToLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonViewHeight;


// admob native view

/// The native ad view that is being presented.
@property(nonatomic, strong) UIView *nativeAdView;

@end

@implementation LVHomeNativeAds

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self loadViewFromNib];
    }
    return self;
}

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self loadViewFromNib];
    }
    return self;
}

- (void)loadViewFromNib{
    UIView *contentView = [[[NSBundle mainBundle] loadNibNamed:@"LVHomeNativeAds" owner:self options:nil] objectAtIndex:0];
    contentView.frame = self.bounds;
    [self addSubview:contentView];
    [self configureUI];
}

- (void) configureUI {
//    self.lineViewHeight.constant = [LVUtils GetHeightLineHair];
    
    self.headerViewHeight.constant = GetSpaceBetweenItemForIphone6Plus(48.f, 0.f);
    self.iconAppSize.constant = GetSpaceBetweenItemForIphone6Plus(16.f, 0.f);
    self.appImageView.layer.cornerRadius = self.iconAppSize.constant/2;
    self.appImageView.layer.masksToBounds = YES;

    if (gScreenSize == LVScreenSizeIpad){
        self.sponsoredIconSize.constant = 20.f;
        self.adChoicesViewWidth.constant = 200.f;
    }
    
    self.installAppButtonWidth.constant = gScreenWidth;
    self.titleAdsLabelToLeft.constant = GetSpaceBetweenItemForIphone6Plus(12.f, 0.f);
    self.coverAdsImageToLeft.constant = GetSpaceBetweenItemForIphone6Plus(34.f, 0.f);
    
    self.buttonViewHeight.constant = GetSpaceBetweenItemForIphone6Plus(48.f, 0.f);
    
    self.coverAdsImageView.layer.cornerRadius = GetSpaceBetweenItemForIphone6Plus(5.f, 0.f);
    self.coverAdsImageView.layer.masksToBounds = YES;

    self.adCoverMediaView.layer.cornerRadius = GetSpaceBetweenItemForIphone6Plus(5.f, 0.f);
    self.adCoverMediaView.layer.masksToBounds = YES;

//    self.descriptionAdsLabelToLeft.constant = GetSpaceBetweenItemForIphone6Plus(12.f, 0.f);
//    self.installAppButtonToRight.constant = GetSpaceBetweenItemForIphone6Plus(12.f, 0.f);
    
    self.titleAdsLabel.font = [UIFont fontWithName:getFontName(MODE_FONT_REGULAR,YES) size:GetFontSizeMaxFitIP6Plus(14.f)];
    self.descriptionAdsLabel.font = [UIFont fontWithName:getFontName(MODE_FONT_REGULAR,NO) size:GetFontSizeMaxFitIP6Plus(11.f)];
    self.sponsoredLabel.font = [UIFont fontWithName:getFontName(MODE_FONT_MEDIUM,NO) size:GetFontSizeMaxFitIP6Plus(10.f)];
    self.installAppButton.titleLabel.font = [UIFont fontWithName:getFontName(MODE_FONT_BOLD,NO) size:GetFontSizeMaxFitIP6Plus(12.f)];
    [self.installAppButton.titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
    
//    [self.installAppButton setImage:TintImageByConstantColor([UIImage imageNamed:@"ic_more"], UIColorFromRGB(25.f, 117.f, 209.f)) forState:UIControlStateNormal];
    
    self.installAppButton.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.installAppButton.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.installAppButton.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);

}

- (void)setNativeAd:(NSObject *)nativeAd fromAdNetwork:(NSString *)adNetworkName{
    self.sponsoredIconSize.constant = 15.f;
    if (gScreenSize == LVScreenSizeIpad){
        self.sponsoredIconSize.constant = 20.f;
    }
    self.sponsoredIconToLabel.constant = 6.f;
    
    // reset old Facebook Native Ad if need
    if ([self.curNativeAd isKindOfClass:[FBNativeAd class]]){
        FBNativeAd* curFBNativeAd = (FBNativeAd*)self.curNativeAd;
        if (curFBNativeAd)
            [curFBNativeAd unregisterView];
    }
    else{
        self.curNativeAd = nil;
    }
    
    // remove admobNativeView
    [self.nativeAdView removeFromSuperview];


    self.curNativeAd = nativeAd;
    self.curAdNetworkName = adNetworkName;
    self.sponsoredLabel.text = @"QC";
    
    self.coverAdsImageView.hidden = YES;
    self.adCoverMediaView.hidden = YES;
    
    if ([adNetworkName isEqualToString:@"mopub"])
    {
        self.coverAdsImageView.hidden = NO;
        
        MPNativeAd *curMobNativeAds = (MPNativeAd*)nativeAd;
        self.adChoicesView.hidden = YES;
        self.sponsoredLabel.hidden = NO;
        self.sponsoredIcon.hidden = NO;
        [(MPNativeAd*)curMobNativeAds setDelegate:self];
        self.titleAdsLabel.text = [((MPNativeAd*)curMobNativeAds).properties objectForKey:kAdTitleKey];
        self.descriptionAdsLabel.text = [((MPNativeAd*)curMobNativeAds).properties objectForKey:kAdTextKey];
        [self.installAppButton setTitle:[NSString stringWithFormat:@"%@",[[((MPNativeAd*)curMobNativeAds).properties objectForKey:kAdCTATextKey] uppercaseString]] forState:UIControlStateNormal];
        NSString *linkImage = [[((MPNativeAd*)curMobNativeAds).properties objectForKey:kAdMainImageKey] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [self.coverAdsImageView sd_setImageWithURL:[NSURL URLWithString:linkImage] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (!error && image) {
                self.coverAdsImageView.image = image;
                if (cacheType == SDImageCacheTypeNone) { // fade in only on first download
                    self.coverAdsImageView.alpha = 0.0;
                    [UIView animateWithDuration:0.3 animations:^{
                        self.coverAdsImageView.alpha = 1.0;
                    }];
                }
                else {
                    self.coverAdsImageView.alpha = 1.0;
                }
            }
        }];
        
        NSString *daaIconImageLoc = [((MPNativeAd*)self.curNativeAd).properties objectForKey:kAdDAAIconImageKey];
        if (daaIconImageLoc) {
            UIImage *daaIconImage = [UIImage imageNamed:daaIconImageLoc];
            self.sponsoredIcon.image = daaIconImage;
        }
        
        self.appImageView.image = [UIImage imageNamed:@"i_home_quangcao"];
        NSURL *imageIconUrl = [NSURL URLWithString:[[((MPNativeAd*)self.curNativeAd).properties objectForKey:kAdIconImageKey] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:imageIconUrl] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            self.appImageView.image = [UIImage imageWithData:data];
        }];
        
        self.sponsoredButton.hidden = NO;
        self.imageCoverButton.hidden = NO;
        [self.sponsoredButton addTarget:self action:@selector(DAAIconTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.installAppButton addTarget:self action:@selector(installNow:) forControlEvents:UIControlEventTouchUpInside];
        [self.imageCoverButton addTarget:self action:@selector(installNow:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if ([adNetworkName isEqualToString:@"facebook"])
    {
        self.adCoverMediaView.hidden = NO;
        
        self.sponsoredButton.hidden = YES;
        self.imageCoverButton.hidden = YES;
        self.adChoicesView.hidden = NO;
        self.sponsoredIcon.hidden = YES;
        self.sponsoredLabel.hidden = NO;
        self.sponsoredLabel.text = @"QC";
        
        [self.sponsoredButton removeTarget:self action:@selector(DAAIconTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.installAppButton removeTarget:self action:@selector(installNow:) forControlEvents:UIControlEventTouchUpInside];
        [self.imageCoverButton removeTarget:self action:@selector(installNow:) forControlEvents:UIControlEventTouchUpInside];
        
        FBNativeAd* curFBNativeAd = (FBNativeAd*)nativeAd;
        if (curFBNativeAd)
            [curFBNativeAd unregisterView];
        curFBNativeAd.delegate = self;
        self.titleAdsLabel.text = curFBNativeAd.title;
        self.descriptionAdsLabel.text = curFBNativeAd.body;
        [self.installAppButton setTitle:[NSString stringWithFormat:@"%@",[curFBNativeAd.callToAction uppercaseString]] forState:UIControlStateNormal];
        [curFBNativeAd.coverImage loadImageAsyncWithBlock:^(UIImage *image){
            self.coverAdsImageView.image = image;
        }];
        
        self.appImageView.image = [UIImage imageNamed:@"i_home_quangcao"];
        [curFBNativeAd.icon loadImageAsyncWithBlock:^(UIImage *image){
            self.appImageView.image = image;
        }];

//        if (curFBNativeAd.coverImage.url.absoluteString && curFBNativeAd.coverImage.url.absoluteString.length) {
            [self.adCoverMediaView setNativeAd:curFBNativeAd];
//        }
        
        if (fbAdChoiceView == nil) {
            fbAdChoiceView = [[FBAdChoicesView alloc] initWithNativeAd:curFBNativeAd expandable:NO];
            
            [self.adChoicesView addSubview:fbAdChoiceView];
            fbAdChoiceView.backgroundShown = NO;
            fbAdChoiceView.label.hidden = YES;
            //fbAdChoiceView.label.font = self.sponsoredLabel.font;
            //fbAdChoiceView.label.text = [@"Sponsored" uppercaseString];
            //fbAdChoiceView.label.textColor = RGB(153, 153, 153);
        }
        else{
            [fbAdChoiceView setNativeAd:curFBNativeAd];
            fbAdChoiceView.backgroundShown = NO;
            fbAdChoiceView.label.hidden = YES;
            //fbAdChoiceView.label.font = self.sponsoredLabel.font;
            //fbAdChoiceView.label.text = @"Sponsored";
            //fbAdChoiceView.label.textColor = RGB(153, 153, 153);
        }
        [fbAdChoiceView updateFrameFromSuperview];
        
        CGRect rect = fbAdChoiceView.frame;
        fbAdChoiceView.frame = CGRectMake(0 ,0, self.sponsoredIconSize.constant, self.sponsoredIconSize.constant);
//        [self setNeedsLayout];
//        [self layoutIfNeeded];
        [curFBNativeAd registerViewForInteraction:self withViewController:self.window.rootViewController withClickableViews:[NSArray arrayWithObjects:self.adCoverMediaView/*, self.appImageView, self.titleAdsLabel*/,self.installAppButton/*, self.descriptionAdsLabel*/, nil]];
        
    }
    else if([adNetworkName isEqualToString:@"admob"]){
        
        if ([self.curNativeAd isKindOfClass:[GADNativeAppInstallAd class]])
        {
            GADNativeAppInstallAd *admobNativeAds = (GADNativeAppInstallAd*)nativeAd;

            GADNativeAppInstallAdView *appInstallAdView =
            [[NSBundle mainBundle] loadNibNamed:@"LVHomeNativeAds_AdmobInstallapp" owner:nil options:nil]
            .firstObject;
            [self setAdView:appInstallAdView];
            
            // setup UI
            
            ((UIImageView *)appInstallAdView.iconView).layer.cornerRadius = self.iconAppSize.constant/2;
            ((UIImageView *)appInstallAdView.iconView).layer.masksToBounds = YES;

            ((UILabel *)appInstallAdView.headlineView).font = [UIFont fontWithName:getFontName(MODE_FONT_REGULAR,YES) size:GetFontSizeMaxFitIP6Plus(14.f)];
            ((UILabel *)appInstallAdView.bodyView).font = [UIFont fontWithName:getFontName(MODE_FONT_REGULAR,NO) size:GetFontSizeMaxFitIP6Plus(11.f)];
            ((UIButton *)appInstallAdView.callToActionView).titleLabel.font = [UIFont fontWithName:getFontName(MODE_FONT_BOLD,NO) size:GetFontSizeMaxFitIP6Plus(12.f)];
            [((UIButton *)appInstallAdView.callToActionView).titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
            
//            [((UIButton *)appInstallAdView.callToActionView) setImage:TintImageByConstantColor([UIImage imageNamed:@"ic_more"], UIColorFromRGB(25.f, 117.f, 209.f)) forState:UIControlStateNormal];
            
            ((UIButton *)appInstallAdView.callToActionView).transform = CGAffineTransformMakeScale(-1.0, 1.0);
            ((UIButton *)appInstallAdView.callToActionView).titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
            ((UIButton *)appInstallAdView.callToActionView).imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);

            // Associate the app install ad view with the app install ad object. This is required to make the
            // ad clickable.
            appInstallAdView.nativeAppInstallAd = admobNativeAds;
            
            // Populate the app install ad view with the app install ad assets.
            // Some assets are guaranteed to be present in every app install ad.
            ((UILabel *)appInstallAdView.headlineView).text = admobNativeAds.headline;
            ((UIImageView *)appInstallAdView.iconView).image = admobNativeAds.icon.image;
            ((UILabel *)appInstallAdView.bodyView).text = admobNativeAds.body;
            [((UIButton *)appInstallAdView.callToActionView)setTitle:[admobNativeAds.callToAction uppercaseString] forState:UIControlStateNormal];
            
            // Set để còn autolayout đc :(
            self.descriptionAdsLabel.text = admobNativeAds.body;

            
            // Some app install ads will include a video asset, while others do not. Apps can use the
            // GADVideoController's hasVideoContent property to determine if one is present, and adjust their
            // UI accordingly.
            
            // The UI for this controller constrains the image view's height to match the media view's
            // height, so by changing the one here, the height of both views are being adjusted.
            
            if (admobNativeAds.videoController.hasVideoContent) {
                // The video controller has content. Show the media view.
                appInstallAdView.mediaView.hidden = NO;
                appInstallAdView.imageView.hidden = YES;
                
                // This app uses a fixed width for the GADMediaView and changes its height to match the aspect
                // ratio of the video it displays.
                NSLayoutConstraint *heightConstraint =
                [NSLayoutConstraint constraintWithItem:appInstallAdView.mediaView
                                             attribute:NSLayoutAttributeHeight
                                             relatedBy:NSLayoutRelationEqual
                                                toItem:appInstallAdView.mediaView
                                             attribute:NSLayoutAttributeWidth
                                            multiplier:(1 / admobNativeAds.videoController.aspectRatio)
                                              constant:0];
                [appInstallAdView.mediaView addConstraint:heightConstraint];
                //                heightConstraint.active = YES;

                // By acting as the delegate to the GADVideoController, this ViewController receives messages
                // about events in the video lifecycle.
                admobNativeAds.videoController.delegate = self.rootViewController;
                
            } else {
                // If the ad doesn't contain a video asset, the first image asset is shown in the
                // image view. The existing lower priority height constraint is used.
                appInstallAdView.mediaView.hidden = YES;
                appInstallAdView.imageView.hidden = NO;
                
                GADNativeAdImage *firstImage = admobNativeAds.images.firstObject;
                ((UIImageView *)appInstallAdView.imageView).image = firstImage.image;
            }
            
            // In order for the SDK to process touch events properly, user interaction should be disabled.
            appInstallAdView.callToActionView.userInteractionEnabled = NO;

        }
        else if ([self.curNativeAd isKindOfClass:[GADNativeContentAd class]])
        {
            GADNativeContentAd *admobNativeAds = (GADNativeContentAd*)nativeAd;
            
            GADNativeContentAdView *contentAdView =
            [[NSBundle mainBundle] loadNibNamed:@"LVHomeNativeAds_AdmobContentAd" owner:nil options:nil].firstObject;
            [self setAdView:contentAdView];
            
            ((UIImageView *)contentAdView.logoView).layer.cornerRadius = self.iconAppSize.constant/2;
            ((UIImageView *)contentAdView.logoView).layer.masksToBounds = YES;

            ((UILabel *)contentAdView.headlineView).font = [UIFont fontWithName:getFontName(MODE_FONT_REGULAR,YES) size:GetFontSizeMaxFitIP6Plus(14.f)];
            ((UILabel *)contentAdView.bodyView).font = [UIFont fontWithName:getFontName(MODE_FONT_REGULAR,NO) size:GetFontSizeMaxFitIP6Plus(11.f)];
            ((UIButton *)contentAdView.callToActionView).titleLabel.font = [UIFont fontWithName:getFontName(MODE_FONT_BOLD,NO) size:GetFontSizeMaxFitIP6Plus(12.f)];
            [((UIButton *)contentAdView.callToActionView).titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
            
//            [((UIButton *)contentAdView.callToActionView) setImage:TintImageByConstantColor([UIImage imageNamed:@"ic_more"], UIColorFromRGB(25.f, 117.f, 209.f)) forState:UIControlStateNormal];
            
            ((UIButton *)contentAdView.callToActionView).transform = CGAffineTransformMakeScale(-1.0, 1.0);
             ((UIButton *)contentAdView.callToActionView).titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
              ((UIButton *)contentAdView.callToActionView).imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);

            // Associate the content ad view with the content ad object. This is required to make the ad
            // clickable.
            contentAdView.nativeContentAd = admobNativeAds;
            
            // Populate the content ad view with the content ad assets.
            // Some assets are guaranteed to be present in every content ad.
            ((UILabel *)contentAdView.headlineView).text = admobNativeAds.headline;
            ((UILabel *)contentAdView.bodyView).text = admobNativeAds.body;
            ((UIImageView *)contentAdView.imageView).image =
            ((GADNativeAdImage *)admobNativeAds.images.firstObject).image;
            ((UILabel *)contentAdView.advertiserView).text = admobNativeAds.advertiser;
            [((UIButton *)contentAdView.callToActionView)setTitle:[admobNativeAds.callToAction uppercaseString] forState:UIControlStateNormal];

            // Set để còn autolayout đc :(
            self.descriptionAdsLabel.text = admobNativeAds.body;

            // Other assets are not, however, and should be checked first.
            if (admobNativeAds.logo && admobNativeAds.logo.image) {
                ((UIImageView *)contentAdView.logoView).image = admobNativeAds.logo.image;
                contentAdView.logoView.hidden = NO;
            } else {
                contentAdView.logoView.hidden = YES;
            }
            
            // In order for the SDK to process touch events properly, user interaction should be disabled.
            contentAdView.callToActionView.userInteractionEnabled = NO;
        }

    }

    else if ([adNetworkName isEqualToString:@"ppclink"])
    {
        self.sponsoredIconSize.constant = 0.f;
        self.sponsoredIconToLabel.constant = 0.f;

        self.coverAdsImageView.hidden = NO;
        
        NSDictionary *ppclinkAds = (NSDictionary*)nativeAd;
        self.adChoicesView.hidden = YES;
        self.sponsoredLabel.hidden = NO;
        self.sponsoredIcon.hidden = YES;
        self.titleAdsLabel.text = [ppclinkAds objectForKey:kNotification_Title];
        self.descriptionAdsLabel.text = [ppclinkAds objectForKey:kNotification_Description];
        [self.installAppButton setTitle: [[ppclinkAds objectForKey:kNotification_TryNowText] uppercaseString] forState:UIControlStateNormal];
        NSString *linkImage = [ppclinkAds objectForKey:kNotification_AdRectangle];
        [self.coverAdsImageView sd_setImageWithURL:[NSURL URLWithString:linkImage] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (!error && image) {
                self.coverAdsImageView.image = image;
                if (cacheType == SDImageCacheTypeNone) { // fade in only on first download
                    self.coverAdsImageView.alpha = 0.0;
                    [UIView animateWithDuration:0.3 animations:^{
                        self.coverAdsImageView.alpha = 1.0;
                    }];
                }
                else {
                    self.coverAdsImageView.alpha = 1.0;
                }
            }
        }];
        
        NSString *linkIcon = [ppclinkAds objectForKey:kNotification_AdImage];
        [self.appImageView sd_setImageWithURL:[NSURL URLWithString:linkIcon] placeholderImage:[UIImage imageNamed:@"i_home_quangcao"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (!error && image) {
                self.appImageView.image = image;
                if (cacheType == SDImageCacheTypeNone) { // fade in only on first download
                    self.appImageView.alpha = 0.0;
                    [UIView animateWithDuration:0.3 animations:^{
                        self.appImageView.alpha = 1.0;
                    }];
                }
                else {
                    self.appImageView.alpha = 1.0;
                }
            }
        }];

        self.sponsoredButton.hidden = YES;
        //[self.sponsoredButton addTarget:self action:@selector(DAAIconTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.installAppButton addTarget:self action:@selector(installNow:) forControlEvents:UIControlEventTouchUpInside];
        self.imageCoverButton.hidden = NO;
        [self.imageCoverButton addTarget:self action:@selector(installNow:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [self setNeedsLayout];
    [self layoutIfNeeded];

}

#pragma mark Button On Click
- (void)installNow:(id)sender {
    if ([self.curAdNetworkName isEqualToString:@"mopub"]) {
        if([((MPNativeAd*)self.curNativeAd) respondsToSelector:@selector(adViewTapped)]){
            [((MPNativeAd*)self.curNativeAd) performSelector:@selector(adViewTapped) withObject:nil];
        }
    }
    else {
        
        NSDictionary *ppclinkAds = (NSDictionary*)self.curNativeAd;
        NSString *customURl = [NSString stringWithFormat:@"%@://",[ppclinkAds objectForKey:kNotification_AppId]];
        NSString *linkUrl = [ppclinkAds objectForKey:kNotification_URL];
        
        [g_vdConfigNotification userJustClickTryNowWithNotificationId:[ppclinkAds objectForKey:kNotification_ID] adImageType: ADIMAGETYPE_RECTANGLE];

        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:customURl]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURl]];
        }
        else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:linkUrl]];
        }
        


    }
}

- (void)DAAIconTapped:(id)sender
{
    if ([self.curAdNetworkName isEqualToString:@"mopub"]) {
        if ([[((MPNativeAd*)self.curNativeAd) performSelector:@selector(adAdapter) withObject:nil] respondsToSelector:@selector(displayContentForDAAIconTap)]) {
            [[((MPNativeAd*)self.curNativeAd) performSelector:@selector(adAdapter) withObject:nil] displayContentForDAAIconTap];
        }
    }
    else {
        NSDictionary *ppclinkAds = (NSDictionary*)self.curNativeAd;
        NSString *customURl = [NSString stringWithFormat:@"%@://",[ppclinkAds objectForKey:kNotification_AppId]];
        NSString *linkUrl = [ppclinkAds objectForKey:kNotification_URL];
        [g_vdConfigNotification userJustClickTryNowWithNotificationId:[ppclinkAds objectForKey:kNotification_ID] adImageType: ADIMAGETYPE_RECTANGLE];

        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:customURl]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURl]];
        }
        else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:linkUrl]];
        }
        
    }

}

- (UIViewController *)viewControllerForPresentingModalView
{
    return self.window.rootViewController;
}


#pragma mark - FACEBOOK Native Ad Handle

- (void)mediaViewDidLoad:(FBMediaView *)mediaView
{
    NSLog(@"mediaViewDidLoad");
}
- (void)nativeAd:(FBNativeAd *)nativeAd didFailWithError:(NSError *)error
{
    //adFBStatusLabel.text = @"Ad failed to load. Check console for details.";
    NSLog(@"Native ad failed to load with error: %@", error);
}

- (void)nativeAdDidClick:(FBNativeAd *)nativeAd
{
    NSLog(@"NativeAd Facebook clicked");
}

- (void)nativeAdDidFinishHandlingClick:(FBNativeAd *)nativeAd
{
    NSLog(@"Native ad facebook did finish click handling.");
}

- (void)nativeAdWillLogImpression:(FBNativeAd *)nativeAd
{
    NSLog(@"Native ad impression is being captured.");
}

+ (CGFloat) nativeViewHeight{
    if (gScreenSize == LVScreenSizeIpad) {
        return 550.f;
    }
    if (gScreenSize == LVScreenSizeIp4 || gScreenSize == LVScreenSizeIp5) {
        return 270.f;
    }
    return GetSpaceBetweenItemForIphone6Plus(320.f, 25.f);
}

- (CGSize) intrinsicContentSize {
    return CGSizeMake(self.bounds.size.width, CGRectGetMaxY(self.buttonView.frame));
}


- (void)setAdView:(UIView *)view {
    // Remove previous ad view.
    [self.nativeAdView removeFromSuperview];
    self.nativeAdView = view;
    
    // Add new ad view and set constraints to fill its container.
    [self addSubview:view];
    [self.nativeAdView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    NSDictionary *viewDictionary = NSDictionaryOfVariableBindings(_nativeAdView);
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_nativeAdView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:viewDictionary]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_nativeAdView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:viewDictionary]];
}

@end
