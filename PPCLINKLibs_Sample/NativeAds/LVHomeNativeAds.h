//
//  LVHomeNativeAds.h
//  Lichviet2016
//
//  Created by Quyet Nguyen on 8/30/16.
//  Copyright © 2016 company. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GADMediaView.h"
#import "GADNativeAppInstallAd.h"

@interface LVHomeNativeAds : UIView

@property (strong, nonatomic) UIViewController<GADVideoControllerDelegate> *rootViewController;
@property (strong, nonatomic) NSString *screenTrackAds;

- (void)setNativeAd:(NSObject *)nativeAd fromAdNetwork:(NSString*)adNetworkName;

@property (weak, nonatomic) IBOutlet UIView *lineTopView;

+ (CGFloat) nativeViewHeight;
- (CGSize) intrinsicContentSize;

@end
