//
//  PopupAppAd.h
//  Test2
//
//  Created by Huy Pham on 6/16/16.
//  Copyright © 2016 Huy Pham. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LVNativeAdsView;
@protocol LVNativeAdsViewDelegate <NSObject>

- (void)nativeAdsDismissView:(LVNativeAdsView*)nativeView;

@end

@interface LVNativeAdsView : UIView

@property (assign)  id<LVNativeAdsViewDelegate> delegate;
@property (nonatomic)   CGRect  mainFrame;
@property (strong, nonatomic) UIViewController *rootViewController;
@property (strong, nonatomic) NSString *screenTrackAds;

- (void)setNativeAd:(NSObject *)nativeAd fromAdNetwork:(NSString*)adNetworkName;

@end
