//
//  PopupAppAd.m
//  Test2
//
//  Created by Huy Pham on 6/16/16.
//  Copyright © 2016 Huy Pham. All rights reserved.
//

#import "LVNativeAdsView.h"
#import "FBMediaView.h"
#import "MPNativeAd.h"
#import "MPNativeAdDelegate.h"
#import "VDConfigNotification.h"
#import "MPMediationAdController.h"
#import "FBAdChoicesView.h"
#import "MPNativeAdAdapter.h"
#import "MPNativeAdConstants.h"
#import "MPMediationAdController.h"
#import "FBAdChoicesView.h"
#import "MPNativeAdAdapter.h"
#import "UIImageView+WebCache.h"
#import "Global.h"
#import "MPMediationAdController.h"

@interface LVNativeAdsView()<MPNativeAdDelegate, FBNativeAdDelegate>
{
    FBAdChoicesView *fbAdChoiceView;
}
@property (strong, nonatomic) NSObject      *curNativeAd;
@property (strong, nonatomic) NSString      *curAdNetworkName;

@property (weak, nonatomic) IBOutlet UIImageView *coverAdsImageView;
@property (weak, nonatomic) IBOutlet FBMediaView *adCoverMediaView;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *titleAdsLabel;
@property (weak, nonatomic) IBOutlet UILabel *respondAdsLabel;
@property (weak, nonatomic) IBOutlet UILabel *descAdsLabel;
@property (weak, nonatomic) IBOutlet UIButton *installButton;
@property (weak, nonatomic) IBOutlet UIButton *installAppFromCover;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightContentViewContraints;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *itemFirstLeadingContraints;

@property (weak, nonatomic) IBOutlet UIButton *respondButton;
@property (weak, nonatomic) IBOutlet UIImageView *respondAdsImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightFBChoicesViewContraints;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthFBChoicesViewContraints;
@property (weak, nonatomic) IBOutlet UIView  *fbChoicesView;


@end

@implementation LVNativeAdsView

- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _mainFrame = self.bounds;
        [self loadViewFromNib];
    }
    return self;
}

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _mainFrame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        [self loadViewFromNib];
    }
    return self;
}

- (void)loadViewFromNib{
    UIView *contentView = [[[NSBundle mainBundle] loadNibNamed:@"LVNativeAdsView" owner:self options:nil] objectAtIndex:0];
    contentView.frame = _mainFrame;
    [self addSubview:contentView];
    [self configureUI];
}

- (void) configureUI {
    
    if (gScreenSize == LVScreenSizeIpad){
        self.heightFBChoicesViewContraints.constant = 50.f;
        self.widthFBChoicesViewContraints.constant = 200.f;
    }
    
    self.heightContentViewContraints.constant = _mainFrame.size.height;
    self.itemFirstLeadingContraints.constant = GetSpaceBetweenItemForIphone6Plus(12.f, 0.f);
    self.contentView.layer.cornerRadius = 8;
    self.contentView.layer.masksToBounds = NO;
    
    self.contentView.layer.shadowOffset = CGSizeZero;
    self.contentView.layer.shadowRadius = 10;
    self.contentView.layer.shadowOpacity = 0.3;
    self.contentView.layer.shadowColor = [UIColor blackColor].CGColor;
    
    CGRect maskLayerFrame = CGRectMake(0, 0, _mainFrame.size.width, self.coverAdsImageView.bounds.size.height);
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:maskLayerFrame
                                                   byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight)
                                                         cornerRadii:CGSizeMake(8, 8)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = maskLayerFrame;
    maskLayer.path = maskPath.CGPath;
    self.coverAdsImageView.layer.mask = maskLayer;
    
    [self.installButton.layer setBorderWidth:1.0];
    [self.installButton.layer setBorderColor:[UIColor colorWithRed:74.0/255.0 green:144.0/255.0 blue:226.0/255.0 alpha:1.0].CGColor];
    self.installButton.layer.cornerRadius = 5;
    
    self.titleAdsLabel.font = [UIFont fontWithName:getFontName(MODE_FONT_BOLD,YES) size:GetFontSizeMaxFitIP6Plus(14.f)];
    self.descAdsLabel.font = [UIFont fontWithName:getFontName(MODE_FONT_REGULAR,NO) size:GetFontSizeMaxFitIP6Plus(12.f)];
    self.respondAdsLabel.font = [UIFont fontWithName:getFontName(MODE_FONT_MEDIUM,NO) size:GetFontSizeMaxFitIP6Plus(10.f)];
    self.installButton.titleLabel.font = [UIFont fontWithName:getFontName(MODE_FONT_MEDIUM,NO) size:GetFontSizeMaxFitIP6Plus(12.f)];
    [self.installButton.titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];

}

- (void)setNativeAd:(NSObject *)nativeAd fromAdNetwork:(NSString *)adNetworkName{
    
    // reset old Facebook Native Ad if need
    if ([self.curNativeAd isKindOfClass:[FBNativeAd class]]){
        FBNativeAd* curFBNativeAd = (FBNativeAd*)self.curNativeAd;
        if (curFBNativeAd)
            [curFBNativeAd unregisterView];
    }
    else{
        self.curNativeAd = nil;
    }

    self.curNativeAd = nativeAd;
    self.curAdNetworkName = adNetworkName;
    self.respondAdsLabel.text = [@"Sponsored" uppercaseString];

    self.coverAdsImageView.hidden = YES;
    self.adCoverMediaView.hidden = YES;
    
    if ([adNetworkName isEqualToString:@"mopub"])
    {
        self.coverAdsImageView.hidden = NO;

        MPNativeAd *curMobNativeAds = (MPNativeAd*)nativeAd;
        self.respondAdsLabel.hidden = NO;
        self.fbChoicesView.hidden = YES;
        self.respondAdsImageView.hidden = NO;
        [(MPNativeAd*)curMobNativeAds setDelegate:self];
        self.titleAdsLabel.text = [((MPNativeAd*)curMobNativeAds).properties objectForKey:kAdTitleKey];
        self.descAdsLabel.text = [((MPNativeAd*)curMobNativeAds).properties objectForKey:kAdTextKey];
        [self.installButton setTitle:[NSString stringWithFormat:@"  %@  ",[[((MPNativeAd*)curMobNativeAds).properties objectForKey:kAdCTATextKey] uppercaseString]] forState:UIControlStateNormal];
        NSString *linkImage = [[((MPNativeAd*)curMobNativeAds).properties objectForKey:kAdMainImageKey] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [self.coverAdsImageView sd_setImageWithURL:[NSURL URLWithString:linkImage] placeholderImage:nil options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];
        
        NSString *daaIconImageLoc = [((MPNativeAd*)self.curNativeAd).properties objectForKey:kAdDAAIconImageKey];
        if (daaIconImageLoc) {
            UIImage *daaIconImage = [UIImage imageNamed:daaIconImageLoc];
            self.respondAdsImageView.image = daaIconImage;
        }
        
        self.respondButton.hidden = NO;
        self.installAppFromCover.hidden = NO;
        [self.respondButton addTarget:self action:@selector(DAAIconTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.installButton addTarget:self action:@selector(installNow:) forControlEvents:UIControlEventTouchUpInside];
        [self.installAppFromCover addTarget:self action:@selector(installNow:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if ([adNetworkName isEqualToString:@"facebook"])
    {
        self.adCoverMediaView.hidden = NO;

        self.respondButton.hidden = YES;
        self.installAppFromCover.hidden = YES;
        self.fbChoicesView.hidden = NO;
        self.respondAdsImageView.hidden = YES;
        self.respondAdsLabel.hidden = YES;
        
        [self.respondButton removeTarget:self action:@selector(DAAIconTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.installButton removeTarget:self action:@selector(installNow:) forControlEvents:UIControlEventTouchUpInside];
        [self.installAppFromCover removeTarget:self action:@selector(installNow:) forControlEvents:UIControlEventTouchUpInside];
        
        FBNativeAd* curFBNativeAd = (FBNativeAd*)nativeAd;
        if (curFBNativeAd)
            [curFBNativeAd unregisterView];
        curFBNativeAd.delegate = self;
        self.titleAdsLabel.text = curFBNativeAd.title;
        self.descAdsLabel.text = curFBNativeAd.body;
        [self.installButton setTitle:[NSString stringWithFormat:@"  %@  ",[curFBNativeAd.callToAction uppercaseString]] forState:UIControlStateNormal];
        [curFBNativeAd.coverImage loadImageAsyncWithBlock:^(UIImage *image){
            self.coverAdsImageView.image = image;
        }];
        
        
        [self.adCoverMediaView setNativeAd:curFBNativeAd];

        if (fbAdChoiceView == nil) {
            fbAdChoiceView = [[FBAdChoicesView alloc] initWithNativeAd:curFBNativeAd expandable:NO];
            
            [self.fbChoicesView addSubview:fbAdChoiceView];
            fbAdChoiceView.backgroundShown = NO;
            fbAdChoiceView.label.font = self.respondAdsLabel.font;
            fbAdChoiceView.label.text = [@"Sponsored" uppercaseString];
            fbAdChoiceView.label.textColor = RGB(153, 153, 153);
        }else{
            [fbAdChoiceView setNativeAd:curFBNativeAd];
            fbAdChoiceView.backgroundShown = NO;
            fbAdChoiceView.label.font = self.respondAdsLabel.font;
            fbAdChoiceView.label.text = [@"Sponsored" uppercaseString];
            fbAdChoiceView.label.textColor = RGB(153, 153, 153);
        }
        [fbAdChoiceView updateFrameFromSuperview];
        
        CGRect rect = fbAdChoiceView.frame;
//        if (gScreenSize == LVScreenSizeIpad) {
//            fbAdChoiceView.frame = CGRectMake(self.widthFBChoicesViewContraints.constant/2 - rect.size.width*0.8f ,0, self.widthFBChoicesViewContraints.constant, rect.size.height);
//        }
//        else{
            fbAdChoiceView.frame = CGRectMake(0 ,0, self.widthFBChoicesViewContraints.constant, rect.size.height);
//        }
        self.heightFBChoicesViewContraints.constant = rect.size.height;
        [self layoutIfNeeded];
        [curFBNativeAd registerViewForInteraction:self withViewController:LVAppDelegate.window.rootViewController withClickableViews:[NSArray arrayWithObjects:self.adCoverMediaView, self.titleAdsLabel,self.installButton, self.descAdsLabel, nil]];
        
    }
    else if ([adNetworkName isEqualToString:@"ppclink"])
    {
        self.coverAdsImageView.hidden = NO;
        
        NSDictionary *ppclinkAds = (NSDictionary*)nativeAd;
        self.fbChoicesView.hidden = YES;
        self.respondAdsLabel.hidden = NO;
        self.respondAdsImageView.hidden = NO;
        self.titleAdsLabel.text = [ppclinkAds objectForKey:kNotification_Title];
        self.descAdsLabel.text = [ppclinkAds objectForKey:kNotification_Description];
        [self.installButton setTitle: [[ppclinkAds objectForKey:kNotification_TryNowText] uppercaseString] forState:UIControlStateNormal];
        NSString *linkImage = [ppclinkAds objectForKey:kNotification_AdRectangle];
        [self.coverAdsImageView sd_setImageWithURL:[NSURL URLWithString:linkImage] placeholderImage:nil options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];
        
//        NSString *linkIcon = [ppclinkAds objectForKey:kNotification_AdImage];
//        [self.respondAdsImageView sd_setImageWithURL:[NSURL URLWithString:linkIcon] placeholderImage:nil options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//            
//        }];
        
        self.respondButton.hidden = NO;
        self.installAppFromCover.hidden = NO;
        [self.respondButton addTarget:self action:@selector(DAAIconTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.installButton addTarget:self action:@selector(installNow:) forControlEvents:UIControlEventTouchUpInside];
        [self.installAppFromCover addTarget:self action:@selector(installNow:) forControlEvents:UIControlEventTouchUpInside];
    }

}

- (IBAction)installOnClick:(id)sender {
    
}

- (IBAction)closeOnClick:(id)sender {
    if ([self.delegate respondsToSelector:@selector(nativeAdsDismissView:)]) {
        [self.delegate nativeAdsDismissView:self];
    }
}

#pragma mark Button On Click
- (void)installNow:(id)sender {
    sendTrackEventValue(self.screenTrackAds, @"Tapped", @"Native Ads");
    
    if ([self.curAdNetworkName isEqualToString:@"mopub"]) {
        if([((MPNativeAd*)self.curNativeAd) respondsToSelector:@selector(adViewTapped)]){
            [((MPNativeAd*)self.curNativeAd) performSelector:@selector(adViewTapped) withObject:nil];
        }
    }
    else {
        NSDictionary *ppclinkAds = (NSDictionary*)self.curNativeAd;
        NSString *customURl = [NSString stringWithFormat:@"%@://",[ppclinkAds objectForKey:kNotification_AppId]];
        NSString *linkUrl = [ppclinkAds objectForKey:kNotification_URL];
        [g_vdConfigNotification userJustClickTryNowWithNotificationId:[ppclinkAds objectForKey:kNotification_ID] adImageType: ADIMAGETYPE_RECTANGLE];

        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:customURl]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURl]];
        }
        else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:linkUrl]];
        }
    }

}

- (void)DAAIconTapped:(id)sender
{
    if ([self.curAdNetworkName isEqualToString:@"mopub"]) {
        if ([[((MPNativeAd*)self.curNativeAd) performSelector:@selector(adAdapter) withObject:nil] respondsToSelector:@selector(displayContentForDAAIconTap)]) {
            [[((MPNativeAd*)self.curNativeAd) performSelector:@selector(adAdapter) withObject:nil] displayContentForDAAIconTap];
        }
    }
    else {
        NSDictionary *ppclinkAds = (NSDictionary*)self.curNativeAd;
        NSString *customURl = [NSString stringWithFormat:@"%@://",[ppclinkAds objectForKey:kNotification_AppId]];
        NSString *linkUrl = [ppclinkAds objectForKey:kNotification_URL];
        [g_vdConfigNotification userJustClickTryNowWithNotificationId:[ppclinkAds objectForKey:kNotification_ID] adImageType: ADIMAGETYPE_RECTANGLE];

        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:customURl]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURl]];
        }
        else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:linkUrl]];
        }
    }

}

- (UIViewController *)viewControllerForPresentingModalView
{
    return LVAppDelegate.window.rootViewController;
}

#pragma FACEBOOK Native Ad Handle

- (void)mediaViewDidLoad:(FBMediaView *)mediaView
{
    NSLog(@"mediaViewDidLoad");
}
- (void)nativeAd:(FBNativeAd *)nativeAd didFailWithError:(NSError *)error
{
    //adFBStatusLabel.text = @"Ad failed to load. Check console for details.";
    NSLog(@"Native ad failed to load with error: %@", error);
}

- (void)nativeAdDidClick:(FBNativeAd *)nativeAd
{
    sendTrackEventValue(self.screenTrackAds, @"Tapped", @"Native Ads");
    NSLog(@"NativeAd Facebook clicked");
}

- (void)nativeAdDidFinishHandlingClick:(FBNativeAd *)nativeAd
{
    NSLog(@"Native ad facebook did finish click handling.");
}

- (void)nativeAdWillLogImpression:(FBNativeAd *)nativeAd
{
    NSLog(@"Native ad impression is being captured.");
}

@end
