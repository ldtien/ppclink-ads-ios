//
//  TheSecondMainViewController.m
//  Mopub_Sample
//
//  Created by PPCLINK on 2/21/14.
//  Copyright (c) 2014 ppclink. All rights reserved.
//

#import "TheSecondMainViewController.h"
#import "PLNativeAdView.h"
//#import "VDUIRemoveAdsViewController.h"
#import  <QuartzCore/QuartzCore.h>
#import "VDUtilities.h"

//#import "V4VCBonusManager.h"

@interface TheSecondMainViewController ()
{
    UIView *baseView;
    LVHomeNativeAds *nativeAdView;
  //  VDUIRemoveAdsViewController* removeAdsViewController;
}

@end

@implementation TheSecondMainViewController

- (id)init
{
    self = [super init];
    if (self)
    {
        self.title = [NSString stringWithFormat:@"PPCLINK ADS %@", [VDConfigNotification getProductBuildVersion]];
        [self initNotifications];
    }
    return self;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}


- (void) initNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNotifyInterstitialCheckAvaibilityStatus) name:kNotifyInterstitialCheckAvaibilityStatus object:nil];
#ifndef __PPCLINKAds_NativeOnly__
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onInterstitialAdDidClick:) name:kInterstitialAdDidClick object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFinishVideoAdWatching:) name:kFinishVideoAdWatching object:nil];
#endif
    
    
}

#ifndef __PPCLINKAds_NativeOnly__
- (void)onInterstitialAdDidClick:(NSNotification *)notify
{
    if ([notify userInfo])  // Click for bonus: trường hợp click quảng cáo để nhận thưởng
    {
        NSDictionary* dictBonusInfo = [notify userInfo];
        int nBonusAdFreeHours = [[dictBonusInfo objectForKey:kNotification_NumberOfAdFreeHours] intValue];
        [g_vdConfigNotification addAdFreeHours:nBonusAdFreeHours];
        // Khi user dc tặng Ad-Free time, chỉ can gọi hàm ẩn quảng cáo banner luôn ở đây, các lúc khác (khi mở app,...) bản thân MPMediationAdController tự check và ẩn rồi
        [[MPMediationAdController sharedManager] hideBannerAd];
        
        //[removeAdsViewController updateAdFreeHours];
        
#if __VIETNAMESE__
        UIAlertView *al = [[UIAlertView alloc] initWithTitle:@"Chúc mừng" message:[NSString stringWithFormat:@"Bạn được tặng %d giờ không phải xem quảng cáo.", nBonusAdFreeHours] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
#else
        UIAlertView *al = [[UIAlertView alloc] initWithTitle:@"Congratulation" message:[NSString stringWithFormat:@"You have been rewarded %d hours use without Ads. Thank you!", nBonusAdFreeHours] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
#endif
        [al show];
    }
    else  // Click quảng cáo bình thường...
    {
    }
    
}

- (void)onFinishVideoAdWatching:(NSNotification *)notify
{
    if (![(NSNumber*)[notify object] boolValue])
        return;
    
    if ([notify userInfo])  // Xem xong video để nhận thưởng...
    {
        NSDictionary* dictBonusInfo = [notify userInfo];
        int nBonusAdFreeHours = [[dictBonusInfo objectForKey:kNotification_NumberOfAdFreeHours] intValue];
        [g_vdConfigNotification addAdFreeHours:nBonusAdFreeHours];
        // Khi user dc tặng Ad-Free time, chỉ can gọi hàm ẩn quảng cáo banner luôn ở đây, các lúc khác (khi mở app,...) bản thân MPMediationAdController tự check và ẩn rồi
        [[MPMediationAdController sharedManager] hideBannerAd];
       // [removeAdsViewController updateAdFreeHours];
#if __VIETNAMESE__
        UIAlertView *al = [[UIAlertView alloc] initWithTitle:@"Chúc mừng" message:[NSString stringWithFormat:@"Bạn được tặng %d giờ không phải xem quảng cáo.", nBonusAdFreeHours] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
#else
        UIAlertView *al = [[UIAlertView alloc] initWithTitle:@"Congratulation" message:[NSString stringWithFormat:@"You have been rewarded %d hours use without Ads. Thank you!", nBonusAdFreeHours] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
#endif
        [al show];
    }
    else  // Xem video bình thường....
    {}
}
#endif

- (void)viewDidLoad
{
//    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
//    loginButton.center = self.view.center;
//    [self.view addSubview:loginButton];
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadView
{
    [super loadView];
    
    CGRect mainFrame = self.view.frame;
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainFrame.size.width, mainFrame.size.height) style:UITableViewStylePlain];
    [self.view addSubview:_tableView];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    //[_tableView release];
    
    [self initMPMediationAdController];
}


- (void) initMPMediationAdController
{
#ifndef __PPCLINKAds_NativeOnly__
    [MPMediationAdController sharedManager].rootViewControllerBannerAd = self;
    [MPMediationAdController sharedManager].rootViewControllerFullscreenAd = self;
    //[[MPMediationAdController sharedManager] initNativeAd];
    //[MPMediationAdController sharedManager].nativeAdDelegate = self;
    
    [self.view addSubview:[MPMediationAdController sharedManager].bannerAdView];
    CGRect mainFrame = self.view.frame;
    CGSize bannerAdSize = [[MPMediationAdController sharedManager] getAdBannerCGSize];
    [MPMediationAdController sharedManager].bannerAdView.frame = CGRectMake((mainFrame.size.width-bannerAdSize.width)/2, mainFrame.size.height - bannerAdSize.height - 44, bannerAdSize.width, bannerAdSize.height);
    [[MPMediationAdController sharedManager] showBannerAd];
    // Start loop to update interstitial ad status
    [[MPMediationAdController sharedManager] startTimerCheckingInterstitialAvailability];
    //[[MPMediationAdController sharedManager] setProductType:PT_PAID];
#endif
    
    [[MPMediationAdController sharedManager] initNativeAd];
}

- (void) onV4VcChecking
{
    static int nCounterTime = 0;
    nCounterTime++;
    
    NSLog(@"Timer: %d",nCounterTime);
    [_tableView reloadData];
}

#pragma mark - V4VC Notifications
- (void) onNotifyV4VCZoneLoading
{
    [_tableView reloadData];
}

- (void) onNotifyV4VCZoneReady
{
    [_tableView reloadData];
}

- (void) onNotifyV4VCZoneOff
{
    [_tableView reloadData];
}

- (void) onNotifyStartV4VCWatching
{
    [_tableView reloadData];
}

- (void) onNotifyFinishV4VCWatching:(NSNotification*)notify
{
    [_tableView reloadData];
}

- (void) onNotifyCurrentBonusPointHasChanged
{
    [_tableView reloadData];
}

#pragma mark - Intertitial Ads Notifications

- (void) onNotifyInterstitialCheckAvaibilityStatus
{
    [_tableView reloadData];
}


#pragma mark - Table view Datasource
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *strCellTag = @"CellTag";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:strCellTag];
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:strCellTag];
    }
    
    NSString* sCurNetworkName = nil;
    switch (indexPath.row)
    {
#ifndef __PPCLINKAds_NativeOnly__
        case 0:
            cell.textLabel.text = @"Banner Ad";
            if ([[MPMediationAdController sharedManager] bBannerAdIsShowing])
                cell.detailTextLabel.text = [NSString stringWithFormat:@"Running %@", [[MPMediationAdController sharedManager] currentRunningBannerAd]];
            else
                cell.detailTextLabel.text = @"Stopped";
            break;
            
        case 1:
            cell.textLabel.text = @"Force show Interstitial ALL";
            sCurNetworkName = [[MPMediationAdController sharedManager] isInterstitialAdAvailable:INTERSTITIAL_AD_TYPE_ALL];
            if (!sCurNetworkName)
                cell.detailTextLabel.text = @"Loading...";
            else
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ Ready to show!", sCurNetworkName];
            break;
        
        case 2:
            cell.textLabel.text = @"Force show Interstitial IMAGE ONLY";
            sCurNetworkName = [[MPMediationAdController sharedManager] isInterstitialAdAvailable:INTERSTITIAL_AD_TYPE_IMAGE_ONLY];
            if (!sCurNetworkName)
                cell.detailTextLabel.text = @"Loading...";
            else
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ Ready to show!", sCurNetworkName];
            break;
            
        case 4:
            cell.textLabel.text = @"Show Interstitial constraint server.";
            break;
        
#ifndef __PPCLINKAds_Lite__
        case 3:
            cell.textLabel.text = @"Force show Interstitial VIDEO ONLY";
            sCurNetworkName = [[MPMediationAdController sharedManager] isInterstitialAdAvailable:INTERSTITIAL_AD_TYPE_VIDEO_ONLY];
            if (!sCurNetworkName)
                cell.detailTextLabel.text = @"Loading...";
            else
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ Ready to show!", sCurNetworkName];
            break;
        case 7:
        {
            cell.textLabel.text = @"Show reward video";
            NSString* sRewardNetwork = [[MPMediationAdController sharedManager] isRewardedVideoAvailable];
            if (!sRewardNetwork)
                cell.detailTextLabel.text = @"Loading...";
            else
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ ready to show!", sRewardNetwork];
            break;
        }
#endif
        case 6:
            cell.textLabel.text = @"Remove Ads";
            break;
#endif
            
        case 5:
            
            if (nativeAdView && !nativeAdView.hidden)
                cell.textLabel.text = @"Hide Native Ad";
            else
                cell.textLabel.text = @"Show Native Ad";
//            sCurNetworkName = [[MPMediationAdController sharedManager] getCurrentNativeAdNetworkName];
//            if (sCurNetworkName)
//                cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ Ready to show!", sCurNetworkName];
//            else
//                cell.detailTextLabel.text = @"Loading...";
            break;

        default:
            break;
    }
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Did select row %d",indexPath.row);
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row)
    {
#ifndef __PPCLINKAds_NativeOnly__
        case 0:
            if ([[MPMediationAdController sharedManager] bBannerAdIsShowing])
                [self hideBannerAd];
            else
                [self showBannerAd];
            break;
            
        case 1:
            //[[MPMediationAdController sharedManager] showInterstitialAd:INTERSTITIAL_AD_TYPE_ALL forceShow:YES];
            [[MPMediationAdController sharedManager] showInterstitialAd:INTERSTITIAL_AD_TYPE_ALL forceShow:YES
            
            success:^(MPMediationAdController *mediationAdController)
             {
                 mediationAdController.interstitialAdClose= ^{
                     NSLog(@"interstitialAdClose");
                 };
                 
                 mediationAdController.interstitialAdClick = ^{
                     NSLog(@"interstitialAdClick");
                 };
                 
//                 mediationAdController.videoAdWatchCompletely = ^(NSDictionary *dictBonusInfo){
//                     NSLog(@"videoAdWatchCompletely: %@", dictBonusInfo);
//                 };
             }
             
            failure:^(NSString *error)
             {
                 NSLog(@"%@", error);
             }];
            
            break;
        
        case 2:
            [[MPMediationAdController sharedManager] showInterstitialAd:INTERSTITIAL_AD_TYPE_IMAGE_ONLY forceShow:YES];
            break;
            
#ifndef __PPCLINKAds_Lite__
        case 3:
            //[[MPMediationAdController sharedManager] showInterstitialAd:INTERSTITIAL_AD_TYPE_VIDEO_ONLY forceShow:YES];
            [[MPMediationAdController sharedManager] showInterstitialAd:INTERSTITIAL_AD_TYPE_VIDEO_ONLY forceShow:YES
             
                                                                success:^(MPMediationAdController *mediationAdController)
             {
                                  mediationAdController.interstitialAdClose= ^{
                                      NSLog(@"interstitialAdClose");
                                  };
                 
                 mediationAdController.interstitialAdClick = ^{
                     NSLog(@"interstitialAdClick");
                 };
//                 mediationAdController.videoAdWatchCompletely = ^(NSDictionary *dictBonusInfo){
//                     NSLog(@"videoAdWatchCompletely: %@", dictBonusInfo);
//                 };
             }
             
            failure:^(NSString *error)
             {
                 NSLog(@"%@", error);
             }];

            
            break;
         
        case 7:
        {

            [[MPMediationAdController sharedManager] showRewardedVideoWithSuccessReward:^(NSDictionary *rewardInfo)
             {
                 NSLog(@"You got reward %d %@", [[rewardInfo objectForKey:@"reward"] intValue], [rewardInfo objectForKey:@"adnetworkname"]);
                 
                 UIAlertView *al = [[UIAlertView alloc] initWithTitle:@"Congratulation!" message:[NSString stringWithFormat:@"You got reward %d %@", [[rewardInfo objectForKey:@"reward"] intValue], [rewardInfo objectForKey:@"adnetworkname"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                 [al show];
                 
             }];

            break;
        }
#endif
        case 4:
            [[MPMediationAdController sharedManager] showInterstitialAd:INTERSTITIAL_AD_TYPE_ALL forceShow:NO];
            break;
#endif
        case 5:
            [self startShowingNativeAd];
            break;
            
//        case 6:
//        {
//            removeAdsViewController = [[VDUIRemoveAdsViewController alloc] init];
//            //UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:removeAdsViewController];
//            [self.navigationController pushViewController:removeAdsViewController animated:YES];
//            break;
//        }

                default:
            break;
    }
}

#ifndef __PPCLINKAds_NativeOnly__
- (void) showBannerAd
{
    [[MPMediationAdController sharedManager] showBannerAd:^(MPMediationAdController *mediationAdController, BOOL bShowResult) {
        NSLog(@"Banner Show Result = %@", bShowResult ? @"SUCCESS":@"FAIL");
    }];
    
    [_tableView reloadData];
    
    [self.view bringSubviewToFront:[MPMediationAdController sharedManager].bannerAdView];
}

- (void) hideBannerAd
{
    [[MPMediationAdController sharedManager] hideBannerAd];
    
    [_tableView reloadData];
}

#endif
#pragma mark - Native Mopub Ad
- (void) startShowingNativeAd
{
    if (!nativeAdView)
    {
        CGRect baseViewFrame;
        
        if ([VDUtilities bIsIOS7Version])
        {
            baseViewFrame = CGRectMake( self.view.frame.size.width/2 - NATIVE_AD_SQUARE_SIZE.width/2, self.view.frame.size.height - NATIVE_AD_SQUARE_SIZE.height - 50, NATIVE_AD_SQUARE_SIZE.width, NATIVE_AD_SQUARE_SIZE.height);
        }
        else
        {
            baseViewFrame = CGRectMake( self.view.frame.size.width/2 - NATIVE_AD_SQUARE_SIZE.width/2, self.view.frame.size.height - NATIVE_AD_SQUARE_SIZE.height - 50, NATIVE_AD_SQUARE_SIZE.width, NATIVE_AD_SQUARE_SIZE.height);
        }
        
        nativeAdView = [[LVHomeNativeAds alloc] initWithFrame:baseViewFrame];
        nativeAdView.layer.cornerRadius = 2.0;
        nativeAdView.layer.borderColor = [UIColor grayColor].CGColor;
        nativeAdView.layer.borderWidth = 1.0;
        nativeAdView.rootViewController = self;
        [self.view addSubview:nativeAdView];
    }
    else
        nativeAdView.hidden = !nativeAdView.hidden;
    
    if (!nativeAdView.hidden)
    {
        NSDictionary* dictNativeAdInfo = [[MPMediationAdController sharedManager] getNativeAdInfoToShow];
        if (dictNativeAdInfo)
        {
            [nativeAdView setNativeAd:[dictNativeAdInfo objectForKey:kNativeAdInfo_Object] fromAdNetwork:[dictNativeAdInfo objectForKey:kNativeAdInfo_NetworkName]];
        }
        
    }
    
    [_tableView reloadData];
}


/*
 Được gọi khi quảng cáo native đã được load về thành công
 */
//- (void) nativeAdDidLoad:(NSObject *)nativeAd fromAdNetwork:(NSString *)adNetworkName
//{
//    if (!nativeAdView)
//    {
//        CGRect baseViewFrame;
//        
//        if ([VDUtilities bIsIOS7Version])
//        {
//            baseViewFrame = CGRectMake( self.view.frame.size.width/2 - NATIVE_AD_SQUARE_SIZE.width/2, self.view.frame.size.height - NATIVE_AD_SQUARE_SIZE.height - [[MPMediationAdController sharedManager] getAdBannerCGSize].height, NATIVE_AD_SQUARE_SIZE.width, NATIVE_AD_SQUARE_SIZE.height);
//        }
//        else
//        {
//            baseViewFrame = CGRectMake( self.view.frame.size.width/2 - NATIVE_AD_SQUARE_SIZE.width/2, self.view.frame.size.height - NATIVE_AD_SQUARE_SIZE.height - [[MPMediationAdController sharedManager] getAdBannerCGSize].height, NATIVE_AD_SQUARE_SIZE.width, NATIVE_AD_SQUARE_SIZE.height);
//        }
//        
//        nativeAdView = [[PLNativeAdView alloc] initWithFrame:baseViewFrame];
//        nativeAdView.layer.cornerRadius = 2.0;
//        nativeAdView.layer.borderColor = [UIColor grayColor].CGColor;
//        nativeAdView.layer.borderWidth = 1.0;
//        nativeAdView.rootViewController = self;
//        nativeAdView.hidden = YES;
//        [self.view addSubview:nativeAdView];
//    }
//    
//    [nativeAdView setNativeAd:nativeAd fromAdNetwork:adNetworkName];
//}


@end
