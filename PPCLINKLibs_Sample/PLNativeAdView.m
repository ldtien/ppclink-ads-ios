//
//  PLNativeAdView.m
//  PPCLINKLibs_Sample
//
//  Created by Do Lam on 07/29/15.
//  Copyright (c) 2015 ppclink. All rights reserved.
//

#import "PLNativeAdView.h"
#import "MPMediationAdController.h"

@interface PLNativeAdView()
{
    CGRect mainFrame;
    UIView *baseViewSquare;
    
    NSObject* curNativeAd;
    
    UIImageView *adIconImageView;
    UILabel *adTitleLabel;
    UILabel *adBodyLabel;
    UIImageView *adCoverImageView;
    UIImageView *daaImageView;
    UIButton *adCoverImageTapHandleButton;
    UIButton *adCallToActionButton;
    
//Addition View for facebook only
    UILabel *adFBStatusLabel;
    //FBMediaView *adFBCoverMediaView;
    UILabel *adFBSocialContextLabel;
    UIView *adFBStarRatingView;
    UILabel *adFBSponsoredLabel;
}

@end
@implementation PLNativeAdView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        mainFrame = frame;
        self.rootViewController = nil;
        [self initComponentViews];
    }
    return self;
}

- (void) initComponentViews
{
    int nIconWidth = 80;
    int nDescriptionHeight = 100;
    
    int nImageViewWidth = mainFrame.size.width;
    int nImageViewHeight = nImageViewWidth/2;
    
    int nActionButtonWidth = 200;
    int nActionButtonHeight = 30;
    
    int nMargin = 5;
    
    self.backgroundColor = [UIColor whiteColor];
    
    adIconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(nMargin, nMargin, nIconWidth - nMargin*2, nIconWidth - nMargin*2)];
    adIconImageView.backgroundColor = [UIColor redColor];
    [self addSubview:adIconImageView];
    //[adIconImageView release];
    
    adTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(nIconWidth, nMargin, mainFrame.size.width - nIconWidth, nIconWidth - nMargin*2)];
    adTitleLabel.textAlignment = NSTextAlignmentLeft;
    adTitleLabel.font = [UIFont boldSystemFontOfSize:18];
    adTitleLabel.textColor = [UIColor blackColor];
    adTitleLabel.text = @"Title Label";
    adTitleLabel.numberOfLines = 2;
    [self addSubview:adTitleLabel];
    //[adTitleLabel release];
    
    adBodyLabel = [[UILabel alloc] initWithFrame:CGRectMake(nMargin, nIconWidth , mainFrame.size.width - nMargin*2, nDescriptionHeight)];
    adBodyLabel.textAlignment = NSTextAlignmentLeft;
    adBodyLabel.font = [UIFont boldSystemFontOfSize:14];
    adBodyLabel.textColor = [UIColor grayColor];
    adBodyLabel.numberOfLines = 3;
    adBodyLabel.text = @"Short description\nThis is the second line of description\nThe third line here";
    [self addSubview:adBodyLabel];
    //[adBodyLabel release];
    
    adCoverImageView = [[UIImageView alloc] initWithFrame:CGRectMake(nMargin, nIconWidth + nDescriptionHeight + nMargin, nImageViewWidth - nMargin*2, nImageViewHeight - nMargin*2)];
    adCoverImageView.backgroundColor = [UIColor blueColor];
    [self addSubview:adCoverImageView];
    
    daaImageView = [[UIImageView alloc] initWithFrame:CGRectMake(nMargin, adCoverImageView.frame.origin.y+adCoverImageView.frame.size.height + 15, 25, 25)];
    [self addSubview:daaImageView];
    
    
// FOR MOPUB AD only
    adCoverImageTapHandleButton = [[UIButton alloc] initWithFrame:adCoverImageView.frame];
    adCoverImageTapHandleButton.backgroundColor = [UIColor clearColor];
    [adCoverImageTapHandleButton setTitle:@"" forState:UIControlStateNormal];
    [adCoverImageTapHandleButton addTarget:self action:@selector(mopubNativeAdDidClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:adCoverImageTapHandleButton];
    adCoverImageTapHandleButton.hidden = YES;
    
    
    // FOR FACEBOOK ONLY
/*
    adFBCoverMediaView = [[FBMediaView alloc] initWithFrame:CGRectMake(nMargin, nIconWidth + nDescriptionHeight + nMargin, nImageViewWidth - nMargin*2, nImageViewHeight - nMargin*2)];
    adFBCoverMediaView.delegate = self;
    [self addSubview:adFBCoverMediaView];
*/
    adCallToActionButton = [[UIButton alloc] initWithFrame:CGRectMake(mainFrame.size.width - nMargin - nActionButtonWidth, nIconWidth + nDescriptionHeight + nImageViewHeight, nActionButtonWidth, nActionButtonHeight)];
    adCallToActionButton.center = CGPointMake(mainFrame.size.width - nMargin - nActionButtonWidth + nActionButtonWidth/2, (mainFrame.size.height + nIconWidth + nDescriptionHeight + nImageViewHeight)/2);
    [adCallToActionButton setTitle:@"VIEW" forState:UIControlStateNormal];
    adCallToActionButton.backgroundColor = [UIColor grayColor];
    [self addSubview:adCallToActionButton];
    
    UIButton* btnClose = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    btnClose.titleLabel.text = @"Close";
    [btnClose addTarget:self action:@selector(onBtnClose) forControlEvents:UIControlEventTouchUpInside];
    btnClose.frame = CGRectMake(mainFrame.size.width - 80,10,50,50);
    [self addSubview:btnClose];
}

- (void)setNativeAd:(NSObject *)nativeAd fromAdNetwork:(NSString*)adNetworkName
{
    curNativeAd = nativeAd;
    
// Hide during download image....
    adIconImageView.hidden = YES;
    adCoverImageView.hidden = YES;
    daaImageView.hidden = YES;
    
    if ([adNetworkName isEqualToString:@"mopub"])
    {
        NSLog(@"%@", ((MPNativeAd*)curNativeAd).properties);
        [(MPNativeAd*)curNativeAd setDelegate:self];
//        adCoverImageTapHandleButton.hidden = NO;
//        [adCallToActionButton addTarget:self action:@selector(mopubNativeAdDidClick) forControlEvents:UIControlEventTouchUpInside];
//        [(MPNativeAd*)curNativeAd prepareForDisplayInView:self];
//        adIconImageView.hidden = NO;
//        adCoverImageView.hidden = NO;
//        daaImageView.hidden = NO;
        
        
        //self.loadAdButton.enabled = YES;
        
        //[[self.adViewContainer subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        UIView *adView = [(MPNativeAd*)curNativeAd retrieveAdViewWithError:nil];
        [self addSubview:adView];
        adView.frame = self.bounds;
    }
    else if ([adNetworkName isEqualToString:@"facebook"])
    {
        FBNativeAd* curFBNativeAd = (FBNativeAd*)nativeAd;
        if (curFBNativeAd)
            [curFBNativeAd unregisterView];
        
        curFBNativeAd.delegate = self;
        
        //curFBNativeAd.mediaCachePolicy = FBNativeAdsCachePolicyAll;
        
        adCoverImageTapHandleButton.hidden = YES;
        [adCallToActionButton removeTarget:self action:@selector(mopubNativeAdDidClick) forControlEvents:UIControlEventTouchUpInside];
        // Create native UI using the ad metadata.
        //[adFBCoverMediaView setNativeAd:curFBNativeAd];
        
        [curFBNativeAd.coverImage loadImageAsyncWithBlock:^(UIImage *image){
            adCoverImageView.image = image;
            adCoverImageView.hidden = NO;
        }];
        
        __weak typeof(self) weakSelf = self;
        [curFBNativeAd.icon loadImageAsyncWithBlock:^(UIImage *image){
            __strong typeof(self) strongSelf = weakSelf;
            adIconImageView.image = image;
            adIconImageView.hidden = NO;
        }];
        adFBStatusLabel.text = @"";
        
        // Render native ads onto UIView
        adTitleLabel.text = curFBNativeAd.title;
        adBodyLabel.text = curFBNativeAd.body;
        //adFBSocialContextLabel.text = curFBNativeAd.socialContext;
        //adFBSponsoredLabel.text = @"Sponsored";
        [adCallToActionButton setTitle:curFBNativeAd.callToAction
                                     forState:UIControlStateNormal];
        
        //[self setStarRating:self._nativeAd.starRating];
        
        NSLog(@"Register UIView for impression and click...");
        
        // Wire up UIView with the native ad; the whole UIView will be clickable.
        //[nativeAd registerViewForInteraction:self.adUIView
        //                  withViewController:self];
        [curFBNativeAd registerViewForInteraction:self withViewController:self.rootViewController withClickableViews:[NSArray arrayWithObjects:adCallToActionButton, adCoverImageView, daaImageView, nil]];
        
    }
}

-(void)onBtnClose
{
    self.hidden = YES;
}

- (void) mopubNativeAdDidClick
{
//    [(MPNativeAd*)curNativeAd displayContentWithCompletion:^(BOOL success, NSError *error)
//    {
//        if (success)
//        {
//            NSLog(@"NativeAd Mopub clicked: Ad present successfully");
//        }
//        else
//        {
//            NSLog(@"NativeAd.Mopub clicked: Ad present failed with error: %@", error);
//        }
//    }];
    
}


- (UIViewController *)viewControllerForPresentingModalView
{
    return self.rootViewController;
}

- (void)layoutAdAssets:(MPNativeAd *)adObject
{
///    [adObject loadTitleIntoLabel:adTitleLabel];
//    [adObject loadTextIntoLabel:adBodyLabel];
//    [adObject loadCallToActionTextIntoButton:adCallToActionButton];
//    [adObject loadIconIntoImageView:adIconImageView];
//    [adObject loadImageIntoImageView:adCoverImageView];
//    [adObject loadDAAIconIntoImageView:daaImageView];
}

#pragma FACEBOOK Native Ad Handle

- (void)mediaViewDidLoad:(FBMediaView *)mediaView
{
    NSLog(@"mediaViewDidLoad");
}
- (void)nativeAd:(FBNativeAd *)nativeAd didFailWithError:(NSError *)error
{
    adFBStatusLabel.text = @"Ad failed to load. Check console for details.";
    NSLog(@"Native ad failed to load with error: %@", error);
}

- (void)nativeAdDidClick:(FBNativeAd *)nativeAd
{
    NSLog(@"NativeAd Facebook clicked");
}

- (void)nativeAdDidFinishHandlingClick:(FBNativeAd *)nativeAd
{
    NSLog(@"Native ad facebook did finish click handling.");
}

- (void)nativeAdWillLogImpression:(FBNativeAd *)nativeAd
{
    NSLog(@"Native ad impression is being captured.");
}

/*
- (void)setStarRating:(struct FBAdStarRating)rating
{
    [[adFBStarRatingView subviews] makeObjectsPerformSelector: @selector(removeFromSuperview)];
    
    if (rating.scale != 0) {
        int i = 0;
        for(; i < rating.value; ++i) {
            [self setStarRatingImage:self._fullStar index:i];
        }
        for (; i < rating.scale; ++i) {
            [self setStarRatingImage:self._emptyStar index:i];
        }
    }
}

- (void)setStarRatingImage:(UIImage *)starImage
                     index:(int)indexOfStar
{
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.image = starImage;
    imageView.frame = CGRectMake(indexOfStar * 12, 0, 12, 12);
    [self.adStarRatingView addSubview:imageView];
}
*/

@end
