//
//  PLNativeAdView.h
//  PPCLINKLibs_Sample
//
//  Created by Do Lam on 07/29/15.
//  Copyright (c) 2015 ppclink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPNativeAdRendering.h"
#import "MPNativeAdDelegate.h"
#import <FBAudienceNetwork/FBAudienceNetwork.h>

#define NATIVE_AD_SQUARE_SIZE   CGSizeMake(320,260)

@interface PLNativeAdView : UIView <MPNativeAdRendering, MPNativeAdDelegate, FBNativeAdDelegate, FBMediaViewDelegate>
{
   
}

@property (weak, nonatomic) UIViewController *rootViewController;

- (void)setNativeAd:(NSObject *)nativeAd fromAdNetwork:(NSString*)adNetworkName;

@end
