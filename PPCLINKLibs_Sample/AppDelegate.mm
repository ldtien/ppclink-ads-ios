//
//  AppDelegate.m
//  PPCLINKLibs_Sample
//
//  Created by Do Lam on 5/27/15.
//  Copyright (c) 2015 Do Lam. All rights reserved.
//

#import "AppDelegate.h"
#import "LVGlobal.h"
#import "VDConfigNotification.h"
#import "TheSecondMainViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

//#import <Rollout/Rollout.h>

//@import Firebase;

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    [Rollout setupWithKey:@"5874ed4224a7837811a1dee0"];

    [Fabric with:@[CrashlyticsKit]];
//    [FIRApp configure];
//    FIRRemoteConfigSettings *remoteConfigSettings = [[FIRRemoteConfigSettings alloc] initWithDeveloperModeEnabled:YES];
//    [FIRRemoteConfig remoteConfig].configSettings = remoteConfigSettings;
//    [[FIRRemoteConfig remoteConfig] setDefaultsFromPlistFileName:@"RemoteConfigDefaults"];
    
    // Override point for customization after application launch.
    g_vdConfigNotification = nil;
    g_vdConfigNotification = [[VDConfigNotification alloc] initWithProductType:PT_FREE];
    //g_vdConfigNotification = [[VDConfigNotification alloc] initWithAppID:@"com.vietlotus.envndictfree" andAppVersion:@"1.5"];
   // g_vdConfigNotification.delegate = (id<VDConfigNotificationDelegate>)self;
    [g_vdConfigNotification setCleverNetAdZoneID:CLEVERNET_AD_TEXT_ZONE_ID];
    //g_vdConfigNotification.shouldAutoShowPopupAdWhenOpenApp = NO;
    
    
    //self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    TheSecondMainViewController *mainView = [[TheSecondMainViewController alloc] init];
    UINavigationController *nav = (UINavigationController*)self.window.rootViewController;
    nav = [nav initWithRootViewController:mainView];
 //   [self.window setRootViewController:nav];
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];


    [self displayGoogleRemoteConfig];
    
    [self loadGlobalVariable];
    // For Facebook Ad Conversion Count
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
    
    
}

- (void) loadGlobalVariable {
    //ScreenSize
    CGFloat screenHeight = MAX([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        if (screenHeight == 480.f)
            gScreenSize = LVScreenSizeIp4;
        else if (screenHeight == 568.f)
            gScreenSize = LVScreenSizeIp5;
        else if (screenHeight == 667.f)
            gScreenSize = LVScreenSizeIp6;
        else
            gScreenSize = LVScreenSizeIp6Plus;
    }
    else {
        gScreenSize = LVScreenSizeIpad;
    }
    
    //iOS version
    NSArray *components = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([components count]) {
        gIOSVersion = [[components objectAtIndex:0] integerValue];
    }
    else {
        gIOSVersion = 0;
    }
    
    //Screensize
    gScreenWidth = [UIScreen mainScreen].bounds.size.width;
    gScreenHeight = [UIScreen mainScreen].bounds.size.height;
    
}


- (void)fetchConfig
{
    long expirationDuration = 3600;
    // If in developer mode cacheExpiration is set to 0 so each fetch will retrieve values from
    // the server.
//    if ([FIRRemoteConfig remoteConfig].configSettings.isDeveloperModeEnabled) {
//        expirationDuration = 0;
//    }
    
    // [START fetch_config_with_callback]
    // cacheExpirationSeconds is set to cacheExpiration here, indicating that any previously
    // fetched and cached config would be considered expired because it would have been fetched
    // more than cacheExpiration seconds ago. Thus the next fetch would go to the server unless
    // throttling is in progress. The default expiration duration is 43200 (12 hours).
//    [[FIRRemoteConfig remoteConfig] fetchWithExpirationDuration:expirationDuration completionHandler:^(FIRRemoteConfigFetchStatus status, NSError *error)
//    {
//        if (status == FIRRemoteConfigFetchStatusSuccess) {
//            NSLog(@"Config fetched!");
//            [[FIRRemoteConfig remoteConfig] activateFetched];
//        } else {
//            NSLog(@"Config not fetched");
//            NSLog(@"Error %@", error.localizedDescription);
//        }
//        [self displayGoogleRemoteConfig];
//    }];
    // [END fetch_config_with_callback]
}

// Display price with discount applied if promotion is on. Otherwise display original price.
- (void)displayGoogleRemoteConfig
{
//    int nativeAdRate = [FIRRemoteConfig remoteConfig][@"nativead_rate"].numberValue.intValue;
//    NSString* sPPCLINKMediationConfigInterstitial = [FIRRemoteConfig remoteConfig][@"PPCLINKMediationConfig_Interstitial"].stringValue;
//    NSLog(@"config1 = %@, config2 = %d", sPPCLINKMediationConfigInterstitial, nativeAdRate);
}


- (void)VDConfigNotification:(VDConfigNotification *)vdConfigNotification showNotificationWithInfo:(NSDictionary *)dictNotifyInfo
{
    
    // Depend on Notification type, show notifiation with your UI style here...
    NOTIFICATION_TYPE ntype = (NOTIFICATION_TYPE)[[dictNotifyInfo objectForKey:kNotification_Type] intValue];
    
    if (ntype == NTYPE_APP)
    {
        NSString* sAppID = [dictNotifyInfo objectForKey:kNotification_AppId];
        NSString* sIconURL = [dictNotifyInfo objectForKey:kNotification_AdImage];
        NSString* sFullscreenURL = [dictNotifyInfo objectForKey:kNotification_AdFullscreen];
        NSString* sClickURL = [dictNotifyInfo objectForKey:kNotification_URL];
        
        //Hien quang cao fullscreen o day....
        
    }
}


/*
- (void)VDConfigNotification:(VDConfigNotification *)vdConfigNotification showNotificationWithInfo:(NSDictionary *)dictNotifyInfo
{
    if (!dictCurNotificationInfo)
        dictCurNotificationInfo = [[NSMutableDictionary alloc] initWithDictionary:dictNotifyInfo];
    else
        [dictCurNotificationInfo setDictionary:dictNotifyInfo];
    
    // Depend on Notification type, show notifiation with your UI style here...
    NOTIFICATION_TYPE ntype = (NOTIFICATION_TYPE)[[dictNotifyInfo objectForKey:kNotification_Type] intValue];
    UIAlertView *al = nil;
    
    if (ntype == NTYPE_APP)
    {
        int nNumberOfAdFreeHours = [[dictNotifyInfo objectForKey:kNotification_NumberOfAdFreeHours] intValue];
        
        if (nNumberOfAdFreeHours > 0)
        {
            
#if __VIETNAMESE__
            al = [[UIAlertView alloc] initWithTitle:@"Loại bỏ quảng cáo" message:[NSString stringWithFormat:@"Cài đặt %@ để không phải xem quảng cáo trong %d ngày.", [dictNotifyInfo objectForKey:kNotification_Title], nNumberOfAdFreeHours/24] delegate:self cancelButtonTitle:[dictNotifyInfo objectForKey:kNotification_CancelText] otherButtonTitles:[dictNotifyInfo objectForKey:kNotification_TryNowText], nil];
#else
            al = [[UIAlertView alloc] initWithTitle:@"Remove Ads" message:[NSString stringWithFormat:@"Install app %@ to remove all ads in %d days.", [dictNotifyInfo objectForKey:kNotification_Title], nNumberOfAdFreeHours/24] delegate:self cancelButtonTitle:[dictNotifyInfo objectForKey:kNotification_CancelText] otherButtonTitles:[dictNotifyInfo objectForKey:kNotification_TryNowText], nil];
#endif
        }
        else
            al = [[UIAlertView alloc] initWithTitle:[dictNotifyInfo objectForKey:kNotification_Title] message:[dictNotifyInfo objectForKey:kNotification_Description] delegate:self cancelButtonTitle:[dictNotifyInfo objectForKey:kNotification_CancelText] otherButtonTitles:[dictNotifyInfo objectForKey:kNotification_TryNowText], nil];
        
        [al show];
        
    }
    else if (ntype == NTYPE_CLEVERNET)
    {
        al = [[UIAlertView alloc] initWithTitle:[dictNotifyInfo objectForKey:kNotification_Title] message:[dictNotifyInfo objectForKey:kNotification_Description] delegate:self cancelButtonTitle:[dictNotifyInfo objectForKey:kNotification_CancelText] otherButtonTitles:[dictNotifyInfo objectForKey:kNotification_TryNowText], nil];
    }
    else if (ntype == NTYPE_VIEWADS)
    {
        // Chi popup khi quang cao interstitial da san sang để hiện
        if ([[MPMediationAdController sharedManager] isInterstitialAdAvailable:INTERSTITIAL_AD_TYPE_ALL])
        {
            // Lấy text theo server nếu thưởng chỉ thưởng Ad-Free như mặc định
            al = [[UIAlertView alloc] initWithTitle:[dictNotifyInfo objectForKey:kNotification_Title] message:[dictNotifyInfo objectForKey:kNotification_Description] delegate:self cancelButtonTitle:[dictNotifyInfo objectForKey:kNotification_CancelText] otherButtonTitles:[dictNotifyInfo objectForKey:kNotification_TryNowText], nil];
            
            [al show];
        }
    }
    else if (ntype == NTYPE_INAPP)
    {
        NSString* sDes = [dictNotifyInfo objectForKey:kNotification_Description];
        
#if __VIETNAMESE__
        if ([sDes length] == 0)
            sDes = @"Xin mời nâng cấp lên bản Pro để loại bỏ vĩnh viễn quảng cáo.";
        al = [[UIAlertView alloc] initWithTitle:@"Nâng cấp bản Pro" message:sDes delegate:self cancelButtonTitle:[dictNotifyInfo objectForKey:kNotification_CancelText] otherButtonTitles:[dictNotifyInfo objectForKey:kNotification_TryNowText], nil];
#else
        if ([sDes length] == 0)
            sDes = @"Upgrade to Pro Version to remove all ads forever!";
        al = [[UIAlertView alloc] initWithTitle:@"Upgrade Pro" message:sDes delegate:self cancelButtonTitle:[dictNotifyInfo objectForKey:kNotification_CancelText] otherButtonTitles:[dictNotifyInfo objectForKey:kNotification_TryNowText], nil];
#endif
        [al show];
    }
    else if (ntype == NTYPE_MESSAGE)
    {
        NSString* sTryNowText = [dictNotifyInfo objectForKey:kNotification_TryNowText];
        
        // Trường hợp thông báo đơn thuần, không gợi ý user click view để xem thêm gì cả thì chỉ cần 1 nut Cancel text
        if ([[dictNotifyInfo objectForKey:kNotification_URL] length] < 2)
            sTryNowText = nil;
        
        al = [[UIAlertView alloc] initWithTitle:[dictNotifyInfo objectForKey:kNotification_Title] message:[dictNotifyInfo objectForKey:kNotification_Description] delegate:self cancelButtonTitle:[dictNotifyInfo objectForKey:kNotification_CancelText] otherButtonTitles:sTryNowText, nil];
        [al show];
    }
}

// Được gọi khi mở app và kiểm tra thấy user đã cài đặt một app để nhận thưởng (Ad-free)
- (void)VDConfigNotification:(VDConfigNotification *)vdConfigNotification userDidInstallAppForRewardWithInfo:(NSDictionary *)dictAppInfo
{
    int nNumberOfAdFreeHours = [[dictAppInfo objectForKey:kNotification_NumberOfAdFreeHours] intValue];
    [g_vdConfigNotification addAdFreeHours:nNumberOfAdFreeHours];
    // Khi user dc tặng Ad-Free time, chỉ can gọi hàm ẩn quảng cáo banner luôn ở đây, các lúc khác (khi mở app,...) bản thân MPMediationAdController tự check và ẩn rồi
    [[MPMediationAdController sharedManager] hideBannerAd];
#if __VIETNAMESE__
    UIAlertView *al = [[UIAlertView alloc] initWithTitle:@"Chúc mừng" message:[NSString stringWithFormat:@"Bạn được tặng %d ngày không phải xem quảng cáo vì đã cài đặt thành công ứng dụng %@", nNumberOfAdFreeHours/24, [dictAppInfo objectForKey:kNotification_Title]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
#else
    UIAlertView *al = [[UIAlertView alloc] initWithTitle:@"Congratulation" message:[NSString stringWithFormat:@"You have just been rewarded %d days use without ads for successfully installed app: %@. Thank you!", nNumberOfAdFreeHours/24, [dictAppInfo objectForKey:kNotification_Title]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
#endif
    [al show];
}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0: //Cannel
            break;
            
        case 1: // Try Now
        {
            // Check this notification is rewarded or not
            BOOL bReward = [[dictCurNotificationInfo objectForKey:kNotification_NumberOfAdFreeHours] intValue] > 0 ? YES:NO;
            [g_vdConfigNotification userJustClickTryNowWithNotificationId:[dictCurNotificationInfo objectForKey:kNotification_ID] forReward:bReward];
            
            NOTIFICATION_TYPE notifyType = [[dictCurNotificationInfo objectForKey:kNotification_Type] intValue];
            if (notifyType == NTYPE_APP || notifyType == NTYPE_MESSAGE || notifyType == NTYPE_CLEVERNET)
            {
                NSString *sURL = [dictCurNotificationInfo objectForKey:kNotification_URL];
                sURL = [sURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSLog(@"Open URL: %@", sURL);
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:sURL]];
            }
            else if (notifyType == NTYPE_VIEWADS)
            {
                // Goi ham hien quang cao o day
                [[MPMediationAdController sharedManager] showInterstitialAd:INTERSTITIAL_AD_TYPE_ALL withBonusInfo:dictCurNotificationInfo];
            }
            else if (notifyType == NTYPE_INAPP)
            {
                // Show buy in app for here...
            }
            
            break;
        }
            
        default:
            break;
    }
}
*/


- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation { // For Facebook Ad Conversion Count
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    [self fetchConfig];
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
