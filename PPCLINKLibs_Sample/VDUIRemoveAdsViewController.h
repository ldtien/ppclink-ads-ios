//
//  VDUIRemoveAdsViewController.h
//  VDFramework_sample
//
//  Created by Do Lam on 4/9/15.
//  Copyright (c) 2015 Do Lam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VDUIRemoveAdsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    UITableView     *_tableView;
    UILabel         *labelNumberOfAdFreeHours;
}

- (void)updateAdFreeHours;
@end
