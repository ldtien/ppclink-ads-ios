////
////  Global.mm
////  Smartkids
////
////  Created by minhbn on 7/29/10.
////  Copyright 2010 __MyCompanyName__. All rights reserved.
////
//
#import "Global.h"

NSString *getFontName(MODE_FONT modeFont, BOOL isLargeText) {
    
    NSString *sFontName;
    float fSystemValue = [[UIDevice currentDevice].systemVersion floatValue];
    if(modeFont == MODE_FONT_REGULAR)
    {
        sFontName = [UIFont systemFontOfSize:14].fontName;
        return sFontName;
        
        if (fSystemValue >= 9)
        {
            if (isLargeText) {
                sFontName = @".SFUIDisplay-Regular";
            }
            else{
                sFontName = @".SFUIText-Regular";
            }
        }
        else
        {
            sFontName = @"HelveticaNeue";
        }
    }
    else if(modeFont == MODE_FONT_MEDIUM)
    {
        if (fSystemValue >= 9)
        {
            if (isLargeText) {
                sFontName = @".SFUIDisplay-Medium";
            }
            else{
                sFontName = @".SFUIText-Medium";
            }
        }
        else
        {
            sFontName = @"HelveticaNeue-Medium";
        }
    }
    else if(modeFont == MODE_FONT_BOLD)
    {
        
        return [UIFont boldSystemFontOfSize:14].fontName;
        
        if (fSystemValue >= 9)
        {
            if (isLargeText) {
                sFontName = @".SFUIDisplay-Bold";
            }
            else{
                sFontName = @".SFUIText-Bold";
            }
        }
        else
        {
            sFontName = @"HelveticaNeue-Bold";
        }
    }
    else if(modeFont == MODE_FONT_ITALIC)
    {
        if (fSystemValue >= 9)
        {
            if (isLargeText) {
                sFontName = @".SFUIDisplay-Italic";
            }
            else{
                sFontName = @".SFUIText-Italic";
            }
        }
        else
        {
            sFontName = @"HelveticaNeue-Italic";
        }
    }
    else
    {
        if (fSystemValue >= 9)
        {
            if (isLargeText) {
                sFontName = @".SFUIDisplay-Light";
            }
            else{
                sFontName = @".SFUIText-Light";
            }
        }
        else
        {
            sFontName = @"HelveticaNeue-Light";
        }
    }
    return sFontName;
}

//#import "VDConfigNotification.h"
////#import <FirebaseAnalytics/FirebaseAnalytics.h>
//
//NSString*           g_strQuoteCateName = nil;
//NSString*           g_strDocumentPath = nil;
//NSString*           g_strMainBundlePath = nil;
//NSMutableArray*     g_arrayCan = nil;
//NSMutableArray*     g_arrayChi = nil;
//NSMutableDictionary *g_pDictSetting = nil; 
//NSMutableDictionary* g_pDictSpecialDay = nil;
//NSMutableArray      *g_ArrIndexBgr = nil;
//NSDateFormatter *g_NSDateFormatter = nil;
//
//
//NSDate          *g_pDateStartSwichView;
//BOOL            g_bAppIsInForeground = FALSE;
//bool                g_bOSVersionIsAtLeastiOS7 = false;
//bool                g_bIsDaySpec;
//int                 g_iOldSelPhotoOption;
//BOOL                g_bCallShowAds;
//int                 g_iCurModeShowAds;
//int                 g_iCountImgBgr = 1;
//int                 g_nCurSendMailMode;
//BOOL                g_bCheckShowAdsFromPopUp;
//float               g_fSapceItem;
//BOOL                g_bTapDetail;
//BOOL                g_bShowTapDetail;
//BOOL                g_bLocationUpdate;
////BOOL                g_bShowRate;
//DEVICE_TYPE                  g_DeviceType;
//CONFIG_APP          g_stConfigApp;
//AppDelegate* g_AppDelegate = nil;
//
//NSString* getDeviceTypeStoryboardName(){
//    switch (g_DeviceType) {
//        case DEVICE_TYPE_IPAD:
//            return @"~ipad";
//            break;
//        case DEVICE_TYPE_IPHONE5:
//            return @"~iphone5";
//            break;
//        case DEVICE_TYPE_IPHONE:
//            return @"~iphone";
//            break;
//        case DEVICE_TYPE_IPHONE6:
//            return @"~iphone6";
//            break;
//        case DEVICE_TYPE_IPHONE6_PLUS:
//            return @"~iphone6plus";
//            break;
//            
//        default:
//            return @"";
//            break;
//    }
//}
//
//
//DEVICE_TYPE getTypeDivice(UIView *pView) {
//    DEVICE_TYPE device_type;
//    
//    int possibleScreenWidthValue1;
//    int iRealWidth, iRealHeight;
//    
//    if(pView == nil)
//    {
//        possibleScreenWidthValue1 = (int)round([[UIScreen mainScreen] bounds].size.width);
//        //possibleScreenWidthValue2 = (int)round([[UIScreen mainScreen] bounds].size.height);
//        iRealWidth = possibleScreenWidthValue1;
//        iRealHeight = (int)round([[UIScreen mainScreen] bounds].size.height);
//    }
//    else
//    {
//        possibleScreenWidthValue1 = (int)round(pView.frame.size.width);
//        //possibleScreenWidthValue2 = (int)round(pView.frame.size.height);
//        
//        iRealWidth = (int)round([[UIScreen mainScreen] bounds].size.width);
//        iRealHeight = (int)round([[UIScreen mainScreen] bounds].size.height);
//        
//    }
//    
//    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
//    {
//        
//        if((iRealWidth == 320 && iRealHeight == 568) || (iRealWidth == 568 && iRealHeight == 320))
//        {
//            if([UIScreen instancesRespondToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 3.0)
//            {
//                device_type = DEVICE_TYPE_IPHONE6_PLUS;
//            }
//            else
//                device_type = DEVICE_TYPE_IPHONE5;
//        }
//        else if((iRealWidth == 375 && iRealHeight == 667) || (iRealWidth == 667 && iRealHeight == 375))
//        {
//            if(possibleScreenWidthValue1 == 320 || possibleScreenWidthValue1 == 568)
//                device_type = DEVICE_TYPE_IPHONE5;
//            else
//            {
//                /*
//                 if([UIScreen instancesRespondToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 3.0)
//                 {
//                 device_type = DEVICE_TYPE_IPHONE6_PLUS;
//                 }
//                 else
//                 */
//                
//                device_type = DEVICE_TYPE_IPHONE6;
//            }
//        }
//        
//        else if((iRealWidth == 414 && iRealHeight == 736) || (iRealWidth == 736 && iRealHeight == 414))
//        {
//            if(possibleScreenWidthValue1 == 320 || possibleScreenWidthValue1 == 568)
//                device_type = DEVICE_TYPE_IPHONE5;
//            else if(possibleScreenWidthValue1 == 375 || possibleScreenWidthValue1 == 667)
//                device_type = DEVICE_TYPE_IPHONE6;
//            else
//                device_type = DEVICE_TYPE_IPHONE6_PLUS;
//        }
//        else
//            device_type = DEVICE_TYPE_IPHONE;
//    }
//    else
//    {
//        device_type = DEVICE_TYPE_IPAD;
//    }
//    return device_type;
//}
//
//
//
//
//#pragma mark UIXImage
//@implementation UIXImage
//
//-(id)initWithContentsOfFile:(NSString *)path{
//    if([UIScreen instancesRespondToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0 )
//    {
//        NSString *path2x = [[path stringByDeletingPathExtension]
//                            stringByAppendingFormat:@"@2x.%@",[path pathExtension]];
//        
//        if ([[NSFileManager defaultManager] fileExistsAtPath:path2x]) return [super initWithContentsOfFile:path2x];
//    }
//    
//    return [super initWithContentsOfFile:path];
//}
//
//@end
//
//
//@implementation UIImage (YALExtension)
//
//+ (UIImage *)imageWithRoundedCorners:(CGFloat)cornerRadius size:(CGSize)sizeToFit
//                           fillColor:(UIColor*)fillColor
//                         strokeColor:(UIColor*)strokeColor
//                           lineWidth:(CGFloat)lineWidth {
//    CGRect rect = (CGRect){0.f, 0.f, sizeToFit};
//    
//    UIGraphicsBeginImageContextWithOptions(sizeToFit, NO, UIScreen.mainScreen.scale);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextAddPath(context,
//                     [UIBezierPath bezierPathWithRoundedRect:CGRectInset(rect, lineWidth, lineWidth) cornerRadius:cornerRadius].CGPath);
//    CGContextSetLineWidth(context, lineWidth);
//    CGContextSetStrokeColorWithColor(context, strokeColor.CGColor);
//    CGContextSetFillColorWithColor(context, fillColor.CGColor);
//    CGContextDrawPath(context, kCGPathFillStroke);
//    //    CGContextClip(context);
//    
//    UIImage *output = UIGraphicsGetImageFromCurrentImageContext();
//    
//    UIGraphicsEndImageContext();
//    
//    return output;
//}
//
//@end
//
//
//
//#pragma mark UIImage (extended)
//@implementation UIImage (extended)
//+ (UIImage*)resourceImageNamed:(NSString *)name{
//    static NSString* resourcePath = [[[NSBundle mainBundle] resourcePath] retain];
//    static NSFileManager* fileManager = [[NSFileManager defaultManager] retain];
//    
//    
//    if([UIScreen instancesRespondToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 3.0)
//    {
//        NSString *path2x = [resourcePath stringByAppendingPathComponent:[[name stringByDeletingPathExtension] stringByAppendingFormat:@"@3x.%@",[name pathExtension]]];
//        
//        if ([fileManager fileExistsAtPath:path2x])
//            return [UIImage imageWithContentsOfFile:path2x];
//    }
//    else if([UIScreen instancesRespondToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0)
//    {
//        NSString *path2x = [resourcePath stringByAppendingPathComponent:[[name stringByDeletingPathExtension] stringByAppendingFormat:@"@2x.%@",[name pathExtension]]];
//        
//        if ([fileManager fileExistsAtPath:path2x])
//            return [UIImage imageWithContentsOfFile:path2x];
//    }
//    return [UIImage imageWithContentsOfFile:[resourcePath stringByAppendingPathComponent:name]];
//}
//
//+ (UIImage*)imageInResourcePath:(NSString*)path named:(NSString *)name{
//    
//    if([UIScreen instancesRespondToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 3.0 )
//    {
//        static NSFileManager* fileManager = [[NSFileManager defaultManager] retain];
//        NSString *path2x = [path stringByAppendingPathComponent:[[name stringByDeletingPathExtension] stringByAppendingFormat:@"@3x.%@",[name pathExtension]]];
//        
//        if ([fileManager fileExistsAtPath:path2x])
//            return [UIImage imageWithContentsOfFile:path2x];
//    }
//    else if([UIScreen instancesRespondToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0 )
//    {
//        static NSFileManager* fileManager = [[NSFileManager defaultManager] retain];
//        NSString *path2x = [path stringByAppendingPathComponent:[[name stringByDeletingPathExtension] stringByAppendingFormat:@"@2x.%@",[name pathExtension]]];
//        
//        if ([fileManager fileExistsAtPath:path2x])
//            return [UIImage imageWithContentsOfFile:path2x];
//    }
//    return /*[UIImage resourceImageNamed:name];/*/[UIImage imageWithContentsOfFile:[path stringByAppendingPathComponent:name]];
//}
//
//@end
//
//
//#pragma mark CustomButtonText
//@implementation UITextButton
//
//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        // Initialization code
//        self.backgroundColor = [UIColor clearColor];
//        self.exclusiveTouch = YES;
//        [self initViews];
//        
//        //[self setShowsTouchWhenHighlighted:YES];
//        //[self setBackgroundImage:[self getImageWithButton:self withColor:[UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:0]] forState:UIControlStateHighlighted];
//        [self addTarget:self action:@selector(onClickDown) forControlEvents:UIControlEventTouchDown];
//        
//        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onUpdateViewAppear) name:@"ExitViewXemSao" object:nil];
//    }
//    
//    
//    return self;
//}
//
//- (void) initViews {
//    m_pLbl1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width , 0)];
//    [m_pLbl1 setBackgroundColor:[UIColor clearColor]];
//    m_pLbl1.numberOfLines = 2;
//    m_pLbl1.textAlignment = UITextAlignmentCenter;
//    //m_pLbl1.font = [UIFont fontWithName:@"Roboto-Bold" size:(fSizeFont - 4)];
//#if __LICHVANSU_FREE__ || __LVN_CHILINGO__
//    m_pLbl1.textColor = [UIColor whiteColor];
//#else
//    m_pLbl1.textColor = COLOR_TITLEBAR;
//#endif
//    [self addSubview:m_pLbl1];
//    [m_pLbl1 release];
//    
//    m_pLbl2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width , 0)];
//    [m_pLbl2 setBackgroundColor:[UIColor clearColor]];
//    m_pLbl2.textAlignment = UITextAlignmentCenter;
//    //m_pLbl1.font = [UIFont fontWithName:@"Roboto-Bold" size:(fSizeFont - 4)];
//#if __LICHVANSU_FREE__ || __LVN_CHILINGO__
//    m_pLbl2.textColor = [UIColor whiteColor];
//#else
//    m_pLbl2.textColor = COLOR_TITLEBAR;
//#endif
//    [self addSubview:m_pLbl2];
//    [m_pLbl2 release];
//}
//
//
//- (void) setFontControl:(UIFont*)pFont {
//    m_pLbl1.font = pFont;
//    m_pLbl2.font = pFont;
//}
//- (void) setText:(NSString*)sText1 withLine2:(NSString*)sText2 {
//    
//    m_pLbl1.text = sText1;
//    m_pLbl2.text = sText2;
//    
//    CGSize size1,size2;
//    
//    CGRect rect = m_pLbl1.frame;
//    float fTop;
//    size1 = [sText1 sizeWithFont:m_pLbl1.font constrainedToSize:CGSizeMake(self.frame.size.width, CGFLOAT_MAX)];
//    if(sText2.length > 0)
//    {
//        size2 = [sText2 sizeWithFont:m_pLbl2.font constrainedToSize:CGSizeMake(self.frame.size.width, CGFLOAT_MAX)];
//    }
//    else
//    {
//        size2 = CGSizeMake(0, 0);
//    }
//    
//    fTop = (self.frame.size.height - size1.height - size2.height)/2;
//    
//    
//    
//    
//    rect.origin.y = fTop;
//    rect.size.height = size1.height;
//    m_pLbl1.frame = rect;
//    
//    fTop += rect.size.height;
//    rect = m_pLbl2.frame;
//    
//    if(g_DeviceType == DEVICE_TYPE_IPHONE6)
//        rect.origin.y = fTop - 2;
//    else if(g_DeviceType == DEVICE_TYPE_IPAD)
//        rect.origin.y = fTop - 5;
//    else
//        rect.origin.y = fTop - 2;
//    rect.size.height = size2.height;
//    m_pLbl2.frame = rect;
//    
//}
//
//- (void) setTextButtonColor:(UIColor*)clrText;
//{
//    m_pLbl1.textColor = clrText;
//    m_pLbl2.textColor = clrText;
//}
//
//- (void) onClickDown {
//    self.alpha = 0.3;
//    
//    [self performSelector:@selector(resetAlplaButton) withObject:nil afterDelay:0.2];
//}
//
//- (void) resetAlplaButton {
//    self.alpha = 1.0;
//}
//
//- (UIImage *) getImageWithButton:(UIButton*)btn withColor:(UIColor *)color  {
//    CGRect rect = CGRectMake(0, 0, btn.frame.size.width, btn.frame.size.height);
//    UIGraphicsBeginImageContext(rect.size);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextSetFillColorWithColor(context, [color CGColor]);
//    CGContextFillRect(context, rect);
//    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return img;
//}
//
//
//#pragma mark Destroy
//- (void) dealloc {
//    m_pLbl1 = nil;
//    m_pLbl2 = nil;
//    [super dealloc];
//}
//@end
//
//@interface UIBarButton ()
//{
//    
//}
//@end
//@implementation UIBarButton
//
//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        // Initialization code
//        self.backgroundColor = [UIColor clearColor];
//        self.exclusiveTouch = YES;
//        [self initViews];
//        
//        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onUpdateViewAppear) name:@"ExitViewXemSao" object:nil];
//    }
//    
//    
//    return self;
//}
//
//- (void) initViews {
//    
//    float fSizeIcon = 36;
//    float fSizeFont = 10;
//    float fBottomMargin = 2,fTopMargin = 3,fTopMarginNum = 3;
//    if(g_DeviceType == DEVICE_TYPE_IPHONE6_PLUS)
//    {
//        fSizeIcon = 47;
//        fSizeFont = 11;
//        fTopMargin = 5;
//        fBottomMargin = 6;
//        fTopMarginNum = 5;
//    }
//    else if(g_DeviceType == DEVICE_TYPE_IPHONE6)
//    {
//        fSizeIcon = 39;
//        fSizeFont = 11;
//        fTopMargin = 5;
//        fBottomMargin = 6;
//        fTopMarginNum = 4;
//    }
//    else if(g_DeviceType == DEVICE_TYPE_IPHONE5)
//    {
//        fSizeIcon = 40;
//        fBottomMargin = 4;
//    }
//    else if(g_DeviceType == DEVICE_TYPE_IPAD)
//    {
//        fTopMargin = 8;
//        fSizeIcon = 60;
//        fSizeFont = 18;
//        fBottomMargin = 14;
//        fTopMarginNum = 5;
//    }
//    
//    CGSize size = getSizeText(TEXT_LICHNGAY, [UIFont fontWithName:getFontName(MODE_FONT_REGULAR,NO) size:fSizeFont], self.frame.size.width);
//    
//    float fTop = self.frame.size.height - size.height - fBottomMargin;
//    m_pLbl1 = [[UILabel alloc] initWithFrame:CGRectMake(0,fTop , self.frame.size.width , size.height + fBottomMargin)];
//    [m_pLbl1 setBackgroundColor:[UIColor clearColor]];
//    m_pLbl1.textAlignment = UITextAlignmentCenter;
//    m_pLbl1.font = [UIFont fontWithName:getFontName(MODE_FONT_REGULAR,NO) size:fSizeFont];
//    m_pLbl1.textColor = COLOR_TITLEBAR;
//    [self addSubview:m_pLbl1];
//    [m_pLbl1 release];
//    
//    
//    m_pImgView = [[UIImageView alloc] initWithFrame:CGRectMake((self.frame.size.width - fSizeIcon)/2,(fTop - fSizeIcon)/2 + fTopMargin, fSizeIcon, fSizeIcon)];
//    [self addSubview:m_pImgView];
//    [m_pImgView release];
//    
//    m_pLblNum = [[UILabel alloc] initWithFrame:CGRectMake(0, fTopMarginNum, m_pImgView.frame.size.width , m_pImgView.frame.size.height)];
//    [m_pLblNum setBackgroundColor:[UIColor clearColor]];
//    m_pLblNum.textAlignment = UITextAlignmentCenter;
//    m_pLblNum.font = [UIFont fontWithName:getFontName(MODE_FONT_MEDIUM,NO) size:fSizeFont];
//    m_pLblNum.textColor = COLOR_TITLEBAR;
//    [m_pImgView addSubview:m_pLblNum];
//    [m_pLblNum release];
//    m_pLblNum.text = @"";
//    
//    //noti Label
//    notiLabel = [[UILabel alloc] init];
//    [notiLabel setBackgroundColor:[UIColor clearColor]];
//    notiLabel.textAlignment = UITextAlignmentCenter;
//    notiLabel.font = [UIFont fontWithName:getFontName(MODE_FONT_REGULAR,NO) size:g_stConfigApp.fFontTitleLikeQuotion];
//    notiLabel.textColor = [UIColor whiteColor];
//    [self addSubview:notiLabel];
//    [notiLabel release];
//    notiLabel.text = @"";
//}
//
//- (void) updateNotificationLabelWithNumberNoti:(int) numberNoti{
//    if (numberNoti > 0) {
//        [notiLabel setHidden:NO];
//        notiLabel.text = [NSString stringWithFormat:@"%d",numberNoti];
//        notiLabel.backgroundColor = [UIColor redColor];
//        CGSize size = [notiLabel sizeThatFits:CGSizeMake(1000, 1000)];
//        float width = size.width;
//        float height = size.height;
//        if (width < height) {
//            width = height;
//        }
//        notiLabel.frame = CGRectMake(m_pImgView.frame.origin.x + m_pImgView.frame.size.width - size.width, m_pImgView.frame.origin.y + height/3.f, width, height);
//        notiLabel.layer.borderColor = [UIColor redColor].CGColor;
//        //        notiLabel.textAlignment = NSTextAlignmentCenter;
//        notiLabel.layer.cornerRadius = height/2.f;
//        notiLabel.layer.masksToBounds = YES;
//    }
//    else{
//        [notiLabel setHidden:YES];
//    }
//}
//
//- (void) setFontControl:(UIFont*)pFont {
//    
//}
//
//- (void) setTextButton:(NSString*)aText andColor:(UIColor*)clr{
//    m_pLbl1.text = aText;
//    m_pLbl1.textColor = clr;
//    
//    m_pLblNum.textColor =  m_pLbl1.textColor;
//}
//
//- (void) setNum:(NSString*)aText {
//    m_pLblNum.text = aText;
//    
//}
//- (void) setImageButton:(UIImage*)pImg {
//    m_pImgView.image = pImg;
//}
//
//UIImage *MakeImage(UIColor *color, CGRect bounds) {
//    UIGraphicsBeginImageContext(bounds.size);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextSetFillColorWithColor(context, [color CGColor]);
//    CGContextFillRect(context, bounds);
//    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return image;
//}
//
//UIImage *MakeHighlightedImage(UIColor *color, CGRect bounds) {
//    UIGraphicsBeginImageContext(bounds.size);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextSetFillColorWithColor(context, [color CGColor]);
//    CGContextFillRect(context, bounds);
//    CGContextSetFillColorWithColor(context, RGBA(0.f, 0.f, 0.f, 0.4f).CGColor);
//    CGContextFillRect(context, bounds);
//    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return image;
//}
//
//UIImage *MakeEventGoogleIcon(UIColor *color, NSString *text , UIFont *font, CGRect bounds){
//    UIGraphicsBeginImageContextWithOptions(bounds.size,NO,0.0f);
//    //UIGraphicsBeginImageContext(bounds.size);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    [[UIBezierPath bezierPathWithRoundedRect:bounds cornerRadius:bounds.size.width] addClip];
//    CGContextSetFillColorWithColor(context, [color CGColor]);
//    CGContextFillRect(context, bounds);
//    //CGContextSetFillColorWithColor(context, RGBA(0.f, 0.f, 0.f, 0.4f).CGColor);
//    //CGContextFillRect(context, bounds);
//    NSMutableParagraphStyle *paragraphStyle = [[[NSMutableParagraphStyle alloc] init] autorelease];
//    paragraphStyle.alignment= NSTextAlignmentCenter;
//
//    NSDictionary *attributes = @{ NSFontAttributeName:font,
//                                  NSParagraphStyleAttributeName:paragraphStyle,
//                                  NSForegroundColorAttributeName: [UIColor whiteColor]};
//    CGRect rectText = bounds;
//    rectText.origin.y = (CGRectGetMidY(bounds) - (font.lineHeight/2));
//    [text drawInRect:rectText withAttributes:attributes];
//    
//    //
//    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return image;
//}
//
//UIImage *DrawTextImage(CGRect bounds, UIColor *backgroundColor, CGFloat radius, UIColor *borderColor, CGFloat borderWidth, NSString *text, UIColor *textColor, UIFont *font){
//    UIGraphicsBeginImageContextWithOptions(bounds.size,NO,0.0f);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    /*[[UIBezierPath bezierPathWithRoundedRect:bounds cornerRadius:radius] addClip];
//    CGContextSetFillColorWithColor(context, [backgroundColor CGColor]);
//    CGContextSetLineWidth(context, borderWidth);
//    CGContextSetStrokeColorWithColor(context, borderColor.CGColor);
//    CGContextFillRect(context, bounds);
//    CGContextDrawPath(context, kCGPathFillStroke);
//     */
//    CGContextSetLineWidth(context, borderWidth);
//    CGContextSetStrokeColorWithColor(context, borderColor.CGColor);
//    UIBezierPath *bezierPath = [UIBezierPath bezierPathWithRoundedRect:bounds cornerRadius:radius];
//    CGContextAddPath(context, bezierPath.CGPath);
//    CGContextSetFillColorWithColor(context, backgroundColor.CGColor);
//    CGContextDrawPath(context, kCGPathFillStroke);
//
//    
//    NSMutableParagraphStyle *paragraphStyle = [[[NSMutableParagraphStyle alloc] init] autorelease];
//    paragraphStyle.alignment= NSTextAlignmentCenter;
//    
//    NSDictionary *attributes = @{ NSFontAttributeName:font,
//                                  NSParagraphStyleAttributeName:paragraphStyle,
//                                  NSForegroundColorAttributeName: textColor};
//    CGRect rectText = bounds;
//    rectText.origin.y = (CGRectGetMidY(bounds) - (font.lineHeight/2));
//    [text drawInRect:rectText withAttributes:attributes];
//    
//    //
//    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return image;
//}
//
////==== Format from 123456789 to 123,456,789 ====
//NSString *NSStringFormatted(NSString *string, NSString *separateString)
//{
//    NSRange commaRange = [string rangeOfString:@","];
//    NSRange kRange     = [string rangeOfString:@"k"];
//    NSRange mRange     = [string rangeOfString:@"m"];
//    NSRange bRange     = [string rangeOfString:@"b"];
//    
//    if (commaRange.location == NSNotFound && kRange.location == NSNotFound
//        && mRange.location == NSNotFound && bRange.location == NSNotFound) {
//        NSMutableArray * array = [[[NSMutableArray alloc] init] autorelease];
//        int startIndex = 0;
//        
//        for (int i = 0; i < string.length; i++) {
//            if (i == 0) {
//                switch (string.length % 3) {
//                    case 0:
//                    {
//                        [array addObject:[NSString stringWithFormat:@"%c%c%c", [string characterAtIndex:i], [string characterAtIndex:i+1], [string characterAtIndex:i+2]]];
//                        startIndex = 0;
//                    }
//                        break;
//                        
//                    case 1:
//                    {
//                        [array addObject:[NSString stringWithFormat:@"%c", [string characterAtIndex:i]]];
//                        startIndex = 1;
//                    }
//                        break;
//                        
//                    case 2:
//                    {
//                        [array addObject:[NSString stringWithFormat:@"%c%c", [string characterAtIndex:i], [string characterAtIndex:i+1]]];
//                        startIndex = 2;
//                    }
//                        break;
//                    default:
//                        break;
//                }
//            }
//            else{
//                if ((i - startIndex)%3 == 0) {
//                    [array addObject:[NSString stringWithFormat:@"%c%c%c", [string characterAtIndex:i], [string characterAtIndex:i+1], [string characterAtIndex:i+2]]];
//                }
//            }
//        }
//        
//        return [array componentsJoinedByString:separateString];
//    }
//    else{
//        return string;
//    }
//}
//
//UIImage *CaptureScreenFromView(UIView *view) {
//    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
//    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
//    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return image;
//}
//#pragma mark Destroy
//- (void) dealloc {
//    m_pLbl1 = nil;
//    m_pImgView = nil;
//    [super dealloc];
//}
//@end
//
//@implementation UIImage (averageColor)
//
//- (UIColor *)averageColor2 {
//    
//    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
//    unsigned char rgba[4];
//    CGContextRef context = CGBitmapContextCreate(rgba, 1, 1, 8, 4, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
//    
//    CGContextDrawImage(context, CGRectMake(0, 0, 1, 1), self.CGImage);
//    CGColorSpaceRelease(colorSpace);
//    CGContextRelease(context);
//    
//    if(rgba[3] > 0) {
//        CGFloat alpha = ((CGFloat)rgba[3])/255.0;
//        CGFloat multiplier = alpha/255.0;
//        return [UIColor colorWithRed:((CGFloat)rgba[0])*multiplier
//                               green:((CGFloat)rgba[1])*multiplier
//                                blue:((CGFloat)rgba[2])*multiplier
//                               alpha:alpha];
//    }
//    else {
//        return [UIColor colorWithRed:((CGFloat)rgba[0])/255.0
//                               green:((CGFloat)rgba[1])/255.0
//                                blue:((CGFloat)rgba[2])/255.0
//                               alpha:((CGFloat)rgba[3])/255.0];
//    }
//}
//
//- (UIColor *)averageColor {
//    
//    unsigned char rgba[4];
//    float dimension = 32;
//    NSUInteger bytesPerPixel = 4;
//    NSUInteger bytesPerRow = bytesPerPixel * dimension;
//    NSUInteger bitsPerComponent = 8;
//    unsigned char *rawData = (unsigned char*) calloc(dimension * dimension * 4, sizeof(unsigned char));
//    if(!rawData) return [UIColor blackColor];
//    
//    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
//    CGContextRef context = CGBitmapContextCreate(rawData, dimension, dimension, bitsPerComponent, bytesPerRow, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
//    
//    CGContextDrawImage(context, CGRectMake(0, 0, dimension, dimension), self.CGImage);
//    CGColorSpaceRelease(colorSpace);
//    CGContextRelease(context);
//    
//    
//    long totalRed = 0;
//    long totalGreen = 0;
//    long totalBlue = 0;
//    long totalAlpha = 0;
//    
//    float x = 0;
//    float y = 0;
//    for (int n = 0; n<(dimension*dimension); n++){
//        
//        int index = (bytesPerRow * y) + x * bytesPerPixel;
//        int red   = rawData[index];
//        int green = rawData[index + 1];
//        int blue  = rawData[index + 2];
//        int alpha = rawData[index + 3];
//        
//        totalRed += red;
//        totalGreen += green;
//        totalBlue += blue;
//        totalAlpha += alpha;
//        
//        y++;
//        if (y==dimension){
//            y=0;
//            x++;
//        }
//    }
//    free(rawData);
//    
//    float count = dimension*dimension;
//    rgba[0] = (CGFloat)totalRed/count;
//    rgba[1] = (CGFloat)totalGreen/count;
//    rgba[2] = (CGFloat)totalBlue/count;
//    rgba[3] = (CGFloat)totalAlpha/count;
//    
//    
//    if(rgba[3] > 0) {
//        CGFloat alpha = ((CGFloat)rgba[3])/255.0;
//        CGFloat multiplier = alpha/255.0;
//        return [UIColor colorWithRed:((CGFloat)rgba[0])*multiplier
//                               green:((CGFloat)rgba[1])*multiplier
//                                blue:((CGFloat)rgba[2])*multiplier
//                               alpha:alpha];
//    }
//    else {
//        return [UIColor colorWithRed:((CGFloat)rgba[0])/255.0
//                               green:((CGFloat)rgba[1])/255.0
//                                blue:((CGFloat)rgba[2])/255.0
//                               alpha:((CGFloat)rgba[3])/255.0];
//    }
//}
//
//- (UIColor *)averageDarkColor {
//    
//    unsigned char rgba[4];
//    float dimension = 32;
//    NSUInteger bytesPerPixel = 4;
//    NSUInteger bytesPerRow = bytesPerPixel * dimension;
//    NSUInteger bitsPerComponent = 8;
//    unsigned char *rawData = (unsigned char*) calloc(dimension * dimension * 4, sizeof(unsigned char));
//    if(!rawData) return [UIColor blackColor];
//    
//    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
//    CGContextRef context = CGBitmapContextCreate(rawData, dimension, dimension, bitsPerComponent, bytesPerRow, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
//    
//    CGContextDrawImage(context, CGRectMake(0, 0, dimension, dimension), self.CGImage);
//    CGColorSpaceRelease(colorSpace);
//    CGContextRelease(context);
//    
//    
//    long totalRed = 0;
//    long totalGreen = 0;
//    long totalBlue = 0;
//    long totalAlpha = 0;
//    
//    long countLightPixel = 0;
//    
//    float x = 0;
//    float y = 0;
//    for (int n = 0; n<(dimension*dimension); n++){
//        
//        int index = (bytesPerRow * y) + x * bytesPerPixel;
//        int red   = rawData[index];
//        int green = rawData[index + 1];
//        int blue  = rawData[index + 2];
//        int alpha = rawData[index + 3];
//        
//        if(red >150 && green >150 && blue >150){
//            countLightPixel++;
//        }
//        else{
//            totalRed += red;
//            totalGreen += green;
//            totalBlue += blue;
//            totalAlpha += alpha;
//        }
//        y++;
//        if (y==dimension){
//            y=0;
//            x++;
//        }
//    }
//    free(rawData);
//    
//    float count = dimension*dimension - countLightPixel;
//    rgba[0] = (CGFloat)totalRed/count;
//    rgba[1] = (CGFloat)totalGreen/count;
//    rgba[2] = (CGFloat)totalBlue/count;
//    rgba[3] = (CGFloat)totalAlpha/count;
//    if(0.2126*rgba[0] + 0.7152*rgba[1] + 0.0722* rgba[2] > 128) {
//        if (rgba[3] > 0) {
//            CGFloat alpha = ((CGFloat)rgba[3])/255.0;
//            CGFloat multiplier = alpha/255.0;
//            return [self darkerColorForColor:[UIColor colorWithRed:((CGFloat)rgba[0])*multiplier
//                                                             green:((CGFloat)rgba[1])*multiplier
//                                                              blue:((CGFloat)rgba[2])*multiplier
//                                                             alpha:alpha]];
//        }
//        else {
//            return [self darkerColorForColor:[UIColor colorWithRed:((CGFloat)rgba[0])/255.0
//                                                             green:((CGFloat)rgba[1])/255.0
//                                                              blue:((CGFloat)rgba[2])/255.0
//                                                             alpha:((CGFloat)rgba[3])/255.0]];
//        }
//    }
//    
//    if(rgba[3] > 0) {
//        CGFloat alpha = ((CGFloat)rgba[3])/255.0;
//        CGFloat multiplier = alpha/255.0;
//        return [UIColor colorWithRed:((CGFloat)rgba[0])*multiplier
//                               green:((CGFloat)rgba[1])*multiplier
//                                blue:((CGFloat)rgba[2])*multiplier
//                               alpha:alpha];
//    }
//    else {
//        return [UIColor colorWithRed:((CGFloat)rgba[0])/255.0
//                               green:((CGFloat)rgba[1])/255.0
//                                blue:((CGFloat)rgba[2])/255.0
//                               alpha:((CGFloat)rgba[3])/255.0];
//    }
//}
//
//- (UIColor *)darkerColorForColor:(UIColor *)c
//{
//    CGFloat r, g, b, a;
//    if ([c getRed:&r green:&g blue:&b alpha:&a])
//        return [UIColor colorWithRed:MAX(r - 0.2, 0.0)
//                               green:MAX(g - 0.2, 0.0)
//                                blue:MAX(b - 0.2, 0.0)
//                               alpha:a];
//    return nil;
//}
//
//@end
//
//
