//
//  PrivateAdKey.h
//  Mopub_Sample
//
//  Created by hieplm on 9/29/14.
//  Copyright (c) 2015 ppclink. All rights reserved.
//

#define PRIVATEADKEY_H

/**********************************
 /* CHÚ Ý:
 + Mục đích: để khi thay module quảng cáo mới, có thể copy - past đè cả Module quảng cáo, nhưng file PrivateAdKey.h sẽ không bị ảnh hưởng, và key quảng cáo vẫn được giữ nguyên.
 *********************************/

/////////// KEY riêng của mỗi sản phẩm: lay key tren file Google Docs va dien vao day  ///////
#define ADCONOLY_APP_ID                     @"app81a5fb046cd64a12a0"
#define ADCONOLY_ZONE_ID                    @"vz8cd0d8df81774ae88c"

#define VUNGLE_APP_ID                       @"53fd5fdd8694acfd17000016"

#define kAdmobBannerMediationID             @"ca-app-pub-4154334692952403/3683185772"
#define kAdmobImageInterstitialMediationID  @"ca-app-pub-4154334692952403/1181430571"
#define kAdmobVideoInterstitialMediationID  @"ca-app-pub-4154334692952403/5611630173"
#define kAdmobNativeAdID                    @"ca-app-pub-4154334692952403/3802401181"
#define kAdmobRewardVideoAdID               @"ca-app-pub-4154334692952403/5111898575"

#define MOPUB_AD_BANNER_IPHONE_KEY          @"0ff5eff3a95b43c88eaa8a2d8abc51b4"
#define MOPUB_AD_BANNER_IPAD_KEY            @"cb643476df7040dca19f6e59388ae426"
#define MOPUB_AD_INTERSTITIAL_IPHONE_KEY    @"c454913ecef24c3dbd27f5aea74e4fa3"
#define MOPUB_AD_INTERSTITIAL_IPAD_KEY      @"5e9d9332504d4cca9d768cdbebdc642a"
#define MOPUB_AD_VIDEO_INTERSTITIAL_KEY     @"0e3336e91621422c877c63096a26c7b1"

#define MOPUB_AD_NATIVE_KEY                 @"e92df6b1af6e407eaa48eb5de1b5d056"
#define FACEBOOK_AD_NATIVE_KEY              @"778766028870339_854846577928950"
