//
//  MPMediationAdController.h
//  PPCLINKAds_Sample
//
//  Created by Do Lam on 10/15/14.
//  Copyright (c) 2016 PPCLINK. All rights reserved.
//

#if defined(__has_include)
#if __has_include("PrivateAdKey.h")
#import "PrivateAdKey.h"
#endif
#endif

#import <UIKit/UIKit.h>
#import "UIDevice-Hardware.h"

////////// AD NETWORK NAME DEFINITION ///////
#define kCleverNETNetworkName       @"clevernet"
#define kMopubNetworkName           @"mopub"
#define kAdmobNetworkName           @"admob"
#define kAdconolyNetworkName        @"adcolony"
#define kFacebookNetworkName        @"facebook"
//#define kVungleNetworkName          @"vungle"
#define kPPCLINKNetworkName         @"ppclink"


#import <FBAudienceNetwork/FBAudienceNetwork.h>
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "MoPub.h"
#import "VDConfigNotification.h"

#ifndef __PPCLINKAds_NativeOnly__

// BANNER AD NOTIFICATIONS
#define kNotifyBannerAdDidClick                     @"NotifyBannerAdDidClick"
#define kNotifyMPBannerAdViewWillPresentModalView   @"MPBannerAdViewWillPresentModalView"
#define kNotifyMPBannerAdViewWillDismissModalView   @"MPBannerAdViewWillDismissModalView"
#define kNotifyMPBannerAdViewWillLeaveAppFromAd     @"MPBannerAdViewWillLeaveAppFromAd"
#define kNotifyMPBannerAdViewStartAutoRefresh       @"MPBannerAdViewStartAutoRefresh"
#define kNotifyMPBannerAdViewDidLoadAdSuccessful    @"MPBannerAdViewDidLoadAdSuccessful"

//// INTERSTITIAL IMAGE STATIC NOTIFICATIONS (Quang cao full static ko co video)
#define kInterstitialAdWillAppear       @"InterstitialAdWillAppear"
#define kInterstitialAdWillDissappear   @"InterstitialAdWillDissappear"
#define kInterstitialAdDidClick         @"InterstitialAdDidClick"

////// INTERSTITIAL VIDEO NOTIFICATIONS
//// Notifications when start playing ad video, stop playing ad video
//// Riêng notify kFinishVideoAdWatching có kèm tham số [NSNumber numberWithBool:YES/NO],
//// YES : video đươc xem hết -> được cộng điểm, NO: user skip video -> không được cộng điểm
#define kStartVideoAdWatching         @"StartVideoAdWatching"
#define kFinishVideoAdWatching        @"FinishVideoAdWatching"

typedef enum
{
    INTERSTITIAL_AD_TYPE_ALL = 0,
    INTERSTITIAL_AD_TYPE_IMAGE_ONLY,
    INTERSTITIAL_AD_TYPE_VIDEO_ONLY
} INTERSTITIAL_AD_TYPE;

#endif

#define kNotifyInterstitialCheckAvaibilityStatus    @"NotifyInterstitialCheckAvaibilityStatus"

@protocol PLNativeAdMediationDelegate <NSObject>

@required
/* 
 Được gọi khi quảng cáo native đã được load về thành công
 */
- (void) nativeAdDidLoad:(NSObject *)nativeAd fromAdNetwork:(NSString *)adNetworkName;
@end

#ifdef __PPCLINKAds_NativeOnly__
@interface MPMediationAdController : UIViewController <FBNativeAdDelegate>
#else
@interface MPMediationAdController : UIViewController <MPAdViewDelegate,MPInterstitialAdControllerDelegate,
GADBannerViewDelegate,GADInterstitialDelegate,
FBNativeAdDelegate, GADNativeAppInstallAdLoaderDelegate, GADNativeContentAdLoaderDelegate>
#endif

+ (MPMediationAdController *)sharedManager;
// Goi ham nay khi mua inapp RemoveAd
- (void)removeAds;


@property (nonatomic, weak) id<PLNativeAdMediationDelegate> nativeAdDelegate;
- (void)startTimerCheckingInterstitialAvailability;
/**
 *  Thông báo cho adcontroller biết rằng app đã in-app purchased hay chưa.
 *  Gọi hàm này ngay khi khởi tạo adcontroller, và khi user mua in-app thành công, hoặc restore previous purchase thành công.
 *  Nếu quên gọi hàm này, khả năng app vẫn sẽ hiện quảng cáo như bản free.
 */
- (void) setProductType:(PRODUCT_TYPE)productType;

// NATIVE AD methods
- (void)initNativeAd;

//- (void)requestNativeAd;
//
- (NSString *)getCurrentNativeAdNetworkName;
///**
// * Trả về một đối tượng Native Ad. = nil nếu chưa lấy cache được Native Ad nào.
// * Sau
// */
//- (NSObject *)getCurrentNativeAdObject;

#define kNativeAdInfo_NetworkName           @"NativeAdInfo_NetworkName"
#define kNativeAdInfo_Object                @"NativeAdInfo_Object"

- (NSDictionary *)getNativeAdInfoToShow;


#ifndef __PPCLINKAds_NativeOnly__
/**
 *  Khi thêm banner ad vào view nào đó, không add thêm mp_adView mà thêm bannerAdView.
 *  View trung gian này nhằm giúp chỉnh lại vị trí banner ad khi kích thước trả về có giá trị khác nhau (vd banner ad trên ipad).
 */
@property (nonatomic, strong) UIView *bannerAdView;

/**
 *  Khi người dùng tap vào banner ad, trang quảng cáo sẽ bung lên dạng modal view, và cần truyền một rootviewcontroller cho nó.
 *  Thông thường chọn rootviewcontroller là viewcontroller đang ở top view.
 */
@property (nonatomic, weak) UIViewController *rootViewControllerBannerAd;

////////// INTERSTITIAL AD //////////////
/**
 *  MPInterstitialAdController là quảng cáo fullscreen.
 *  Khi fullscreen ad bung lên theo dạng modal view, cần chọn cho nó rootviewconller. Cũng tương tự như banner ad.
 */
@property (nonatomic, weak) UIViewController *rootViewControllerFullscreenAd;

@property (nonatomic, strong) void (^interstitialAdClose)(void);
@property (nonatomic, strong) void (^interstitialAdClick)(void);
//@property (nonatomic, strong) void (^videoAdWatchCompletely)(NSDictionary*);
/**
 * Ad Management Singleton
 */

/**
 *  Các hàm ẩn/hiện quảng cáo banner. bBannerAdIsShowing trả về YES khi quảng cáo banner đang được hiện, NO trong trường hợp còn lại.
 */
- (void)showBannerAd:(void (^)(MPMediationAdController *mediationAdController, BOOL bShowResult))bannerShowResult;
- (void) showBannerAd;

- (void) hideBannerAd;
- (BOOL) bBannerAdIsShowing;

// Kiểm tra xem hiện tại quảng cáo interstitial có đang được hiện không
- (BOOL) isShowingInterstitialAd;

// Kiểm tra xem hiện tại đã đủ điều kiện nhận thưởng chưa: user phải click hoặc xem hết video interstitial
- (BOOL) isInterstitialAdQualifiedForBonus;

- (CGSize)getAdBannerCGSize;

/////////// FULSCREEN AD HANDLE ////////

/**
 * Check Interstitial ad is ready to show or not
 * @param adType The interstitial ad type(Video/Image/All)
 * @return YES/NO
 */
- (NSString*) isInterstitialAdAvailable:(INTERSTITIAL_AD_TYPE)adType;

/**
 * Check Interstitial ad is ready to show or not
 * @param adType The interstitial ad type(Video/Image/All)
 * @param bBonus for bonus or not: will check all network except AdMob incase bBonus = YES
 */
- (NSString*) isInterstitialAdAvailable:(INTERSTITIAL_AD_TYPE)adType forBonus:(BOOL)bBonus;

- (NSString*) isInterstitialAdAvailableForBonus:(INTERSTITIAL_AD_TYPE)adType DEPRECATED_MSG_ATTRIBUTE("Use isInterstitialAdAvailable:forBonus instead, with bBonus = YES");

/**
 * Kiểm tra xem thời điểm hiện tại có được phép hiện quảng cáo interstitial hay ko: phụ thuộc vào điều kiện trên server, Free/paid product, InAdFreeTime,... Trường hợp Interstitial force show sẽ luôn hiện và không check đến hàm này.
 */
- (BOOL)isInterstitialAdAllowedtoShow;
/**
 * Show Interstitial ad
 * @param adType The interstitial ad type (Video/Image/All)
 * @param bYesNo:
 = NO: có xét đến các điều kiện rằng buộc như: 
    + Config trên server như (fullScreenAdFreeBetweenAdShowMinTimeInterval, fullScreenAdFreeTimeIntervalBonus...). Đủ điều kiện mới hiện.
    + Có đang trong thời gian thưởng Ad-Free hay không?
    + Chuong trinh đã đươc nâng cap bản Pro hay chưa?
 = YES: force show (luôn hiện trong bất cứ trường hợp nào!)
*/
- (void)showInterstitialAd:(INTERSTITIAL_AD_TYPE)adType forceShow:(BOOL)bForceShow;

/**
 * Show Interstitial ad: tương tự hàm ở trên nhưng thêm hỗ trợ callback code block
Ex implementation:
 success:^(MPMediationAdController *mediationAdController)
 {
        mediationAdController.interstitialAdClose= ^{
        NSLog(@"interstitialAdClose");
        };
 
        mediationAdController.interstitialAdClick = ^{
        NSLog(@"interstitialAdClick");
        };
 }
 
 failure:^(NSString *error)
 {
    NSLog(@"%@", error);
 }];
 */
- (void)showInterstitialAd:(INTERSTITIAL_AD_TYPE)adType forceShow:(BOOL)bForceShow success:(void (^)(MPMediationAdController *mediationAdController))success failure:(void (^)(NSString *error))failure;

#ifndef __PPCLINKAds_Lite__
/**
 * Kiểm tra quảng cáo Reward Video đã cache và sẵn sàng để hiện chưa
 */
- (NSString*)isRewardedVideoAvailable;
/**
 * Hiện quảng cáo reward Video. Nếu user xem hết video hoặc click vào quảng cáo, rewardSuccess sẽ được gọi kèm theo các
 thông tin trong rewardInfo
 */
- (void)showRewardedVideoWithSuccessReward:(void (^)(NSDictionary *rewardInfo))rewardSuccess;
#endif
/**
 * Force show Interstitial ad with bonus info
 * @param adType The interstitial ad type (Video/Image/All)
 * @param dictBonusInfo: all things that you want to pass with Notification when user view video or click fullscreen ad
*/
- (void)showInterstitialAd:(INTERSTITIAL_AD_TYPE)adType withBonusInfo:(NSDictionary*)dictBonusInfo;

/** Similar showInterstitialAd:forceShow: with adType = INTERSTITIAL_AD_TYPE_ALL, bForceShow = NO
 */
- (void)showInterstitialAd;


- (NSString*)currentRunningBannerAd;
- (NSString*)currentRunningIntertitialAd;

// Cập nhật lại thời gian expired free quảng cáo bằng cách cộng thêm nTimeInterval giây
- (void)updateInterstitialAdFreeExpiredDate:(NSTimeInterval)nTimeInterval;

#pragma mark _Deprecated
/**
 * Hàm này tương tự hàm showInterstitialAd: forceShow: chỉ khác tham số bYesNo ngược nhau. Hàm này đã deprecated (sẽ bỏ trong tương lại vì thừa). Cần dùng hàm thay thế là: showInterstitialAd: forceShow: 
 */
- (void)showInterstitialAd:(INTERSTITIAL_AD_TYPE)adType withConstraintCondition:(BOOL)bYesNo __attribute__((deprecated));

#endif
@end
