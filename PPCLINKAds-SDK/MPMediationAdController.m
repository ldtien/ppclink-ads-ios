//
//  MPMediationAdController.m
//  PPCLINKAds_Sample
//
//  Created by Do Lam on 10/15/14.
//  Copyright (c) 2016 PPCLINK. All rights reserved.
//

#import "MPMediationAdController.h"
#import "MPAdConversionTracker.h"

#import "VDUtilities.h"

#import <AdSupport/ASIdentifierManager.h>
#include <CommonCrypto/CommonDigest.h>

#ifndef __PPCLINKAds_NativeOnly__
#ifndef __PPCLINKAds_Lite__
#import <AdColony/AdColony.h>
//#import <VungleSDK/VungleSDK.h>
#endif
#endif

//AdID configuration key
#define kConfig_adid_adcolony_appid                 @"adid_adcolony_appid"
#define kConfig_adid_adcolony_zoneid                @"adid_adcolony_zoneid"
//#define kConfig_adid_adcolony_reward_zoneid         @"adid_adcolony_reward_zoneid"
#define kConfig_adid_vungle_appid                   @"adid_vungle_appid"

#define kConfig_adid_admob_banner                   @"adid_admob_banner"
#define kConfig_adid_admob_interstitial_image       @"adid_admob_interstitial_image"
#define kConfig_adid_admob_interstitial_video       @"adid_admob_interstitial_video"
#define kConfig_adid_admob_native                   @"adid_admob_native"
#define kConfig_adid_admob_rewardvideo              @"adid_admob_rewardvideo"

#define kConfig_adid_mopub_phone_banner             @"adid_mopub_phone_banner"
#define kConfig_adid_mopub_phone_full               @"adid_mopub_phone_full"
#define kConfig_adid_mopub_tablet_leaderboard       @"adid_mopub_tablet_leaderboard"
#define kConfig_adid_mopub_tablet_full              @"adid_mopub_tablet_full"
#define kConfig_adid_mopub_video                    @"adid_mopub_video"
#define kConfig_adid_mopub_native                   @"adid_mopub_native"

#define kConfig_adid_facebook_native                @"adid_facebook_native"

//KEY quang cao default
#define DEFAULT_ADIDs        @{ @"adid_adcolony_appid":             @"app81a5fb046cd64a12a0",\
                                @"adid_adcolony_zoneid":            @"vz8cd0d8df81774ae88c",\
                                \
                                @"adid_vungle_appid":               @"53ba08326e325bea6500002f",\
                                \
                                @"adid_admob_banner":               @"ca-app-pub-4154334692952403/3683185772",\
                                @"adid_admob_interstitial_image":   @"ca-app-pub-4154334692952403/1181430571",\
                                @"adid_admob_interstitial_video":   @"ca-app-pub-4154334692952403/5611630173",\
                                @"adid_admob_native":               @"ca-app-pub-4154334692952403/3802401181",\
                                @"adid_admob_rewardvideo":          @"ca-app-pub-4154334692952403/5111898575",\
                                \
                                @"adid_mopub_phone_banner":         @"0ff5eff3a95b43c88eaa8a2d8abc51b4",\
                                @"adid_mopub_phone_full":           @"c454913ecef24c3dbd27f5aea74e4fa3",\
                                @"adid_mopub_tablet_leaderboard":   @"cb643476df7040dca19f6e59388ae426",\
                                @"adid_mopub_tablet_full":          @"5e9d9332504d4cca9d768cdbebdc642a",\
                                @"adid_mopub_video":                @"0e3336e91621422c877c63096a26c7b1",\
                                @"adid_mopub_native":               @"e92df6b1af6e407eaa48eb5de1b5d056",\
                                \
                                @"adid_facebook_native":            @"778766028870339_854846577928950",\
                               }


#define keyinterstitialAdFreeExpiredDate                @"interstitialAdFreeExpiredDate"

@interface MPMediationAdController ()
{
    ////////// Product Type ///////////////
    PRODUCT_TYPE    _productType;
    BOOL                bAppDidEnterForeground;
    NSMutableArray      *nativeAdNetworkGroup;
    NSObject                *curNativeAd;
    NSString                *sCurNativeAdNetworkName;
    int                     nCurIndexNativeAdNetwork;
    
    BOOL bIsRequestingNativeAd;
    BOOL bUseNativeAd;
    
    int nCountShowInterstitialAd;
  
#ifndef __PPCLINKAds_NativeOnly__
    ////////// BANNER AD //////////////////
    MPAdView            *mopubBannerAd;
    GADBannerView       *admobBannerAd;
    BOOL                isBannerMopub;  //True neu nhu banner thuoc kieu Mopub. False nguoc lai
    
    BOOL                isMopubBannerAdReady;
    BOOL                isAdmobBannerAdReady;
    BOOL                isMillennialBannerAdReady;
    
    NSMutableArray      *bannerAdNetworkGroup;
    int                 currentBannerAdNetworkIndex;
    
    ////////// FULLSCREEN AD //////////////
    MPInterstitialAdController  *mopubInterstitial;
    MPInterstitialAdController  *mopubVideoInterstitial;
    GADInterstitial             *admobInterstitial;    // For Admob static image Interstitial
    GADInterstitial             *admobVideoInterstitial;  // For Admob Video Interstitial
    
    NSTimer             *timerInterstitialAdFreeTime;
    NSTimer             *checkIntertitialAvaibilityTimer;
    
    //BOOL                isAdconolyZoneIDReady;
    
    NSMutableArray      *interstitialAdNetworkGroup;
    int                 currentSelectedInterstitialAdNetworkIndex;
    
    NSMutableArray      *videoInterstitialAdNetworkGroup;
    
    BOOL bBannerAdIsRequestedToShow;    /* Use to store banner ad state, YES if banner ad is able to show, NO in others case */
    
    ///////// AdNetwork Initialized mark //////
    BOOL _bDidInitMopubInterstitial;
    BOOL _bDidInitAdmobInterstitial;
    BOOL _bDidInitAdcolony;
    BOOL bNeedRequestNewAdmobRewardVideo;
    
    INTERSTITIAL_AD_TYPE  curLoadingInterstitialAdtype;
    
    NSMutableDictionary     *dictCurBonusInfo;
    BOOL                    bIsQualifiedBonus;  // Đủ điều kiện thưởng khi xem/click quảng cáo hay không
    BOOL                    bShowingInterstitialAd;
    
    void (^interstitialAdShowSuccess)(MPMediationAdController*);
    void (^interstitialAdShowFailure)(NSString*);
    void (^bannerAdShowResult)(MPMediationAdController*, BOOL);
    
#ifndef __PPCLINKAds_Lite__
    void (^rewardVideoSuccess)(NSDictionary*);
    AdColonyInterstitial *_adColonyInterstitalAd;
    // AdColonyInterstitial *_adColonyRewardVideo;
#endif
#endif
    
}

/// You must keep a strong reference to the GADAdLoader during the ad loading process.
@property(nonatomic, strong) GADAdLoader *adLoader;

@end


@implementation MPMediationAdController

+ (MPMediationAdController *)sharedManager
{
    static MPMediationAdController *_sharedManager;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[super allocWithZone:NULL] init];
        
    });
    
    return _sharedManager;
}

- (NSString*)getHashedDeviceID
{
    NSUUID* adid = [[ASIdentifierManager sharedManager] advertisingIdentifier];
    const char *cStr = [adid.UUIDString UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, strlen(cStr), digest );
    NSMutableString *sHashedDeviceID = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [sHashedDeviceID appendFormat:@"%02x", digest[i]];
    
    return sHashedDeviceID;
}

- (BOOL)iPhone4AndLower
{
    NSString* deviceModelIdentifier = [[UIDevice currentDevice] modelIdentifier];
    if ([deviceModelIdentifier hasPrefix:@"iPhone4,"] || [deviceModelIdentifier hasPrefix:@"iPhone3,"])
        return YES;
    
    if ([deviceModelIdentifier hasPrefix:@"iPod"])
        return ([deviceModelIdentifier compare:@"iPod4,1"] == NSOrderedAscending);
    
    return NO;
}

#ifndef __PPCLINKAds_NativeOnly__
- (GADRequest*) refineGADRequest
{
    GADRequest *request = [GADRequest request];

    // Make the request for a test ad. Put in an identifier for
    // the simulator as well as any devices you want to receive test ads.
    
//#if __RELEASEMODE__
    
//#else
    
    NSString *sHashedDeviceID = nil;
// Chỉ áp dụng bật/tắt TestMode trong thời gian test hoặc Apple Review
    if ([g_vdConfigNotification isNewVersionInTestOrReviewTime] && [[g_vdConfigNotification._config objectForKey:kConfig_AdTestMode] boolValue])
    {
        sHashedDeviceID = [self getHashedDeviceID];
    }
    
    request.testDevices = [NSArray arrayWithObjects:kGADSimulatorID, sHashedDeviceID, nil];
//#endif
    
    return request;
}

#endif

- (id) init
{
    if (self = [super init])
    {
        //VD config
        if (!g_vdConfigNotification)
            g_vdConfigNotification = [[VDConfigNotification alloc] initWithProductType:PT_FREE];
        
        _productType = PT_FREE;
        bAppDidEnterForeground = YES;
        bUseNativeAd = NO;
        bNeedRequestNewAdmobRewardVideo = YES;
        
#ifndef __PPCLINKAds_NativeOnly__
        interstitialAdShowSuccess = nil;
        interstitialAdShowFailure = nil;
        bannerAdShowResult = nil;
        
#ifndef __PPCLINKAds_Lite__
        rewardVideoSuccess = nil;
        _adColonyInterstitalAd = nil;
        // _adColonyRewardVideo = nil;
#endif
        self.interstitialAdClick = nil;
        self.interstitialAdClose = nil;
        //self.videoAdWatchCompletely = nil;

        timerInterstitialAdFreeTime = nil;
        bShowingInterstitialAd = NO;
        checkIntertitialAvaibilityTimer = nil;
        dictCurBonusInfo = nil;
        bIsQualifiedBonus = NO;
        _bDidInitMopubInterstitial = NO;
        _bDidInitAdmobInterstitial = NO;
        _bDidInitAdcolony = NO;
        
        isMopubBannerAdReady = NO;
        isAdmobBannerAdReady = NO;
        //isAdconolyZoneIDReady = NO;
        
        currentBannerAdNetworkIndex = 108;
        currentSelectedInterstitialAdNetworkIndex = 0;
        
        mopubBannerAd = nil;
        admobBannerAd = nil;
        
        ///// Init base view
        CGSize sizeAdBanner = [self getAdBannerCGSize];
        CGRect recMainView = CGRectMake(0, 0, sizeAdBanner.width, sizeAdBanner.height);
        if (!self.bannerAdView)
        {
            self.bannerAdView = [[UIView alloc] initWithFrame:recMainView];
            self.bannerAdView.backgroundColor = [UIColor clearColor];
            self.bannerAdView.clipsToBounds = YES;
        }
        
        bannerAdNetworkGroup = [[NSMutableArray alloc] initWithObjects:kAdmobNetworkName, nil];
        interstitialAdNetworkGroup = [[NSMutableArray alloc] initWithObjects:kAdmobNetworkName, nil];
        videoInterstitialAdNetworkGroup = [[NSMutableArray alloc] initWithObjects:kAdmobNetworkName, nil];
        
#endif
        nativeAdNetworkGroup = [[NSMutableArray alloc] initWithObjects:kFacebookNetworkName, kMopubNetworkName, nil];
        
        [self initAdNetworksComponent];
        [self updateAdConfigParam];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedUpdateAdConfigNotification) name:kNotifyDidLoadNewConfigNotification object:nil];
        
       
    }
    
    return self;
}

/*
 Ham chung de lay cac loại key quang cáo, hoạt động theo quy tắc sau:
 - First, lấy key quảng cáo từ server config
 - Nếu ko lấy được từ server thì lấy key define trong PrivateKey.h
 - Nếu ko define key trong PrivateAdKey.h hoặc không có file PrivateAdKey.h thì sẽ lấy key mặc định
 */
- (NSString *)adIDWithConfigKey:(NSString*)configKey
{
    NSString* adID = [g_vdConfigNotification._config objectForKey:configKey];
    
    if ([adID length] < 3) // Nếu key lấy về không hợp lệ
    {
        if ([configKey isEqualToString:kConfig_adid_admob_banner])
        {
#ifdef kAdmobBannerMediationID
            adID = kAdmobBannerMediationID;
#endif
        }
        else if ([configKey isEqualToString:kConfig_adid_admob_interstitial_image])
        {
#ifdef kAdmobImageInterstitialMediationID
            adID = kAdmobImageInterstitialMediationID;
#endif
        }
        else if ([configKey isEqualToString:kConfig_adid_admob_interstitial_video])
        {
#ifdef kAdmobVideoInterstitialMediationID
            adID = kAdmobVideoInterstitialMediationID;
#endif
        }
        else if ([configKey isEqualToString:kConfig_adid_admob_native])
        {
#ifdef kAdmobNativeAdID
            adID = kAdmobNativeAdID;
#endif
        }
        else if ([configKey isEqualToString:kConfig_adid_admob_rewardvideo])
        {
#ifdef kAdmobRewardVideoAdID
            adID = kAdmobRewardVideoAdID;
#endif
        }
        else if ([configKey isEqualToString:kConfig_adid_mopub_phone_banner])
        {
#ifdef MOPUB_AD_BANNER_IPHONE_KEY
            adID = MOPUB_AD_BANNER_IPHONE_KEY;
#endif
        }
        else if ([configKey isEqualToString:kConfig_adid_mopub_phone_full])
        {
#ifdef MOPUB_AD_INTERSTITIAL_IPHONE_KEY
            adID = MOPUB_AD_INTERSTITIAL_IPHONE_KEY;
#endif
        }
        else if ([configKey isEqualToString:kConfig_adid_mopub_tablet_leaderboard])
        {
#ifdef MOPUB_AD_BANNER_IPAD_KEY
            adID = MOPUB_AD_BANNER_IPAD_KEY;
#endif
        }
        else if ([configKey isEqualToString:kConfig_adid_mopub_tablet_full])
        {
#ifdef MOPUB_AD_INTERSTITIAL_IPAD_KEY
            adID = MOPUB_AD_INTERSTITIAL_IPAD_KEY;
#endif
        }
        else if ([configKey isEqualToString:kConfig_adid_mopub_video])
        {
#ifdef MOPUB_AD_VIDEO_INTERSTITIAL_KEY
            adID = MOPUB_AD_VIDEO_INTERSTITIAL_KEY;
#endif
        }
        else if ([configKey isEqualToString:kConfig_adid_mopub_native])
        {
#ifdef MOPUB_AD_NATIVE_KEY
            adID = MOPUB_AD_NATIVE_KEY;
#endif
        }
        else if ([configKey isEqualToString:kConfig_adid_adcolony_appid])
        {
#ifdef ADCONOLY_APP_ID
            adID = ADCONOLY_APP_ID;
#endif
        }
        else if ([configKey isEqualToString:kConfig_adid_adcolony_zoneid])
        {
#ifdef ADCONOLY_ZONE_ID
            adID = ADCONOLY_ZONE_ID;
#endif
        }
        else if ([configKey isEqualToString:kConfig_adid_vungle_appid])
        {
#ifdef VUNGLE_APP_ID
            adID = VUNGLE_APP_ID;
#endif
        }
        else if ([configKey isEqualToString:kConfig_adid_facebook_native])
        {
#ifdef FACEBOOK_AD_NATIVE_KEY
            adID = FACEBOOK_AD_NATIVE_KEY;
#endif
        }
    }
    
    if ([adID length] < 3)
        adID = [DEFAULT_ADIDs objectForKey:configKey];
    
    return adID;
}
/*
#pragma mark - AlertView Delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag = 77)
    {
        exit(1);
    }
}
*/
/*
 Init all adnetwork instance by config
 */
- (void) initAdNetworksComponent
{
    //if ([g_vdConfigNotification isInAdFreeTime])
    //    return;
// Nếu không có mạng thì chưa khởi tạo, tránh lỗi lần đầu tiên user mở app, admob khởi tạo ko thành công và ko
// trả về ad ngay cả khi bật mạng trở lại
    if (![VDUtilities checkInternetConnection])
        return;

    if (_productType == PT_PAID)
        return;
    
#ifndef __PPCLINKAds_NativeOnly__
/// MOPUB BANNER
    if (!mopubBannerAd && [bannerAdNetworkGroup containsObject:kMopubNetworkName])
    {
        NSLog(@"PPCLINKADS: Init AdNetwork MOPUB BANNER");
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
            mopubBannerAd = [[MPAdView alloc] initWithAdUnitId:[self adIDWithConfigKey:kConfig_adid_mopub_phone_banner] size:MOPUB_BANNER_SIZE];
        else
            //mopubBannerAd = [[MPAdView alloc] initWithAdUnitId:MOPUB_AD_BANNER_IPAD_KEY size:MOPUB_LEADERBOARD_SIZE];
            mopubBannerAd = [[MPAdView alloc] initWithAdUnitId:[self adIDWithConfigKey:kConfig_adid_mopub_tablet_leaderboard] size:CGSizeMake(768, 90)];
        mopubBannerAd.delegate = self;
        mopubBannerAd.accessibilityLabel = @"banner";
        mopubBannerAd.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        mopubBannerAd.hidden = YES;

#if DEBUG
      //  mopubBannerAd.testing = YES;  //Comment lại vi o che do test mopub hiện ko lay dc quang cao
#endif

        
    }
    
/// MOPUB INTERSTITIAL
    if (!_bDidInitMopubInterstitial && ![self iPhone4AndLower] &&([interstitialAdNetworkGroup containsObject:kMopubNetworkName] ||
                                        [videoInterstitialAdNetworkGroup containsObject:kMopubNetworkName]))
    {
        NSLog(@"PPCLINKADS: Init AdNetwork MOPUB INTERSTITIAL");
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
            mopubInterstitial = [MPInterstitialAdController interstitialAdControllerForAdUnitId:[self adIDWithConfigKey:kConfig_adid_mopub_phone_full]];
        else
            mopubInterstitial = [MPInterstitialAdController interstitialAdControllerForAdUnitId:[self adIDWithConfigKey:kConfig_adid_mopub_tablet_full]];
        
        mopubInterstitial.delegate = self;
        
        mopubVideoInterstitial = [MPInterstitialAdController interstitialAdControllerForAdUnitId:[self adIDWithConfigKey:kConfig_adid_mopub_video]];
        mopubVideoInterstitial.delegate = self;
        
#if DEBUG ///Comment lại vi o che do test mopub hiện ko lay dc quang cao
//        mopubInterstitial.testing = YES;
//        mopubVideoInterstitial.testing = YES;
#endif
        
        [mopubInterstitial loadAd];
        [mopubVideoInterstitial loadAd];
        
        _bDidInitMopubInterstitial = YES;
    }
    
//// ADMOB BANNER
    if (!admobBannerAd && [bannerAdNetworkGroup containsObject:kAdmobNetworkName])
    {
        NSLog(@"PPCLINKADS: Init AdNetwork ADMOB BANNER");
    
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
            admobBannerAd = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
        else
            admobBannerAd = [[GADBannerView alloc] initWithAdSize:kGADAdSizeLeaderboard];
        
        admobBannerAd.adUnitID = [self adIDWithConfigKey:kConfig_adid_admob_banner];
        admobBannerAd.rootViewController = self;
        [admobBannerAd setDelegate:self];
        admobBannerAd.hidden = YES;
    }
    
// ADMOB INTERSTITIAL
    if (!_bDidInitAdmobInterstitial && ![self iPhone4AndLower] && ([interstitialAdNetworkGroup containsObject:kAdmobNetworkName] ||
                                        [videoInterstitialAdNetworkGroup containsObject:kAdmobNetworkName]))
    {
        NSLog(@"PPCLINKADS: Init AdNetwork ADMOB INTERSTITIAL");
        
        admobInterstitial = [[GADInterstitial alloc] initWithAdUnitID:[self adIDWithConfigKey:kConfig_adid_admob_interstitial_image]];
        admobInterstitial.delegate = self;
        
        admobVideoInterstitial = [[GADInterstitial alloc] initWithAdUnitID:[self adIDWithConfigKey:kConfig_adid_admob_interstitial_video]];
        admobVideoInterstitial.delegate = self;
        
        //PreLoad for the first time
        [admobInterstitial loadRequest:[self refineGADRequest]];
        [admobVideoInterstitial loadRequest:[self refineGADRequest]];
        
        _bDidInitAdmobInterstitial = YES;
    }
#ifndef __PPCLINKAds_Lite__
// ADMOB
    if (([interstitialAdNetworkGroup containsObject:kAdmobNetworkName] ||
                               [videoInterstitialAdNetworkGroup containsObject:kAdmobNetworkName]))
    {
        [self initAdmobRewardedVideo];
    }
    
// ADCOLONY
    if (!_bDidInitAdcolony && ([interstitialAdNetworkGroup containsObject:kAdconolyNetworkName] ||
                               [videoInterstitialAdNetworkGroup containsObject:kAdconolyNetworkName]))
    {
        [self initAdColony];
    }
    
// VUNGLE
//    if (([interstitialAdNetworkGroup containsObject:kVungleNetworkName] ||
//                             [videoInterstitialAdNetworkGroup containsObject:kVungleNetworkName]))
//    {
//        [self initVungle];
//    }
#endif
#endif
    
}


#pragma mark - Notifications
- (void) receivedUpdateAdConfigNotification
{
    NSLog(@"MPAdViewController: Did receive update config notification");
    
    [self updateAdConfigParam];
}

- (void)appWillEnterForeground
{
    bAppDidEnterForeground = YES;
}


- (void)appDidBecomeActive
{
    if (!bAppDidEnterForeground)
        return;
    bAppDidEnterForeground = NO;
    
    nCountShowInterstitialAd = 0;
    
// Chỉ áp dụng bật/tắt TestMode trong thời gian test hoặc Apple Review
    if ([g_vdConfigNotification isNewVersionInTestOrReviewTime] && [[g_vdConfigNotification._config objectForKey:kConfig_AdTestMode] boolValue])
    {
        UIAlertView *al = [[UIAlertView alloc] initWithTitle:@"AdMob TEST MODE" message:@"Bạn đang ở chế độ TEST của quảng cáo ADMOB và có thể test click quang cáo thoải mái! Khi release sản phẩm hoặc muốn test qc thật, nhớ báo lại với admin. Cám ơn! " delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [al show];
    }
    
    // Mopub Conversion Tracking
    NSString* sItunesApplicationID = [VDConfigNotification getItunesApplicationID];
    if (sItunesApplicationID)
        [[MPAdConversionTracker sharedConversionTracker] reportApplicationOpenForApplicationID:sItunesApplicationID];
    
    //[self checkAdKeyConfig];
    // Call these method to reload ad after Internet connection change or Ad-Free time expired
    // Gọi các method này để cập nhật lại load lại các mạng quảng cáo sau thời gian mất mạng hoặc khi thời hạn free quảng cáo đã hết.
    [self initAdNetworksComponent];
    
#ifndef __PPCLINKAds_NativeOnly__
    [self updateAdBannerVisibility];
    [self isInterstitialAdAvailable:INTERSTITIAL_AD_TYPE_ALL];
    
    //NSLog([NSString stringWithFormat:@"appDidBecomeActive.isAdconolyZoneIDReady = %@", isAdconolyZoneIDReady?@"YES":@"NO"]);
    /// Reactive adconoly video
    //isAdconolyZoneIDReady = NO;
    
    NSDate* interstitialAdFreeExpiredDate = [[NSUserDefaults standardUserDefaults] objectForKey:keyinterstitialAdFreeExpiredDate];
        NSDate *dateExpiredOpenApp = [[VDConfigNotification getCurrentDate] dateByAddingTimeInterval:[[g_vdConfigNotification._config objectForKey:kConfig_InterstitialAdFreeWhenOpenAppMinTimeInterval] intValue]];
    
    if (!interstitialAdFreeExpiredDate || [dateExpiredOpenApp compare:interstitialAdFreeExpiredDate] == NSOrderedDescending)
    {
        [[NSUserDefaults standardUserDefaults] setObject:dateExpiredOpenApp forKey:keyinterstitialAdFreeExpiredDate];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
#endif
    
    if (bUseNativeAd)
        [self initNativeAd];
}


- (void)removeAds
{
#ifndef __PPCLINKAds_NativeOnly__
    mopubBannerAd = nil; mopubInterstitial = nil; mopubVideoInterstitial = nil;
    admobBannerAd = nil; admobInterstitial = nil; admobVideoInterstitial = nil;
#endif
    
    sCurNativeAdNetworkName = nil;  curNativeAd = nil;
    //[AdColony turnAllAdsOff];
}

#pragma mark - Update Ad Config Params

- (void) updateAdConfigParam
{
     NSLog(@"%s",__PRETTY_FUNCTION__);

    NSArray *currentNativeAdNetworkGroup = [[g_vdConfigNotification._config objectForKey:kConfig_PPCLINKAdsMediationNativeAd] componentsSeparatedByString:@"#"];
    
    if ([currentNativeAdNetworkGroup count] > 0 && ![nativeAdNetworkGroup isEqualToArray:currentNativeAdNetworkGroup])
    {
        [nativeAdNetworkGroup setArray:currentNativeAdNetworkGroup];
    }
    
#ifndef __PPCLINKAds_NativeOnly__
// Update BANNER ad config
    NSArray *currentBannerNetworkGroup = [[g_vdConfigNotification._config objectForKey:kConfig_PPCLINKAdsMediationBanner] componentsSeparatedByString:@"#"];
    if ([currentBannerNetworkGroup count] > 0 && ![bannerAdNetworkGroup isEqualToArray:currentBannerNetworkGroup])
    {
        [bannerAdNetworkGroup removeAllObjects];
        [bannerAdNetworkGroup addObjectsFromArray:currentBannerNetworkGroup];
        
        [self initAdNetworksComponent];
        [self updateAdBannerVisibility];
    }
    
// Update Interstitial ad config
    NSArray *currentInterstitialNetworkGroup = [[g_vdConfigNotification._config objectForKey:kConfig_PPCLINKAdsMediationInterstitial] componentsSeparatedByString:@"#"];
    
    if ([currentInterstitialNetworkGroup count] > 0 && ![interstitialAdNetworkGroup isEqualToArray:currentInterstitialNetworkGroup])
    {
        currentSelectedInterstitialAdNetworkIndex = 0;
        [interstitialAdNetworkGroup setArray:currentInterstitialNetworkGroup];
        
        [self initAdNetworksComponent];
    }
    
    NSArray *currentVideoInterstitialNetworkGroup = [[g_vdConfigNotification._config objectForKey:kConfig_PPCLINKAdsMediationVideoInterstitial] componentsSeparatedByString:@"#"];
    
    if ([currentVideoInterstitialNetworkGroup count] > 0 && ![videoInterstitialAdNetworkGroup isEqualToArray:currentVideoInterstitialNetworkGroup])
    {
        [videoInterstitialAdNetworkGroup setArray:currentVideoInterstitialNetworkGroup];
        
        [self initAdNetworksComponent];
    }

    // Update Ad ID for some ad networks
    if (admobBannerAd && ![admobBannerAd.adUnitID isEqualToString:[self adIDWithConfigKey:kConfig_adid_admob_banner]])
        admobBannerAd.adUnitID = [self adIDWithConfigKey:kConfig_adid_admob_banner];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (mopubBannerAd && ![mopubBannerAd.adUnitId isEqualToString:[self adIDWithConfigKey:kConfig_adid_mopub_phone_banner]])
            mopubBannerAd.adUnitId = [self adIDWithConfigKey:kConfig_adid_mopub_phone_banner];
        if (mopubInterstitial && ![mopubInterstitial.adUnitId isEqualToString:[self adIDWithConfigKey:kConfig_adid_mopub_phone_full]])
            mopubInterstitial.adUnitId = [self adIDWithConfigKey:kConfig_adid_mopub_phone_full];
    }
    else
    {
        if (mopubBannerAd && ![mopubBannerAd.adUnitId isEqualToString:[self adIDWithConfigKey:kConfig_adid_mopub_tablet_leaderboard]])
            mopubBannerAd.adUnitId = [self adIDWithConfigKey:kConfig_adid_mopub_tablet_leaderboard];
        if (mopubInterstitial && ![mopubInterstitial.adUnitId isEqualToString:[self adIDWithConfigKey:kConfig_adid_mopub_tablet_full]])
            mopubInterstitial.adUnitId = [self adIDWithConfigKey:kConfig_adid_mopub_tablet_full];
    }
    if (mopubVideoInterstitial && ![mopubVideoInterstitial.adUnitId isEqualToString:[self adIDWithConfigKey:kConfig_adid_mopub_video]])
        mopubVideoInterstitial.adUnitId = [self adIDWithConfigKey:kConfig_adid_mopub_video];
    
#ifndef __PPCLINKAds_Lite__
    if (([interstitialAdNetworkGroup containsObject:kAdconolyNetworkName] ||
         [videoInterstitialAdNetworkGroup containsObject:kAdconolyNetworkName]))
    {
        [self initAdColony];
    }
    
//    if (([interstitialAdNetworkGroup containsObject:kVungleNetworkName] ||
//         [videoInterstitialAdNetworkGroup containsObject:kVungleNetworkName]))
//    {
//        [self initVungle];
//    }
#endif
#endif
    
}

/**
 *  Set AppType, so that if user has purchased in-app before, ad won't be displayed.
 */
- (void) setProductType:(PRODUCT_TYPE)productType
{
    _productType = productType;
    [g_vdConfigNotification setProductType:_productType];
    [self initAdNetworksComponent];
}

#pragma mark NATIVE AD handle

- (void)requestNativeAd
{
    if (_productType == PT_PAID)
    {
        curNativeAd = nil; sCurNativeAdNetworkName = nil;
        return;
    }
    
    if (bIsRequestingNativeAd)
        return;
    
    // Neu da cho cache native chua show thi ko can request new nua. Ad metadata can be cached and re-used for up to 1 hour.
    //    if (curNativeAd && sCurNativeAdNetworkName)
    //        return;
    
    bIsRequestingNativeAd = YES;
    nCurIndexNativeAdNetwork = 0;
    
    [self requestNativeAdRecursive];
}

- (void)requestNativeAdRecursive
{
    if (nCurIndexNativeAdNetwork >= [nativeAdNetworkGroup count])
    {
        // Incase no native ad, use house ad (notification) instead
        NSDictionary* dictNotifyInfo = [g_vdConfigNotification getAPPCLINKNativeAd];
        if (dictNotifyInfo)
        {
            curNativeAd = dictNotifyInfo;
            sCurNativeAdNetworkName = kPPCLINKNetworkName;
            
            if ([self.nativeAdDelegate respondsToSelector:@selector(nativeAdDidLoad:fromAdNetwork:)])
                [self.nativeAdDelegate nativeAdDidLoad:curNativeAd fromAdNetwork:sCurNativeAdNetworkName];
        }
        
        bIsRequestingNativeAd = NO;
        //NSLog(@"NATIVE AD: No ad configured to show!!!");
        return;
    }
    
    NSString* sNativeAdNetworkName = [nativeAdNetworkGroup objectAtIndex:nCurIndexNativeAdNetwork];
    if ([sNativeAdNetworkName isEqualToString:kAdmobNetworkName])
    {
        NSMutableArray *adTypes = [[NSMutableArray alloc] init];
        [adTypes addObject:kGADAdLoaderAdTypeNativeAppInstall];
        [adTypes addObject:kGADAdLoaderAdTypeNativeContent];
        GADVideoOptions *videoOptions = [[GADVideoOptions alloc] init];
        videoOptions.startMuted = NO;
        
        GADNativeAdViewAdOptions *adViewOption = [[GADNativeAdViewAdOptions alloc] init];
        adViewOption.preferredAdChoicesPosition = GADAdChoicesPositionBottomLeftCorner;
        
        self.adLoader = [[GADAdLoader alloc] initWithAdUnitID:[self adIDWithConfigKey:kConfig_adid_admob_native]
                                           rootViewController:self
                                                      adTypes:adTypes
                                                      options:@[ videoOptions,adViewOption ]];
        self.adLoader.delegate = self;
        [self.adLoader loadRequest:[GADRequest request]];
        
    }
    else if ([sNativeAdNetworkName isEqualToString:kFacebookNetworkName])
    {
        FBNativeAd *nativeAd = [[FBNativeAd alloc] initWithPlacementID:[self adIDWithConfigKey:kConfig_adid_facebook_native]];
        
        // Set a delegate to get notified when the ad was loaded.
        nativeAd.delegate = self;
        nativeAd.mediaCachePolicy = FBNativeAdsCachePolicyAll;
        
        // When testing on a device, add its hashed ID to force test ads.
        // The hash ID is printed to console when running on a device.
        //[FBAdSettings addTestDevice:@"38c66f50c9ffc22bf3ec83e30c4d87b0fbb3db91"];
        
        // Initiate a request to load an ad.
        [nativeAd loadAd];
    }
    else if ([sNativeAdNetworkName isEqualToString:kMopubNetworkName])
    {
        // Create and configure a renderer configuration for native ads.
        MPStaticNativeAdRendererSettings *settings = [[MPStaticNativeAdRendererSettings alloc] init];
        settings.renderingViewClass = [self class];
        
        MPNativeAdRendererConfiguration *config = [MPStaticNativeAdRenderer rendererConfigurationWithRendererSettings:settings];
        MPNativeAdRequest *currentAdRequest = [MPNativeAdRequest requestWithAdUnitIdentifier:[self adIDWithConfigKey:kConfig_adid_mopub_native] rendererConfigurations:@[config]];
        
        
        //MPNativeAdRequestTargeting *targeting = [[MPNativeAdRequestTargeting alloc] init];
        //        targeting.keywords = self.keywordsTextField.text;
        //        adRequest1.targeting = targeting;
        //        self.info.keywords = adRequest1.targeting.keywords;
        //        // persist last used keywords if this is a saved ad
        //        if ([[MPAdPersistenceManager sharedManager] savedAdForID:self.info.ID] != nil) {
        //            [[MPAdPersistenceManager sharedManager] addSavedAd:self.info];
        //        }
        
        [currentAdRequest startWithCompletionHandler:^(MPNativeAdRequest *request, MPNativeAd *response, NSError *error)
         {
             
             if (error)
             {
                 nCurIndexNativeAdNetwork++;
                 [self requestNativeAdRecursive];
             }
             else
             {
                 curNativeAd = response;
                 sCurNativeAdNetworkName = kMopubNetworkName;
                 
                 bIsRequestingNativeAd = NO;
                 
                 if ([self.nativeAdDelegate respondsToSelector:@selector(nativeAdDidLoad:fromAdNetwork:)])
                     [self.nativeAdDelegate nativeAdDidLoad:curNativeAd fromAdNetwork:sCurNativeAdNetworkName];
                 
                 // Dao mang quang cao cho lan sau
                 if ([[g_vdConfigNotification._config objectForKey:kConfig_RecirculateAdNetworkEnable] intValue] > 0)
                 {
                     [nativeAdNetworkGroup removeObjectAtIndex:nCurIndexNativeAdNetwork];
                     [nativeAdNetworkGroup addObject:sCurNativeAdNetworkName];
                 }
             }
         }];
        
        
        //MPNativeAdRequest *currentAdRequest = [MPNativeAdRequest requestWithAdUnitIdentifier:[self adIDWithConfigKey:kConfig_adid_mopub_native]];
        
    }
    else
    {
        nCurIndexNativeAdNetwork++;
        [self requestNativeAdRecursive];
    }
}

- (NSString *)getCurrentNativeAdNetworkName
{
    return sCurNativeAdNetworkName;
}

- (void)initNativeAd
{
    if (bIsRequestingNativeAd)
        return;
    
    bUseNativeAd = YES;
    
    curNativeAd = nil;
    sCurNativeAdNetworkName = nil;
    bIsRequestingNativeAd = NO;
    nCurIndexNativeAdNetwork = 0;
    
    [self requestNativeAd];
}

- (NSDictionary *)getNativeAdInfoToShow
{
    if (_productType == PT_PAID || [g_vdConfigNotification isInAdFreeTime])
        return nil;
    
    NSDictionary* dictNativeAdInfo = nil;
    
    int nPercentPPCLINKNativeAds = [[g_vdConfigNotification._config objectForKey:@"percent_ppclink_nativeads"] intValue];
    
    if (nPercentPPCLINKNativeAds > 0 && nPercentPPCLINKNativeAds <= 100)
    {
        int nRandomIn100 = [VDConfigNotification getRandomIntValue:100];
        if (nRandomIn100 < nPercentPPCLINKNativeAds)
        {
            NSDictionary *dictNotifyInfo = [g_vdConfigNotification getAPPCLINKNativeAd];
            if (dictNotifyInfo)
            {
                dictNativeAdInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                    kPPCLINKNetworkName, kNativeAdInfo_NetworkName,
                                    dictNotifyInfo, kNativeAdInfo_Object,
                                    nil];
                
                return dictNativeAdInfo;
            }
            
        }
    }
    
    
    if (curNativeAd && sCurNativeAdNetworkName)
    {
        dictNativeAdInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                            sCurNativeAdNetworkName, kNativeAdInfo_NetworkName,
                            curNativeAd, kNativeAdInfo_Object,
                            nil];
    }
    
    // Cache for next show!
    //sCurNativeAdNetworkName = nil;
    //curNativeAd = nil;
    [self requestNativeAd];
    
    return dictNativeAdInfo;
}

#pragma mark Admob Native Advanced Ad implementation

- (void)adLoader:(GADAdLoader *)adLoader didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"%@ failed with error: %@", adLoader, error);
    nCurIndexNativeAdNetwork++;
    [self requestNativeAdRecursive];
}

- (void)adLoader:(GADAdLoader *)adLoader
didReceiveNativeAppInstallAd:(GADNativeAppInstallAd *)nativeAppInstallAd {
    NSLog(@"Received native app install ad: %@", nativeAppInstallAd);
    
    curNativeAd = nativeAppInstallAd;
    sCurNativeAdNetworkName = kAdmobNetworkName;
    
    bIsRequestingNativeAd = NO;
    
    if ([self.nativeAdDelegate respondsToSelector:@selector(nativeAdDidLoad:fromAdNetwork:)])
        [self.nativeAdDelegate nativeAdDidLoad:curNativeAd fromAdNetwork:sCurNativeAdNetworkName];
    
    // Dao mang quang cao cho lan sau
    if ([[g_vdConfigNotification._config objectForKey:kConfig_RecirculateAdNetworkEnable] intValue] > 0)
    {
        [nativeAdNetworkGroup removeObjectAtIndex:nCurIndexNativeAdNetwork];
        [nativeAdNetworkGroup addObject:sCurNativeAdNetworkName];
    }
}

- (void)adLoader:(GADAdLoader *)adLoader
didReceiveNativeContentAd:(GADNativeContentAd *)nativeContentAd {
    NSLog(@"Received native content ad: %@", nativeContentAd);
    curNativeAd = nativeContentAd;
    sCurNativeAdNetworkName = kAdmobNetworkName;
    
    bIsRequestingNativeAd = NO;
    
    if ([self.nativeAdDelegate respondsToSelector:@selector(nativeAdDidLoad:fromAdNetwork:)])
        [self.nativeAdDelegate nativeAdDidLoad:curNativeAd fromAdNetwork:sCurNativeAdNetworkName];
    
    // Dao mang quang cao cho lan sau
    if ([[g_vdConfigNotification._config objectForKey:kConfig_RecirculateAdNetworkEnable] intValue] > 0)
    {
        [nativeAdNetworkGroup removeObjectAtIndex:nCurIndexNativeAdNetwork];
        [nativeAdNetworkGroup addObject:sCurNativeAdNetworkName];
    }
}

#pragma mark Facebook Native Ad delegate
- (void)nativeAdDidLoad:(FBNativeAd *)nativeAd
{
    curNativeAd = nativeAd;
    sCurNativeAdNetworkName = kFacebookNetworkName;
    
    bIsRequestingNativeAd = NO;
    
    if ([self.nativeAdDelegate respondsToSelector:@selector(nativeAdDidLoad:fromAdNetwork:)])
        [self.nativeAdDelegate nativeAdDidLoad:curNativeAd fromAdNetwork:sCurNativeAdNetworkName];
    
    // Dao mang quang cao cho lan sau
    if ([[g_vdConfigNotification._config objectForKey:kConfig_RecirculateAdNetworkEnable] intValue] > 0)
    {
        [nativeAdNetworkGroup removeObjectAtIndex:nCurIndexNativeAdNetwork];
        [nativeAdNetworkGroup addObject:sCurNativeAdNetworkName];
    }
}

- (void)nativeAd:(FBNativeAd *)nativeAd didFailWithError:(NSError *)error
{
#if __PPCLINKAds_DEV__
    UIAlertView* al = [[UIAlertView alloc] initWithTitle:error.description message:error.debugDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [al show];
#endif
    nCurIndexNativeAdNetwork++;
    [self requestNativeAdRecursive];
}


#ifndef __PPCLINKAds_NativeOnly__

#pragma mark - Update Banner Ad & Interstitial
- (CGSize)getAdBannerCGSize
{
    CGSize sizeAdBanner;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        sizeAdBanner = CGSizeMake(320, 50);
    }
    else
    {
        sizeAdBanner = CGSizeMake(728, 90); // Leaderboard size
    }
    
    return sizeAdBanner;
}

- (void)startTimerCheckingInterstitialAvailability
{
    //// Call this function once after updating ad config param
    if (checkIntertitialAvaibilityTimer)
    {
        [checkIntertitialAvaibilityTimer invalidate];
        checkIntertitialAvaibilityTimer = nil;
    }
    
    checkIntertitialAvaibilityTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(onInterstitialAvaibilityChecking) userInfo:nil repeats:YES];
}

- (void) onInterstitialAvaibilityChecking
{
    //// This checks each second if any interstitial available
    
    if ([interstitialAdNetworkGroup count] <= 0 && [videoInterstitialAdNetworkGroup count] <= 0)
    {
        [checkIntertitialAvaibilityTimer invalidate];
        checkIntertitialAvaibilityTimer = nil;
        return;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyInterstitialCheckAvaibilityStatus object:nil];
    
}

#pragma mark - Ad Banner Show/Hide handle
- (void) showBannerAd
{
    if (_productType == PT_PAID)
    {
        /// In-App purchased is done
        NSLog(@"In-App purchased is done");
        return;
    }
    
    NSLog(@"ADVIEWCONTROLLER : showBannerAd");
    
    bBannerAdIsRequestedToShow = YES;
    [self updateAdBannerVisibility];
}

- (void)showBannerAd:(void (^)(MPMediationAdController *mediationAdController, BOOL bShowResult))bannerShowResult
{
    bannerAdShowResult = bannerShowResult;
    
    [self showBannerAd];
}

- (void) hideBannerAd
{
    NSLog(@"ADVIEWCONTROLLER : hideBannerAd");
    bannerAdShowResult = nil;
    bBannerAdIsRequestedToShow = NO;
    [self updateAdBannerVisibility];
}

- (BOOL) bBannerAdIsShowing
{
    return !self.bannerAdView.hidden;
}

- (BOOL) isShowingInterstitialAd
{
    return bShowingInterstitialAd;
}

- (BOOL) isInterstitialAdQualifiedForBonus
{
    if (dictCurBonusInfo && bIsQualifiedBonus)
        return YES;
    else
        return NO;
}

- (NSString*)currentRunningBannerAd
{
    if (currentBannerAdNetworkIndex < [bannerAdNetworkGroup count])
        return [bannerAdNetworkGroup objectAtIndex:currentBannerAdNetworkIndex];
    else
        return @"All network unavailable!";
}

- (NSString*)currentRunningIntertitialAd
{
    return [interstitialAdNetworkGroup objectAtIndex:currentSelectedInterstitialAdNetworkIndex];
}

- (void) startAutoRefreshAdBanner
{
    NSLog(@"MOPUB: startAutoRefreshAdBanner");
    
    // Notify start autorefresh banner ad
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyMPBannerAdViewStartAutoRefresh object:nil];
    
    mopubBannerAd.hidden = YES;
    [mopubBannerAd stopAutomaticallyRefreshingContents];
    admobBannerAd.hidden = YES;

    
    [mopubBannerAd removeFromSuperview];
    [admobBannerAd removeFromSuperview];

    
    NSString *activeAdNetworkName = nil;
    
    while (currentBannerAdNetworkIndex < [bannerAdNetworkGroup count])
    {
        activeAdNetworkName = [bannerAdNetworkGroup objectAtIndex:currentBannerAdNetworkIndex];

        if ([activeAdNetworkName isEqualToString:kMopubNetworkName])
        {
            NSLog(@"MP BANNER: loading MOPUB");
            
            /// Active Mopub Banner
//            mopubBannerAd.hidden = NO;
//            [self.bannerAdView addSubview:mopubBannerAd];
//            [self.bannerAdView bringSubviewToFront:mopubBannerAd];
          
            CGFloat OffsetX = (self.bannerAdView.frame.size.width - [mopubBannerAd adContentViewSize].width) / 2;
            mopubBannerAd.center = CGPointMake(self.bannerAdView.bounds.size.width/2 + OffsetX, self.bannerAdView.bounds.size.height/2);

            [mopubBannerAd startAutomaticallyRefreshingContents];
            [mopubBannerAd loadAd];
            return;
        }
        
        if ([activeAdNetworkName isEqualToString:kAdmobNetworkName])
        {
            NSLog(@"MP BANNER: loading ADMOB");
            
            /// Active Admob Banner
//            admobBannerAd.hidden = NO;
//            [self.bannerAdView addSubview:admobBannerAd];
//            [self.bannerAdView bringSubviewToFront:admobBannerAd];
            
            CGFloat OffsetX = (self.bannerAdView.frame.size.width - admobBannerAd.adSize.size.width) / 2;
            admobBannerAd.center = CGPointMake(self.bannerAdView.bounds.size.width/2 + OffsetX, self.bannerAdView.bounds.size.height/2);
            
            [admobBannerAd loadRequest:[self refineGADRequest]];
            return;
        }
        
        currentBannerAdNetworkIndex++;
    }
    
    if (bannerAdShowResult)
    {
        bannerAdShowResult(self, NO);
        bannerAdShowResult = nil;
    }
    
    [self hideBannerAd];
    NSLog(@"PPCINKADS: Banner Ad: all network unavailable at this time!.");
}

- (void) stopAutoRefreshAdBanner
{
    NSLog(@"MOPUB: stopAutoRefreshAdBanner");
    
    [mopubBannerAd stopAutomaticallyRefreshingContents];
}

- (void) updateAdBannerVisibility
{
// Nếu đang trong thời gian free quảng cáo, thì ko cho hiện banner
    if ([g_vdConfigNotification isInAdFreeTime])
        bBannerAdIsRequestedToShow = NO;
    
    if (bBannerAdIsRequestedToShow)
    {
        self.bannerAdView.hidden = NO;
        currentBannerAdNetworkIndex = 0;
        [self startAutoRefreshAdBanner];
    }
    else
    {
        self.bannerAdView.hidden = YES;
        [self stopAutoRefreshAdBanner];
    }
}

- (NSString*) isInterstitialAdAvailable:(INTERSTITIAL_AD_TYPE)adType
{
    return [self isInterstitialAdAvailable:adType forBonus:NO];
}

- (NSString*) isInterstitialAdAvailableForBonus:(INTERSTITIAL_AD_TYPE)adType
{
    return [self isInterstitialAdAvailable:adType forBonus:YES];
}

- (NSString*) isInterstitialAdAvailable:(INTERSTITIAL_AD_TYPE)adType forBonus:(BOOL)bBonus
{
// Check limit show interstital ad per session
    int nMaxTimeShowInterstitialAdPerSession = [[g_vdConfigNotification._config objectForKey:kConfig_InterstitialAdMaxTimePerSession] intValue];
    
    if (nMaxTimeShowInterstitialAdPerSession > 0 && nCountShowInterstitialAd >= nMaxTimeShowInterstitialAdPerSession)
        return nil;
    
    NSString* sCurAdNetwork = nil;
    int i;
    if (adType == INTERSTITIAL_AD_TYPE_VIDEO_ONLY)
    {
        for (i = 0; i < [videoInterstitialAdNetworkGroup count]; i++)
        {
            sCurAdNetwork = [videoInterstitialAdNetworkGroup objectAtIndex:i];
            if ([sCurAdNetwork isEqualToString:kMopubNetworkName])
            {
                //exclude MoPub/Fb incase Bonus
                if (!bBonus)
                {
                    if (mopubVideoInterstitial.ready) return kMopubNetworkName;
                    // try to request again here...
                    [mopubVideoInterstitial loadAd];
                }
            }
            else if ([sCurAdNetwork isEqualToString:kAdmobNetworkName])
            {
                //exclude AdMob incase Bonus
                if (!bBonus)
                {
                    if (admobVideoInterstitial.isReady && !admobVideoInterstitial.hasBeenUsed) return kAdmobNetworkName;
                    [admobVideoInterstitial loadRequest:[self refineGADRequest]];
                }
            }
#ifndef __PPCLINKAds_Lite__
            else if ([sCurAdNetwork isEqualToString:kAdconolyNetworkName])
            {
               // if (isAdconolyZoneIDReady && [AdColony zoneStatusForZone:[self adIDWithConfigKey:kConfig_adid_adcolony_zoneid]]== ADCOLONY_ZONE_STATUS_ACTIVE)
                if (_adColonyInterstitalAd && !_adColonyInterstitalAd.expired)
                    return kAdconolyNetworkName;
                [self requestAdColonyInterstitial];
                
            }
//            else if ([sCurAdNetwork isEqualToString:kVungleNetworkName])
//            {
//                if ([[VungleSDK sharedSDK] isAdPlayable])
//                    return kVungleNetworkName;
//            }
#endif
        }
    }
    else if (adType == INTERSTITIAL_AD_TYPE_IMAGE_ONLY)
    {
        for (i = 0; i < [interstitialAdNetworkGroup count]; i++)
        {
            sCurAdNetwork = [interstitialAdNetworkGroup objectAtIndex:i];
            if ([sCurAdNetwork isEqualToString:kMopubNetworkName])
            {
                //exclude MoPub/Fb incase Bonus
                if (!bBonus)
                {
                    if (mopubInterstitial.ready) return kMopubNetworkName;
                    [mopubInterstitial loadAd];
                }
            }
            else if ([sCurAdNetwork isEqualToString:kAdmobNetworkName])
            {
                if (!bBonus)
                {
                    if (admobInterstitial.isReady && !admobInterstitial.hasBeenUsed) return kAdmobNetworkName;
                    // if there isn't any network ready, try to request again here...
                    [admobInterstitial loadRequest:[self refineGADRequest]];
                }
            }
        }
    }
    else
    {
        for (i = 0; i < [interstitialAdNetworkGroup count]; i++)
        {
            sCurAdNetwork = [interstitialAdNetworkGroup objectAtIndex:i];
            if ([sCurAdNetwork isEqualToString:kMopubNetworkName])
            {
                //exclude MoPub/Fb incase Bonus
                if (!bBonus)
                {
                    if (mopubInterstitial.ready || mopubVideoInterstitial.ready) return kMopubNetworkName;
                    [mopubVideoInterstitial loadAd];
                    [mopubInterstitial loadAd];
                }
            }
            else if ([sCurAdNetwork isEqualToString:kAdmobNetworkName])
            {
                if (!bBonus)
                {
                    if (admobVideoInterstitial.isReady && !admobVideoInterstitial.hasBeenUsed) return kAdmobNetworkName;
                    // if there isn't any network ready, try to request again here..
                    [admobVideoInterstitial loadRequest:[self refineGADRequest]];
                    
                    if (admobInterstitial.isReady && !admobInterstitial.hasBeenUsed) return kAdmobNetworkName;
                    [admobInterstitial loadRequest:[self refineGADRequest]];
                }
            }
#ifndef __PPCLINKAds_Lite__
            else if ([sCurAdNetwork isEqualToString:kAdconolyNetworkName])
            {
                //if (isAdconolyZoneIDReady && [AdColony zoneStatusForZone:[self adIDWithConfigKey:kConfig_adid_adcolony_zoneid]]== ADCOLONY_ZONE_STATUS_ACTIVE)
                if (_adColonyInterstitalAd && !_adColonyInterstitalAd.expired)
                    return kAdconolyNetworkName;
                
                [self requestAdColonyInterstitial];
            }
//            else if ([sCurAdNetwork isEqualToString:kVungleNetworkName])
//            {
//                if ([[VungleSDK sharedSDK] isAdPlayable])
//                    return kVungleNetworkName;
//            }
#endif
        }
    }
    
    return nil;
}

- (BOOL)isInterstitialAdAllowedtoShow
{
    // Check dieu kien ve khoang cach giua 2 lan bat quang cao tren server
    NSDate* interstitialAdFreeExpiredDate = [[NSUserDefaults standardUserDefaults] objectForKey:keyinterstitialAdFreeExpiredDate];
    
    if ([[VDConfigNotification getCurrentDate] compare:interstitialAdFreeExpiredDate] != NSOrderedDescending)
    {
        int nRemainSeconds = [interstitialAdFreeExpiredDate timeIntervalSinceDate:[VDConfigNotification getCurrentDate]];
        NSLog(@"%@", [NSString stringWithFormat:@"showInterstitialAd:In %ds remaining of interstitial ad free time interval.", nRemainSeconds]);
#if __PPCLINKADS__
        UIAlertView* al =[[UIAlertView alloc] initWithTitle:@"PPCLINK ADS SDK" message:[NSString stringWithFormat:@"showInterstitialAd:In %ds remaining of interstitial ad free time interval.", nRemainSeconds] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [al show];
#endif
        // Chống đổi ngày tăng lên rồi lại giảm đi
        if (nRemainSeconds < [[g_vdConfigNotification._config objectForKey:kConfig_InterstitialAdFreeMinTimeIntervalBonus] intValue])
            return NO;
    }
    // Check dieu khien app dang duoc nang cap len ban Pro hay chua
    if (_productType == PT_PAID)
    {
        /// In-App purchased is done
        NSLog(@"showInterstitialAd:NOT SHOW because this is Pro Version");
#if __PPCLINKADS__
        UIAlertView* al =[[UIAlertView alloc] initWithTitle:@"PPCLINK ADS SDK" message:@"showInterstitialAd:NOT SHOW because this is Pro Version." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [al show];
#endif
        return NO;
    }
    
    // Có đang trong thời gian thưởng Ad-Free hay không?
    if ([g_vdConfigNotification isInAdFreeTime])
    {
        NSLog(@"showInterstitialAd: NOT SHOW because In Ad-Free time!");
#if __PPCLINKADS__
        UIAlertView* al =[[UIAlertView alloc] initWithTitle:@"PPCLINK ADS SDK" message:@"showInterstitialAd: NOT SHOW because In Ad-Free time!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [al show];
#endif
        return NO;
    }
    
    return YES;
}

- (void)showInterstitialAd:(INTERSTITIAL_AD_TYPE)adType forceShow:(BOOL)bForceShow success:(void (^)(MPMediationAdController *mediationAdController))success failure:(void (^)(NSString *error))failure;
{
    interstitialAdShowSuccess = success;
    interstitialAdShowFailure = failure;
    
    [self showInterstitialAd:adType forceShow:bForceShow];
}

- (void)showInterstitialAd:(INTERSTITIAL_AD_TYPE)adType forceShow:(BOOL)bForceShow
{
    dictCurBonusInfo = nil;
    bIsQualifiedBonus = NO;
    
    if (!bForceShow && ![self isInterstitialAdAllowedtoShow])
    {
        if (interstitialAdShowFailure)
        {
            interstitialAdShowFailure(@"Interstitial ad not allow to show");
            interstitialAdShowFailure = nil;
        }
    }
    else
        [self startShowInterstitialAd:adType];
}

- (void)showInterstitialAd:(INTERSTITIAL_AD_TYPE)adType withBonusInfo:(NSDictionary*)dictBonusInfo
{
    if (dictBonusInfo)
    {
        dictCurBonusInfo = [dictBonusInfo mutableCopy];
        bIsQualifiedBonus = NO;
    }
    
    [self startShowInterstitialAd:adType];
}

- (void)showInterstitialAd
{
    [self showInterstitialAd:INTERSTITIAL_AD_TYPE_ALL forceShow:NO];
}

- (void)showInterstitialAd:(INTERSTITIAL_AD_TYPE)adType withConstraintCondition:(BOOL)bYesNo
{
    [self showInterstitialAd:adType forceShow:!bYesNo];
}


- (void) startShowInterstitialAd:(INTERSTITIAL_AD_TYPE)adType
{
    NSLog(@"%s",__PRETTY_FUNCTION__);
    
    NSString *activeAdNetworkName = nil;
    
    int nIndexNetwork = 0;
    BOOL bShowSuccess = NO;
    
    while ((adType == INTERSTITIAL_AD_TYPE_VIDEO_ONLY && nIndexNetwork < [videoInterstitialAdNetworkGroup count]) || (adType != INTERSTITIAL_AD_TYPE_VIDEO_ONLY && nIndexNetwork < [interstitialAdNetworkGroup count]))
    {
        if (adType == INTERSTITIAL_AD_TYPE_VIDEO_ONLY)
            activeAdNetworkName = [videoInterstitialAdNetworkGroup objectAtIndex:nIndexNetwork];
        else
            activeAdNetworkName = [interstitialAdNetworkGroup objectAtIndex:nIndexNetwork];
        
#ifndef __PPCLINKAds_Lite__
        //// Adconoly ////
        if ([activeAdNetworkName isEqualToString:kAdconolyNetworkName])
        {
            if (adType == INTERSTITIAL_AD_TYPE_IMAGE_ONLY || !_adColonyInterstitalAd || _adColonyInterstitalAd.expired)
            {
                nIndexNetwork++;
                continue;
            }
            
            NSLog(@"INTERSTITIAL: Play ADCONOLY VIDEO for zone: %@",[self adIDWithConfigKey:kConfig_adid_adcolony_zoneid]);
            if (!_adColonyInterstitalAd.expired)
                [_adColonyInterstitalAd showWithPresentingViewController:self.rootViewControllerFullscreenAd];

            //[AdColony playVideoAdForZone:[self adIDWithConfigKey:kConfig_adid_adcolony_zoneid] withDelegate:self];
           // isAdconolyZoneIDReady = NO;
            
            bShowSuccess = YES;

            break;
        }
        //// Vungle ////
//        if ([activeAdNetworkName isEqualToString:kVungleNetworkName])
//        {
//            if (adType == INTERSTITIAL_AD_TYPE_IMAGE_ONLY || ![[VungleSDK sharedSDK] isAdPlayable])
//            {
//                nIndexNetwork++;
//                continue;
//            }
//            //        // Dict to set custom ad options
//            //        NSDictionary* options = @{@"orientations": @(UIInterfaceOrientationMaskAll),
//            //                                  @"incentivized": @(YES),
//            //                                  @"userInfo": @{@"user": @""},
//            //                                  @"showClose": @(NO)};
//            //
//            //        // Pass in dict of options, play ad
//            //        [sdk playAd:self.rootViewControllerFullscreenAd withOptions:options];
//            NSError* err;
//            [[VungleSDK sharedSDK] playAd:self.rootViewControllerFullscreenAd error:&err];
//            bShowSuccess = YES;
//            break;
//        }
#endif
        //// Admob ////
        if ([activeAdNetworkName isEqualToString:kAdmobNetworkName])
        {
        // Nếu show click để nhận thưởng, loại mạng AdMob ra
            if (dictCurBonusInfo)
            {
                nIndexNetwork++;
                continue;
            }
            
            if (adType == INTERSTITIAL_AD_TYPE_IMAGE_ONLY)
            {
                if (!admobInterstitial.isReady || admobInterstitial.hasBeenUsed)
                {
                    nIndexNetwork++;
                    continue;
                }
                
                [admobInterstitial presentFromRootViewController:self.rootViewControllerFullscreenAd];
                bShowSuccess = YES;
            
            }
            else if (adType == INTERSTITIAL_AD_TYPE_VIDEO_ONLY)
            {
                if (!admobVideoInterstitial.isReady || admobVideoInterstitial.hasBeenUsed)
                {
                    nIndexNetwork++;
                    continue;
                }
                
                [admobVideoInterstitial presentFromRootViewController:self.rootViewControllerFullscreenAd];
                bShowSuccess = YES;
            }
            else
            {
                
                if ((!admobInterstitial.isReady || admobInterstitial.hasBeenUsed) && (!admobVideoInterstitial.isReady || admobVideoInterstitial.hasBeenUsed))
                {
                    nIndexNetwork++;
                    continue;
                }
                
            // Tuy theo trong so uu tien cua quang cao video se uu tien hien video hay image truoc
                int nPercentVideoAd = [[g_vdConfigNotification._config objectForKey:kConfig_PercentVideoInterstitalAd] intValue];
                int nRandomIn100 = [VDConfigNotification getRandomIntValue:100];
                if (nRandomIn100 < nPercentVideoAd)
                {
                    if (admobVideoInterstitial.isReady)
                        [admobVideoInterstitial presentFromRootViewController:self.rootViewControllerFullscreenAd];
                    else
                        [admobInterstitial presentFromRootViewController:self.rootViewControllerFullscreenAd];
                }
                else
                {
                    if (admobInterstitial.isReady)
                        [admobInterstitial presentFromRootViewController:self.rootViewControllerFullscreenAd];
                    else
                        [admobVideoInterstitial presentFromRootViewController:self.rootViewControllerFullscreenAd];
                }
                
                bShowSuccess = YES;
            }
            
            break;
        }
        
        //// Mopub ////
        if ([activeAdNetworkName isEqualToString:kMopubNetworkName])
        {
            // Nếu show click để nhận thưởng, loại mạng MoPub/Facebook ra
            if (dictCurBonusInfo)
            {
                nIndexNetwork++;
                continue;
            }
            
            if (adType == INTERSTITIAL_AD_TYPE_VIDEO_ONLY)
            {
                if (!mopubVideoInterstitial.ready)
                {
                    nIndexNetwork++;
                    continue;
                }
                
                [mopubVideoInterstitial showFromViewController:self.rootViewControllerFullscreenAd];
            }
            else if (adType == INTERSTITIAL_AD_TYPE_IMAGE_ONLY)
            {
                if (!mopubInterstitial.ready)
                {
                    nIndexNetwork++;
                    continue;
                }
//                NSLog(@"%d", mopubInterstitial.ready?1:0);
//                NSLog(@"%d", mopubInterstitial.isBeingPresented?1:0);
                [mopubInterstitial showFromViewController:self.rootViewControllerFullscreenAd];
//                NSLog(@"%d", mopubInterstitial.ready?1:0);
//                NSLog(@"%d", mopubInterstitial.isBeingPresented?1:0);
            }
            else
            {
                if (!mopubVideoInterstitial.ready && !mopubInterstitial.ready)
                {
                    nIndexNetwork++;
                    continue;
                }
                
                 // Tuy theo trong so uu tien cua quang cao video se uu tien hien video hay image truoc
                int nPercentVideoAd = [[g_vdConfigNotification._config objectForKey:kConfig_PercentVideoInterstitalAd] intValue];
                int nRandomIn100 = [VDConfigNotification getRandomIntValue:100];
                if (nRandomIn100 < nPercentVideoAd)
                {
                    if (mopubVideoInterstitial.ready)
                        [mopubVideoInterstitial showFromViewController:self.rootViewControllerFullscreenAd];
                    else
                        [mopubInterstitial showFromViewController:self.rootViewControllerFullscreenAd];
                }
                else
                {
                    if (mopubInterstitial.ready)
                        [mopubInterstitial showFromViewController:self.rootViewControllerFullscreenAd];
                    else
                        [mopubVideoInterstitial showFromViewController:self.rootViewControllerFullscreenAd];
                }
            
            }
            
            bShowSuccess = YES;
            
            break;
        }
        
      

        nIndexNetwork++;
    }
    
    if (bShowSuccess)
    {
        nCountShowInterstitialAd++;
        
        if (interstitialAdShowSuccess)
        {
            interstitialAdShowSuccess(self);
            interstitialAdShowSuccess = nil;
        }
        
        currentSelectedInterstitialAdNetworkIndex = nIndexNetwork;
        
        [self updateInterstitialAdFreeExpiredDate:[[g_vdConfigNotification._config objectForKey:kConfig_InterstitialAdFreeMinTimeInterval] intValue]];
        
        /// Configure to recirculate adnetwork for futher loading
        [self configureToRecirculateAdNetworks];
    }
    else
    {
        if (interstitialAdShowFailure)
        {
            interstitialAdShowFailure(@"No Interstitial ad to show");
            interstitialAdShowFailure = nil;
        }
    }
}

- (void)updateInterstitialAdFreeExpiredDate:(NSTimeInterval)nTimeInterval
{
    [[NSUserDefaults standardUserDefaults] setObject:[[VDConfigNotification getCurrentDate] dateByAddingTimeInterval:nTimeInterval] forKey:keyinterstitialAdFreeExpiredDate];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - MOPUB BannerAdView Delegate
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    [mopubBannerAd rotateToOrientation:toInterfaceOrientation];
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (UIViewController *)viewControllerForPresentingModalView
{
    NSLog(@"--------- %s", __PRETTY_FUNCTION__);
    
    //return self;
    UIViewController *modalView = self.rootViewControllerBannerAd.presentedViewController;
    if (modalView){
        if(modalView.parentViewController){
            return modalView.parentViewController;
        }else{
            return modalView;
        }
    }
    
    return [[[[UIApplication sharedApplication]delegate] window] rootViewController];
}

- (void)adViewDidLoadAd:(MPAdView *)view
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    if (bannerAdShowResult)
    {
        bannerAdShowResult(self, YES);
        bannerAdShowResult = nil;
    }
    if (isBannerMopub) {
        mopubBannerAd.hidden = NO;
        [self.bannerAdView addSubview:mopubBannerAd];
        [self.bannerAdView bringSubviewToFront:mopubBannerAd];
    }else{
        admobBannerAd.hidden = NO;
        [self.bannerAdView addSubview:admobBannerAd];
        [self.bannerAdView bringSubviewToFront:admobBannerAd];
    }
    //// Notify did load banner Ad successfully
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyMPBannerAdViewDidLoadAdSuccessful object:nil];
    
    CGSize size = [view adContentViewSize];
    CGFloat centeredX = (self.bannerAdView.frame.size.width - size.width) / 2;
    mopubBannerAd.center = CGPointMake(self.bannerAdView.frame.size.width/2 + centeredX, self.bannerAdView.frame.size.height/2);
}

- (void)adViewDidFailToLoadAd:(MPAdView *)view
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"MP BANNER: MOPUB failed to load");
    
    /// Try to get banner Ad of the next network
    currentBannerAdNetworkIndex++;
    [self startAutoRefreshAdBanner];
}

- (void)willPresentModalViewForAd:(MPAdView *)view
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Khác với banner admob, khi user click vào banner mopub sẽ present  modal webview ngay trong app và có thể
    // download luôn, chưa cần leave app
    [self userDidClickOnBannerAd];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyMPBannerAdViewWillPresentModalView object:nil];
}

- (void)didDismissModalViewForAd:(MPAdView *)view
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyMPBannerAdViewWillDismissModalView object:nil];
}

- (void)willLeaveApplicationFromAd:(MPAdView *)view
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyMPBannerAdViewWillLeaveAppFromAd object:nil];
}

#pragma mark - MOPUB Interstitial AdController Delegate

- (void)interstitialDidLoadAd:(MPInterstitialAdController *)interstitial
{
    NSLog(@"====== %s", __PRETTY_FUNCTION__);
}

- (void)interstitialDidFailToLoadAd:(MPInterstitialAdController *)interstitial
{
    NSLog(@"====== %s", __PRETTY_FUNCTION__);
    
    // Retry to load Ad
    //[interstitial loadAd];
}

- (void)interstitialDidExpire:(MPInterstitialAdController *)interstitial
{
    NSLog(@"====== %s", __PRETTY_FUNCTION__);
    
    if ([interstitial isBeingPresented])
        [interstitial dismissViewControllerAnimated:YES completion:nil];
}


- (void)interstitialWillAppear:(MPInterstitialAdController *)interstitial
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    [self interstitialAdAppear:interstitial];
    
}

- (void)interstitialDidAppear:(MPInterstitialAdController *)interstitial
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    // Phòng trường hợp lỗi MoPub không gọi interstitialWillAppear
    if (!bShowingInterstitialAd)
        [self interstitialAdAppear:interstitial];
}

- (void)interstitialWillDisappear:(MPInterstitialAdController *)interstitial
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    // Phai comment lai vi neu goi luon tu day, khi goi [MPInterstitialAdController loadAd] sẽ ko load cache ad dc!
    //[self interstitialAdDisappear:interstitial];
}

- (void)interstitialDidDisappear:(MPInterstitialAdController *)interstitial
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Phòng trường hợp lỗi MoPub không gọi interstitialWillDisappear
    if (bShowingInterstitialAd)
        [self interstitialAdDisappear:interstitial];
}

//- (void) interstitialDidTap:(MPInterstitialAdController *)interstitial
- (void)interstitialDidReceiveTapEvent:(MPInterstitialAdController *)interstitial
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    //if (interstitial != mopubVideoInterstitial)
    [self userDidClickOnInterstitialAd];
}

#pragma mark - ADMOB BannerAdView Delegate

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView
{
    //NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"ADMOB Banner adapter class name: %@", bannerView.adNetworkClassName);
    
    if (bannerAdShowResult)
    {
        bannerAdShowResult(self, YES);
        bannerAdShowResult = nil;
    }
    if (isBannerMopub) {
        mopubBannerAd.hidden = NO;
        [self.bannerAdView addSubview:mopubBannerAd];
        [self.bannerAdView bringSubviewToFront:mopubBannerAd];
    }else{
        admobBannerAd.hidden = NO;
        [self.bannerAdView addSubview:admobBannerAd];
        [self.bannerAdView bringSubviewToFront:admobBannerAd];
    }
    //// Notify did load banner Ad successfully
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyMPBannerAdViewDidLoadAdSuccessful object:nil];
}

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"ADMOB failed to load with error: %@", error.localizedDescription);
    NSLog(@"MP BANNER: ADMOB failed to load");
    
    /// Try to get banner Ad of the next network
    currentBannerAdNetworkIndex++;
    [self startAutoRefreshAdBanner];
}

- (void)adViewWillPresentScreen:(GADBannerView *)bannerView
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    //// ???: Check then
    //[self userDidClickOnBannerAd];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyMPBannerAdViewWillPresentModalView object:nil];
}

- (void)adViewDidDismissScreen:(GADBannerView *)bannerView
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)adViewWillDismissScreen:(GADBannerView *)bannerView
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyMPBannerAdViewWillDismissModalView object:nil];
}

- (void)adViewWillLeaveApplication:(GADBannerView *)bannerView
{
    // Khác với Mopub, khi user click bào Admob banner, app thường sẽ leave app và nhảy vào đây luôn thay vì willPresentScreen
    [self userDidClickOnBannerAd];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyMPBannerAdViewWillLeaveAppFromAd object:nil];
}

#pragma mark - ADMOB Interstitial AdController Delegate


- (void)interstitialDidReceiveAd:(GADInterstitial *)interstitial
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"ADMOB Interstitial adapter class name: %@", interstitial.adNetworkClassName);
}


- (void)interstitial:(GADInterstitial *)interstitial didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)interstitialWillPresentScreen:(GADInterstitial *)interstitial
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [self interstitialAdAppear:interstitial];
}

- (void)interstitialWillDismissScreen:(GADInterstitial *)interstitial
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [self interstitialAdDisappear:interstitial];
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    // Phòng trường hợp lỗi AdMob không gọi interstitialWillDismissScreen
    if (bShowingInterstitialAd)
        [self interstitialAdDisappear:interstitial];
}

- (void)interstitialWillLeaveApplication:(GADInterstitial *)interstitial
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    //// ???: Check then
    [self userDidClickOnInterstitialAd];
}

#ifndef __PPCLINKAds_Lite__
#pragma mark - AdColony handle
- (void)initAdColony
{
    if ([self iPhone4AndLower])
        return;
    
    if (_productType == PT_PAID)
        return;
    
    NSLog(@"PPCLINKADS: Init AdNetwork ADCOLONY");
    //[AdColony configureWithAppID:[self adIDWithConfigKey:kConfig_adid_adcolony_appid] zoneIDs:@[[self adIDWithConfigKey:kConfig_adid_adcolony_zoneid]] delegate:self logging:YES];
    
    [AdColony configureWithAppID:[self adIDWithConfigKey:kConfig_adid_adcolony_appid]
                         zoneIDs:@[[self adIDWithConfigKey:kConfig_adid_adcolony_zoneid]]
     //[self adIDWithConfigKey:kConfig_adid_adcolony_reward_zoneid]]
                         options:nil
                      completion:^(NSArray<AdColonyZone*>* zones)
     {
         //Set the zone's reward handler
         //This implementation is designed for client-side virtual currency without a server
         //It uses NSUserDefaults for persistent client-side storage of the currency balance
         //For applications with a server, contact the server to retrieve an updated currency balance
         
         AdColonyZone *zoneReward = nil;
         for (int i = 0; i < [zones count]; i++)
             if (((AdColonyZone*)([zones objectAtIndex:i])).rewarded)
             {
                 zoneReward = [zones objectAtIndex:i];
                 break;
             }
         
         if (zoneReward)
             zoneReward.reward = ^(BOOL success, NSString *name, int amount) {
                 
                 //Store the new balance and update the UI to reflect the change
                 if (success) {
                     //NSLog(@"You got reward %d %@", amount, name);
                     if (rewardVideoSuccess)
                     {
                         int nRewardAmount = [[g_vdConfigNotification._config objectForKey:@"VideoAdRewardAmount"] intValue];
                         if (nRewardAmount < 2)
                         {
                             if (amount < 2)
                                 nRewardAmount = 14;
                             else
                                 nRewardAmount = amount;
                         }
                         rewardVideoSuccess([NSDictionary dictionaryWithObjectsAndKeys:kAdconolyNetworkName, @"adnetworkname",
                                             [NSNumber numberWithInt:nRewardAmount], @"reward", nil]);
                         rewardVideoSuccess = nil;
                     }
                 }
             };
         
         //AdColony has finished configuring, so let's request an interstitial ad
         [self requestAdColonyInterstitial];
         //[self requestAdColonyRewardVideo];
     }
     ];
    
    _bDidInitAdcolony = YES;
}

- (void)requestAdColonyInterstitial
{
    if (_productType == PT_PAID)
        return;
    
    if ([self iPhone4AndLower])
        return;
    
    //Request an interstitial ad from AdColony
    [AdColony requestInterstitialInZone:[self adIDWithConfigKey:kConfig_adid_adcolony_zoneid] options:nil
     
     //Handler for successful ad requests
                                success:^(AdColonyInterstitial *ad) {
                                    
                                    ad.open = ^{
                                        bShowingInterstitialAd = YES;
                                        //// Do pause game/sound if need
                                        [[NSNotificationCenter defaultCenter] postNotificationName:kStartVideoAdWatching object:nil];
                                    };
                                    
                                    ad.click = ^{
                                        [self userDidClickOnInterstitialAd];
                                    };
                                    
                                    //Once the ad has finished, set the loading state and request a new interstitial
                                    ad.close = ^{
                                        [self userDidWatchAnAdVideo:YES];
                                        if (self.interstitialAdClose)
                                        {
                                            self.interstitialAdClose();
                                            self.interstitialAdClose = nil;
                                        }
                                        
                                        if (rewardVideoSuccess) // Trường hợp xem quang cao de nhan thưởng, adcolony ko co close giưa chừng, đã bung là xem hết
                                        {
                                            int nRewardAmount = [[g_vdConfigNotification._config objectForKey:@"VideoAdRewardAmount"] intValue];
                                            if (nRewardAmount < 2)
                                                nRewardAmount = 14;
                                            
                                            rewardVideoSuccess([NSDictionary dictionaryWithObjectsAndKeys:kAdconolyNetworkName, @"adnetworkname",
                                                                [NSNumber numberWithInt:nRewardAmount], @"reward", nil]);
                                            rewardVideoSuccess = nil;
                                        }
                                        [self requestAdColonyInterstitial];
                                    };
                                    
                                    //Interstitials can expire, so we need to handle that event also
                                    ad.expire = ^{
                                        _adColonyInterstitalAd = nil;
                                        [self requestAdColonyInterstitial];
                                    };
                                    
                                    //Store a reference to the returned interstitial object
                                    _adColonyInterstitalAd = ad;
                                    
                                    
                                }
     
     //Handler for failed ad requests
                                failure:^(AdColonyAdRequestError *error) {
                                    NSLog(@"SAMPLE_APP: Request failed with error: %@ and suggestion: %@", [error localizedDescription], [error localizedRecoverySuggestion]);
                                }
     ];
}

#pragma mark - Reward video handle
- (NSString*)isRewardedVideoAvailable
{
    NSString* sCurAdNetwork = nil;
    
    for (int i = 0; i < [videoInterstitialAdNetworkGroup count]; i++)
    {
        sCurAdNetwork = [videoInterstitialAdNetworkGroup objectAtIndex:i];
        if ([sCurAdNetwork isEqualToString:kAdmobNetworkName])
        {
            if ([[GADRewardBasedVideoAd sharedInstance] isReady])
                return kAdmobNetworkName;
            
            [self requestAdmobRewardedVideo];
        }
        else if ([sCurAdNetwork isEqualToString:kAdconolyNetworkName])
        {
            //if (_adColonyRewardVideo && !_adColonyRewardVideo.expired)
            if (_adColonyInterstitalAd && !_adColonyInterstitalAd.expired)
                return kAdconolyNetworkName;
            [self requestAdColonyInterstitial];
        }
//        else if ([sCurAdNetwork isEqualToString:kVungleNetworkName])
//        {
//            if ([[VungleSDK sharedSDK] isAdPlayable])
//                return kVungleNetworkName;
//        }
    }
    return nil;
}

- (void)showRewardedVideoWithSuccessReward:(void (^)(NSDictionary *))rewardSuccess
{
    rewardVideoSuccess = rewardSuccess;
    NSString* sCurAdNetwork = nil;
    BOOL bShow = NO;
    for (int i = 0; i < [videoInterstitialAdNetworkGroup count]; i++)
    {
        sCurAdNetwork = [videoInterstitialAdNetworkGroup objectAtIndex:i];
        if ([sCurAdNetwork isEqualToString:kAdmobNetworkName] && [[GADRewardBasedVideoAd sharedInstance] isReady])
        {
            [[GADRewardBasedVideoAd sharedInstance] presentFromRootViewController:self.rootViewControllerFullscreenAd];
            bShow = YES;
            break;
        }
        else if ([videoInterstitialAdNetworkGroup containsObject:kAdconolyNetworkName] && _adColonyInterstitalAd && !_adColonyInterstitalAd.expired)
        {
            [_adColonyInterstitalAd showWithPresentingViewController:self.rootViewControllerFullscreenAd];
            bShow = YES;
            break;
        }
//        else if ([videoInterstitialAdNetworkGroup containsObject:kVungleNetworkName] && [[VungleSDK sharedSDK] isAdPlayable])
//        {
//        // Dict to set custom ad options
//            NSDictionary* options = @{VunglePlayAdOptionKeyIncentivized: @YES,
//                                  VunglePlayAdOptionKeyIncentivizedAlertBodyText : @"If the video isn't completed you won't get your reward! Are you sure you want to close early?",
//                                  VunglePlayAdOptionKeyIncentivizedAlertCloseButtonText : @"Close",
//                                  VunglePlayAdOptionKeyIncentivizedAlertContinueButtonText : @"Keep Watching",
//                                  VunglePlayAdOptionKeyIncentivizedAlertTitleText : @"Careful!"};
//
//        // Pass in dict of options, play ad
//            NSError *error;
//            [[VungleSDK sharedSDK] playAd:self.rootViewControllerFullscreenAd withOptions:options error:&error];
//
//            if (error)
//            {
//                NSLog(@"Error encountered playing ad: %@", error);
//            }
//            else
//                bShow = YES;
//
//            break;
//        }
    }
    
    if (!bShow)
    {
        if (rewardVideoSuccess)
        {
            rewardVideoSuccess([NSDictionary dictionaryWithObjectsAndKeys:@"NoAdNetwork", @"adnetworkname",
                                [NSNumber numberWithInt:0], @"reward", nil]);
            rewardVideoSuccess = nil;
        }
    }
}

/*
 - (void)requestAdColonyRewardVideo
 {
 //Request an interstitial ad from AdColony
 [AdColony requestInterstitialInZone:[self adIDWithConfigKey:kConfig_adid_adcolony_reward_zoneid] options:nil
 
 //Handler for successful ad requests
 success:^(AdColonyInterstitial *ad) {
 
 ad.open = ^{
 bShowingInterstitialAd = YES;
 //// Do pause game/sound if need
 [[NSNotificationCenter defaultCenter] postNotificationName:kStartVideoAdWatching object:nil];
 };
 
 ad.click = ^{
 [self userDidClickOnInterstitialAd];
 };
 
 //Once the ad has finished, set the loading state and request a new interstitial
 ad.close = ^{
 [self userDidWatchAnAdVideo:YES];
 if (rewardVideoSuccess) // Trường hợp xem quang video ko hết đã close, reward = 0
 {
 rewardVideoSuccess([NSDictionary dictionaryWithObjectsAndKeys:kAdconolyNetworkName, @"adnetworkname",
 [NSNumber numberWithInt:0], @"reward", nil]);
 rewardVideoSuccess = nil;
 }
 [self requestAdColonyRewardVideo];
 };
 
 //Interstitials can expire, so we need to handle that event also
 ad.expire = ^{
 _adColonyRewardVideo = nil;
 [self requestAdColonyRewardVideo];
 };
 
 //Store a reference to the returned interstitial object
 _adColonyRewardVideo = ad;
 
 
 }
 
 //Handler for failed ad requests
 failure:^(AdColonyAdRequestError *error) {
 NSLog(@"SAMPLE_APP: Request failed with error: %@ and suggestion: %@", [error localizedDescription], [error localizedRecoverySuggestion]);
 }
 ];
 }
 */

/*
 #pragma mark - AdConolyDelegate & AdConolyAdDelegate
 - ( void ) onAdColonyAdAvailabilityChange:(BOOL)available inZone:(NSString*) zoneID
 {
 
 if(available)
 {
 NSLog(@"Adconoly READY for zone : %@",zoneID);
 
 if ([zoneID isEqualToString:[self adIDWithConfigKey:kConfig_adid_adcolony_zoneid]])
 isAdconolyZoneIDReady = YES;
	}
 else
 {
 NSLog(@"Adconoly NOT READY for zone : %@",zoneID);
 
 if ([zoneID isEqualToString:[self adIDWithConfigKey:kConfig_adid_adcolony_zoneid]])
 isAdconolyZoneIDReady = NO;
 }
 
 ADCOLONY_ZONE_STATUS zoneStatus = [AdColony zoneStatusForZone:[self adIDWithConfigKey:kConfig_adid_adcolony_zoneid]];
 switch (zoneStatus) {
 case ADCOLONY_ZONE_STATUS_NO_ZONE:
 NSLog(@"ZoneStatus: ADCOLONY_ZONE_STATUS_NO_ZONE");
 break;
 case ADCOLONY_ZONE_STATUS_OFF:
 NSLog(@"ZoneStatus: ADCOLONY_ZONE_STATUS_OFF");
 break;
 case ADCOLONY_ZONE_STATUS_LOADING:
 NSLog(@"ZoneStatus: ADCOLONY_ZONE_STATUS_LOADING");
 break;
 case ADCOLONY_ZONE_STATUS_ACTIVE:
 NSLog(@"ZoneStatus: ADCOLONY_ZONE_STATUS_ACTIVE");
 break;
 case ADCOLONY_ZONE_STATUS_UNKNOWN:
 NSLog(@"ZoneStatus: ADCOLONY_ZONE_STATUS_UNKNOWN");
 break;
 default:
 break;
 }
 
 }
 
 - ( void ) onAdColonyV4VCReward:(BOOL)success currencyName:(NSString*)currencyName currencyAmount:(int)amount inZone:(NSString*)zoneID
 {
 NSLog(@"MP : onAdColonyV4VCReward : %d, currencyName : %@, currencyAmount: %d, inZone : %@",success,currencyName,amount,zoneID);
 
 [self userDidWatchAnAdVideo:success];
 }
 
 - ( void ) onAdColonyAdStartedInZone:( NSString * )zoneID
 {
 NSLog(@"onAdColonyAdStartedInZone %@",zoneID);
 
 bShowingInterstitialAd = YES;
 //// Do pause game/sound if need
 [[NSNotificationCenter defaultCenter] postNotificationName:kStartVideoAdWatching object:nil];
 }
 
 - ( void ) onAdColonyAdAttemptFinished:(BOOL)shown inZone:( NSString * )zoneID
 {
 NSLog(@"onAdColonyAdStartedInZone %@ - %d",zoneID,shown);
 
 //// Do un-pause game/sound if need
 //// Passed param: YES - if v4vc is watched compleletely, NO - otherwise
 
 [self userDidWatchAnAdVideo:shown];
 }
 
 */
#pragma mark - VungleSDK Handle & Delegate

//- (void)initVungle
//{
//    if ([self iPhone4AndLower])
//        return;
//
//    if (_productType == PT_PAID)
//        return;
//
//    NSLog(@"PPCLINKADS: Init AdNetwork VUNGLE");
//    VungleSDK* vungleSdk = [VungleSDK sharedSDK];
//    [vungleSdk setDelegate:(id<VungleSDKDelegate>)self];
//    NSString* appid = [self adIDWithConfigKey:kConfig_adid_vungle_appid];
//    [vungleSdk startWithAppId:appid];
//}
//
//- (void)vungleSDKAdPlayableChanged:(BOOL)isAdPlayable {
//    if (isAdPlayable)
//    {
//        NSLog(@"An ad is available for playback");
//    }
//    else
//    {
//        NSLog(@"No ads currently available for playback");
//
//    }
//}
//
///**
// * if implemented, this will get called when the SDK is about to show an ad. This point
// * might be a good time to pause your game, and turn off any sound you might be playing.
// */
//- (void)vungleSDKwillShowAd
//{
//    NSLog(@"MP_Vungle: vungleSDKwillShowAd");
//    bShowingInterstitialAd = YES;
//    [[NSNotificationCenter defaultCenter] postNotificationName:kStartVideoAdWatching object:[NSNumber numberWithBool:NO]];
//}
//
///**
// * if implemented, this will get called when the SDK closes the ad view, but there might be
// * a product sheet that will be presented. This point might be a good place to resume your game
// * if there's no product sheet being presented. The viewInfo dictionary will contain the
// * following keys:
// * - "completedView": NSNumber representing a BOOL whether or not the video can be considered a
// *               full view.
// * - "playTime": NSNumber representing the time in seconds that the user watched the video.
// * - "didDownlaod": NSNumber representing a BOOL whether or not the user clicked the download
// *                  button.
// */
//- (void)vungleSDKwillCloseAdWithViewInfo:(NSDictionary*)viewInfo willPresentProductSheet:(BOOL)willPresentProductSheet
//{
//    NSLog(@"MP_Vungle: vungleSDKwillCloseAdWithViewInfo : %@, viewSheet:%d",viewInfo,willPresentProductSheet);
//
//    if (!willPresentProductSheet)
//    {
//        //// Do un-pause game/sound if needed
//        [self userDidWatchAnAdVideo:[[viewInfo objectForKey:@"completedView"] boolValue]];
//
//        if (self.interstitialAdClose)
//        {
//            self.interstitialAdClose();
//            self.interstitialAdClose = nil;
//        }
//
//        if ([[viewInfo objectForKey:@"completedView"] boolValue] && rewardVideoSuccess)
//        {
//            int nRewardAmount = [[g_vdConfigNotification._config objectForKey:@"VideoAdRewardAmount"] intValue];
//            if (nRewardAmount < 2)
//                nRewardAmount = 14;
//            rewardVideoSuccess([NSDictionary dictionaryWithObjectsAndKeys:kVungleNetworkName, @"adnetworkname",
//                                [NSNumber numberWithInt:nRewardAmount], @"reward", nil]);
//            rewardVideoSuccess = nil;
//        }
//        else if (rewardVideoSuccess)
//        {
//            rewardVideoSuccess([NSDictionary dictionaryWithObjectsAndKeys:kVungleNetworkName, @"adnetworkname",
//                                [NSNumber numberWithInt:0], @"reward", nil]);
//            rewardVideoSuccess = nil;
//        }
//
//
//    }
//}
//
///**
// * if implemented, this will get called when the product sheet is about to be closed.
// */
//- (void)vungleSDKwillCloseProductSheet:(id)productSheet
//{
//    NSLog(@"MP_Vungle: vungleSDKwillCloseProductSheet %@",productSheet);
//
//    //// ???: Check then
//    [self userDidWatchAnAdVideo:YES];
//    [self userDidClickOnInterstitialAd];
//    if (rewardVideoSuccess)
//    {
//        int nRewardAmount = [[g_vdConfigNotification._config objectForKey:@"VideoAdRewardAmount"] intValue];
//        if (nRewardAmount < 2)
//            nRewardAmount = 14;
//        rewardVideoSuccess([NSDictionary dictionaryWithObjectsAndKeys:kVungleNetworkName, @"adnetworkname",
//                            [NSNumber numberWithInt:nRewardAmount], @"reward", nil]);
//        rewardVideoSuccess = nil;
//    }
//}
//


#pragma mark Admob Rewarded Video handle & GADRewardBasedVideoAdDelegate implementation

- (void)initAdmobRewardedVideo
{
    if (!bNeedRequestNewAdmobRewardVideo)
        return;
    [GADRewardBasedVideoAd sharedInstance].delegate = (id<GADRewardBasedVideoAdDelegate>)self;
    [self requestAdmobRewardedVideo];
}

- (void)requestAdmobRewardedVideo
{
    if (!bNeedRequestNewAdmobRewardVideo)
        return;
    
    if (![[GADRewardBasedVideoAd sharedInstance] isReady])
    {
        GADRequest *request = [GADRequest request];
        [[GADRewardBasedVideoAd sharedInstance] loadRequest:request
                                               withAdUnitID:[self adIDWithConfigKey:kConfig_adid_admob_rewardvideo]];
        bNeedRequestNewAdmobRewardVideo = NO;
    }
}

- (void)rewardBasedVideoAdDidReceiveAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad is received.");
}

- (void)rewardBasedVideoAdDidOpen:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Opened reward based video ad.");
}

- (void)rewardBasedVideoAdDidStartPlaying:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad started playing.");
}

- (void)rewardBasedVideoAdDidClose:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad is closed.");
    if (rewardVideoSuccess)
    {
        rewardVideoSuccess([NSDictionary dictionaryWithObjectsAndKeys:kAdmobNetworkName, @"adnetworkname",
                            [NSNumber numberWithInt:0], @"reward", nil]);
        rewardVideoSuccess = nil;
    }
// Prepare for new
    bNeedRequestNewAdmobRewardVideo = YES;
}

- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
   didRewardUserWithReward:(GADAdReward *)reward {
    NSString *rewardMessage =
    [NSString stringWithFormat:@"Reward received with currency %@ , amount %lf", reward.type,
     [reward.amount doubleValue]];
    if (rewardVideoSuccess)
    {
        rewardVideoSuccess([NSDictionary dictionaryWithObjectsAndKeys:kAdmobNetworkName, @"adnetworkname",
                        [NSNumber numberWithInt:[reward.amount intValue]], @"reward", nil]);
        rewardVideoSuccess = nil;
    }
    //NSLog(@"%@", rewardMessage);
    // Reward the user for watching the video.
    //[self earnCoins:[reward.amount integerValue]];
   // self.showVideoButton.hidden = YES;
}

- (void)rewardBasedVideoAdWillLeaveApplication:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad will leave application.");
}

- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
    didFailToLoadWithError:(NSError *)error {
    NSLog(@"Reward based video ad failed to load: %@", error);
    bNeedRequestNewAdmobRewardVideo = YES;
}
#endif

#pragma mark - Ad status handle
- (void)interstitialAdAppear:(NSObject*)interstitial
{
    bShowingInterstitialAd = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:kInterstitialAdWillAppear object:nil];
}

- (void)interstitialAdDisappear:(NSObject *)interstitial
{
#if __PPCLINKADS__
    if (dictCurBonusInfo && !bIsQualifiedBonus)
    {
        UIAlertView* al =[[UIAlertView alloc] initWithTitle:@"PPCLINK ADS SDK" message:@"Bạn phai click quang cao moi duoc thuong!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [al show];
    }
#endif
    
    // Check limit show interstital ad per session
    int nMaxTimeShowInterstitialAdPerSession = [[g_vdConfigNotification._config objectForKey:kConfig_InterstitialAdMaxTimePerSession] intValue];
    
    if (!nMaxTimeShowInterstitialAdPerSession || nCountShowInterstitialAd < nMaxTimeShowInterstitialAdPerSession)
    {
    // Request ad to cache for next time!
        if (interstitial == admobInterstitial)
        {
            admobInterstitial = [[GADInterstitial alloc] initWithAdUnitID:[self adIDWithConfigKey:kConfig_adid_admob_interstitial_image]];
            admobInterstitial.delegate = self;
            [admobInterstitial loadRequest:[self refineGADRequest]];
        }
        else if (interstitial == admobVideoInterstitial)
        {
            admobVideoInterstitial = [[GADInterstitial alloc] initWithAdUnitID:[self adIDWithConfigKey:kConfig_adid_admob_interstitial_video]];
            admobVideoInterstitial.delegate = self;
            [admobVideoInterstitial loadRequest:[self refineGADRequest]];
            
            // Complete watching admob video ad??: hiện tai chưa biết được user có xem hết video admob hay ko nên với admob bắt buộc user phải click mới dc thưởng
            // [self userDidWatchAnAdVideo:YES];
        }
        else if (interstitial == mopubVideoInterstitial)
        {
            // Phai click moi dc thuong
            //[self userDidWatchAnAdVideo:YES];   // Coi như user xem hết quảng cáo video của MoPub
            [mopubVideoInterstitial loadAd];
        }
        else if (interstitial == mopubInterstitial)
        {
            // Load to pre-cache for next show!
            [mopubInterstitial loadAd];
        }
    }
    
    bShowingInterstitialAd = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:kInterstitialAdWillDissappear object:nil];
    if (self.interstitialAdClose)
    {
        self.interstitialAdClose();
        self.interstitialAdClose = nil;
    }
}

- (void) userDidClickOnBannerAd
{
    NSLog(@"MP: userDidClickOnBannerAd");
    // Ko can bonus interstitial ad free time khi user click vao quang banner
    //[self updateInterstitialAdFreeExpiredDate:[[g_vdConfigNotification._config objectForKey:kConfig_InterstitialAdFreeMinTimeIntervalBonus] intValue]];
    
    [self updateAdBannerVisibility];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyBannerAdDidClick object:nil];
}

- (void) userDidClickOnInterstitialAd
{
    NSLog(@"PPCLINKADS: userDidClickOnInterstitialAd");
    
    [self updateInterstitialAdFreeExpiredDate:[[g_vdConfigNotification._config objectForKey:kConfig_InterstitialAdFreeMinTimeIntervalBonus] intValue]];
    
    [self updateAdBannerVisibility];
    
    bIsQualifiedBonus = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:kInterstitialAdDidClick object:nil userInfo:dictCurBonusInfo];
    if (self.interstitialAdClick)
    {
        self.interstitialAdClick();
        self.interstitialAdClick = nil;
    }
}

- (void) userDidWatchAnAdVideo:(BOOL)bCompletely
{
    NSLog(@"PPCLINKADS: userDidWatchAnAdVideo");
    
    [self updateInterstitialAdFreeExpiredDate:[[g_vdConfigNotification._config objectForKey:kConfig_InterstitialAdFreeMinTimeIntervalBonus] intValue]];
    
    [self updateAdBannerVisibility];
    
    bIsQualifiedBonus = bCompletely;
    
#if __PPCLINKADS__
    if (dictCurBonusInfo && !bIsQualifiedBonus)
    {
        UIAlertView* al =[[UIAlertView alloc] initWithTitle:@"PPCLINK ADS SDK" message:@"Bạn phai XEM HET quang cao VIDEO moi duoc thuong!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [al show];
    }
#endif
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kFinishVideoAdWatching object:[NSNumber numberWithBool:bCompletely] userInfo:dictCurBonusInfo];
    //    if (self.videoAdWatchCompletely)
    //    {
    //        self.videoAdWatchCompletely(dictCurBonusInfo);
    //        self.videoAdWatchCompletely = nil;
    //    }
    
    bShowingInterstitialAd = NO;
}

- (void)configureToRecirculateAdNetworks
{
    if ([[g_vdConfigNotification._config objectForKey:kConfig_RecirculateAdNetworkEnable] intValue] > 0)
    {
        if (currentSelectedInterstitialAdNetworkIndex < [interstitialAdNetworkGroup count])
        {
            // Cho uu tien mang quang cao vua hien xuong duoi cung...
            NSString* networkNameToBePushed = [NSString stringWithFormat:@"%@",[interstitialAdNetworkGroup objectAtIndex:currentSelectedInterstitialAdNetworkIndex]];
            [interstitialAdNetworkGroup removeObjectAtIndex:currentSelectedInterstitialAdNetworkIndex];
            [interstitialAdNetworkGroup addObject:networkNameToBePushed];
        }
        
        /*
         int numberAdNetworkTobePushed = currentSelectedInterstitialAdNetworkIndex + 1;
         
         for (int counter = 0; counter < numberAdNetworkTobePushed; counter++)
         {
         NSString* networkNameToBePushed = [NSString stringWithFormat:@"%@",[interstitialAdNetworkGroup objectAtIndex:0]];
         //NSString* networkNameToBePushed = [interstitialAdNetworkGroup objectAtIndex:0];
         [interstitialAdNetworkGroup removeObjectAtIndex:0];
         [interstitialAdNetworkGroup addObject:networkNameToBePushed];
         }
         
         NSLog(@"AdNetwork: %@",interstitialAdNetworkGroup);
         */
    }
}

#endif //#ifndef __PPCLINKAds_NativeOnly__

@end

